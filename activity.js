// JavaScript Document
if(!(window.console && console.log)) {
  console = {
    log: function(){},
    debug: function(){},
    info: function(){},
    warn: function(){},
    error: function(){}
  };
}

var debug = true;
var section;
var nextSection = "";
var config;
var press = null;
var blotter_content = [];
var completed = [];
var speaker_snd;
var scoring;
var pauseInfo;
var itemIdx = 0;
var useDebugFunc;
if (useDebugFunc==null) var useDebugFunc = false; 
var fa_answers;
var fa_order;
var fa_correct;
var fa_score;
var kc_correct = false;
var continue_after_blotter = false;
var continue_after_coaching = false;
var showBlotterMessage = false;
var blotterMessageShown = false;
var help_overlay_shown_count = 0;
var showHelpID, scrollUpID, scrollDownID;
var activeScrollbar;
var finalExamCount;
var scorm;

function debugFunc(){
	section = {person:"tammy", scenario:config.tammy, section:1};
	//results = { correct:0, score:0, count:12 };
	//exportRoot.gotoAndPlay("initPerson");
	//initCoaching();
	//showBlotterAfterCoaching();
	//completed.push("james");
	//completed.push("alison");
	//completed.push("tammy");
	completed.push("kevin");
	//exportRoot.gotoAndPlay("video");
	//exportRoot.gotoAndPlay("final_assessment");
	//exportRoot.gotoAndPlay("knowledge_check");
	//jumpToSection("tammy");
	//jumpToSection("coach");
	//blotter_content.push({text:"This is the blotter text", coaching:"This is coaching text"});
	//exportRoot.miniblotter_mc.play();
	exportRoot.gotoAndPlay("epilogue");
}

$(document).ready(function(){
	dbLog("Document ready " + navigator.userAgent);
	$.getJSON( "config.json", function( _data ) {
  		config = _data;
		findRepeatBlotterEntries();
		try{
			scorm = new SCORMInterface("1.2", debug);
		}catch(e){
		}
		if (scorm){
			scorm.initialize();
		}else if (typeof(Storage) !== "undefined") {
			var json = JSON.parse(localStorage.getItem("data"));
			if (json!=null){
				completed = (json.completed!=null) ? json.completed : [];
				if (json.section!=null) section = json.section;
				if (json.title!=null) title = json.title;
				finalExamCount = (json.finalExamCount!=null) ? Number(json.finalExamCount) : 0;
			}else{
				finalExamCount = 0;
			}
		}
  	}).fail(function() {
		console.log( "error loading config file" );
	});
});

function scormStart(){
	var json = JSON.parse(scorm.get());
	completed = json.completed;
	section = json.section;
	results = json.results;
	finalExamCount = (json.finalExamCount==null) ? 0 : json.finalExamCount;
	if (section!=null && section.person!=null){
		if (json.title!=null && json.title){
			exportRoot.gotoAndPlay("title");
		}else{
			jumpToSection(section.person);
		}
	}else{
		exportRoot.play();
	}
}

function initCourseComplete(mc){
	dbLog("initCourseComplete " + fa_score);
	mc.score_txt.text = Math.floor(fa_score*10) + "%";
}

function endCourse(){
	dbLog("End course");
	if (scorm && scorm.initialized){
		scorm.save();
	}
	window.location = "eval.php";
}

function failCourse(){
	dbLog("Fail course");
	if (scorm && scorm.initialized){
		scorm.terminate();
	}else{
		completed = [];
		exportRoot.gotoAndPlay(0);
		useDebugFunc = false;
		alert("No scorm");
	}
}

function findRepeatBlotterEntries(){
	var speakers = [ "james", "alison", "tammy", "kevin" ];
	
	for (var i=0; i<speakers.length; i++){
		var interview = config[speakers[i]];
		for (var j=0; j<interview.length; j++){
			if (interview[j].blotter!="" && interview[j].blotter_repeat==null){
				for (var k=j+1; k<interview.length; k++){
					if (interview[k].blotter!="" && interview[k].blotter==interview[j].blotter){
						dbLog("findRepeatBlotterEntries speaker:" + speakers[i] + " repeat at " + k); 
						interview[k].blotter_repeat = true;
					}
				}
			}
		}
	}
}

function showVideo(show, video){
	if (show){
		var left = $("#inner").css("left");
		var top = $("#inner").css("top");
		dbLog("Inner at " + left + ", " + top);
		$("#videoPlaceholder").show();
		var vid = $('video')[0];
		if (vid!=null){
			if (video!=null){
				var ext = (navigator.userAgent.indexOf("Firefox")==-1) ? ".mp4" : ".webm";
				vid.src = "video/" + video + ext;
				vid.load();
				vid.onended = function(){
					showVideo(false);
					itemIdx = 0;
					fa_answers = new Array();
					exportRoot.gotoAndPlay("final_assessment");
				}
			}else{
				vid.onended = function(){
					exportRoot.play();
				}
			}
			
			vid.play();
		}
		exportRoot.skip_btn.visible = false;
	}else{
		$("#videoPlaceholder").hide();
		var vid = $('video')[0];
		if (vid!=null) vid.pause();
	}
}

function gotoAndPlay(label){
	exportRoot.gotoAndPlay(label);
}

function jumpToSection(speaker){
	if (speaker=="coach"){
		section = {person:"alison", scenario:config.alison, section:1};
		completed.push("alison");
		//blotter_content = new Array();
		//blotter_content.push({person:"tammy", id:24 });
		//blotter_content.push({person:"tammy", id:49 });
		results = { correct:12, score:0, count:12 };
		exportRoot.gotoAndPlay("coaching");
	}else{
		section = {person:speaker, scenario:config[speaker], section:1};
		results = { correct:0, score:0, count:0 };
		exportRoot.gotoAndPlay("initPerson");
	}
}

function dbLog(str){
	if (debug) console.log(str);
}

function helpPressed(){
	dbLog("helpPressed");
	exportRoot.biographies_mc.visible = false;
	exportRoot.background_mc.visible = false;
	exportRoot.glossary_mc.visible = false;
	exportRoot.help_mc.visible = true;
	exportRoot.help_mc.gotoAndPlay(0);
}

function pausePressed(btn){
	dbLog("pausePressed " + btn.name);
	
	if (btn.currentFrame==0){
		if (speaker_snd!=null){
			speaker_snd.pause();
			setAnimation("head", "static");
			btn.gotoAndStop(1);
		}
	}else{
		if (speaker_snd!=null){
			speaker_snd.togglePlay();
			setAnimation("head", "talk");
			btn.gotoAndStop(0);
		}
	}
}

function closePressed(){
	dbLog("closePressed");
}

function jamesPressed(direct){
	dbLog("jamesPressed");
	if (direct==null) direct = (completed.indexOf("james")==-1) ? true : false;
	if (!direct){
		exportRoot.james_mc.confirm_mc.gotoAndPlay(0);	
	}else{
		section = {person:"james", scenario:config.james, section:1};
		exportRoot.progress_mc.scenario_mc.gotoAndStop(0);
		exportRoot.gotoAndPlay("title");
		blotter_content = new Array();
	}
}

function alisonPressed(direct){
	if (direct==null) direct = (completed.indexOf("alison")==-1) ? true : false;
	if (!direct){
		exportRoot.alison_mc.confirm_mc.gotoAndPlay(0);	
	}else{
		dbLog("alisonPressed");
		section = {person:"alison", scenario:config.alison, section:1};
		exportRoot.gotoAndPlay("title");
		blotter_content = new Array();
	}
}

function tammyPressed(direct){
	if (direct==null) direct = (completed.indexOf("tammy")==-1) ? true : false;
	if (!direct){
		exportRoot.tammy_mc.confirm_mc.gotoAndPlay(0);	
	}else{
		dbLog("tammyPressed");
		section = {person:"tammy", scenario:config.tammy, section:1};
		exportRoot.gotoAndPlay("title");
		blotter_content = new Array();
	}
}

function kevinPressed(direct){
	if (direct==null) direct = (completed.indexOf("kevin")==-1) ? true : false;
	if (!direct){
		exportRoot.kevin_mc.confirm_mc.gotoAndPlay(0);	
	}else{
		dbLog("kevinPressed");
		section = {person:"kevin", scenario:config.kevin, section:1};
		exportRoot.gotoAndPlay("title");
		blotter_content = new Array();
	}
}

function backgroundPressed(){
	dbLog("backgroundPressed");
	exportRoot.biographies_mc.visible = false;
	exportRoot.glossary_mc.visible = false;
	exportRoot.background_mc.visible = true;
	exportRoot.background_mc.gotoAndPlay("open");
}

function initHelp(mc){
	if (config==null || config.help==null) return;
	var help = config.help;//[ {name:"Help", desc:"This will be help"} ];
	var items_mc = mc.items_mc.text_txt;
	var help_mc;
	
	for(var i=0; i<help.length; i++){
		if (i==0){
			help_mc = items_mc.glossary1_mc;
		}else{
			help_mc = new lib.glossary_item();
			items_mc.addChild(help_mc);
			help_mc.x = 0;
			help_mc.y = items_mc.glossary1_mc.y + i*160;
		}
		help_mc.title_txt.text = help[i].name;
		help_mc.desc_txt.text = help[i].desc;
	}
	
	var height = 456;
	var textHeight = 160 * help.length;
	initScrollbar(mc.items_mc, height, textHeight, 153);
	
	mc.close_btn.removeAllEventListeners();
	mc.close_btn.on("click", function(event){
		var parent = event.currentTarget.parent;
		parent.gotoAndPlay("close");
	});
}

function initKnowledgeCheck(mc){
	switch (section.person){
		case "james":
		mc.kcperson_mc.gotoAndStop(0);
		break;
		case "alison":
		mc.kcperson_mc.gotoAndStop(1);
		break;
		case "tammy":
		mc.kcperson_mc.gotoAndStop(2);
		break;
		case "kevin":
		mc.kcperson_mc.gotoAndStop(3);
		break;
	}
	itemIdx = 0;
	nextSection = (checkCompleted("kevin")) ? "epilogue" : "start";
}

function initKCQuestion(mc){
	var questions = config.knowledge_check[section.person];
	
	if (itemIdx<questions.length){
		mc.kcprogress_mc.text_txt.text = "KNOWLEDGE CHECK: Question " + (itemIdx + 1) + " of " + questions.length;
		mc.kcquestion_mc.text_txt.text = questions[itemIdx].question;
		var mcs = new Array(mc.kcanswer1_mc, mc.kcanswer2_mc, mc.kcanswer3_mc, mc.kcanswer4_mc);
		var letters = new Array("A", "B", "C", "D");
		for(var i=0; i<4; i++){
			mcs[i].letter_txt.text = letters[i];
			mcs[i].answer_txt.text = questions[itemIdx].answers[i].answer;
			mcs[i].feedback = questions[itemIdx].answers[i].feedback;
			mcs[i].correct = questions[itemIdx].answers[i].correct;
			mcs[i].gotoAndStop(0);
			mcs[i].cursor = "pointer";
			mcs[i].removeAllEventListeners();
			mcs[i].on("click", function(event){
				var mc = event.currentTarget;
				if (mc.correct){
					mc.gotoAndStop(2);
					kc_correct = true;
				}else{
					mc.gotoAndStop(1);
					kc_correct = false;
				}
				exportRoot.kcfeedback_mc.scrollpanel_mc.text_txt.text = mc.feedback;
				var height = 93;
				var textHeight = exportRoot.kcfeedback_mc.scrollpanel_mc.text_txt.getMeasuredHeight();
				dbLog("initKCQuestion text height=" + textHeight + " scrollbar height=" + height);	
				initScrollbar(exportRoot.kcfeedback_mc.scrollpanel_mc, height, textHeight);
				
				exportRoot.play();
			});
		}
		itemIdx++;
	}else{
		exportRoot.gotoAndPlay("kc_end");
	}
}

function initEpilogueText(mc){
	mc.etext_mc.text_txt.text = config.epilogue[itemIdx].text;
	mc.etext_mc.result_txt.text = "RESULT: " + config.epilogue[itemIdx].result;
	var length = config.epilogue[itemIdx].text.length + config.epilogue[itemIdx].result.length;
	var delay = length * 50;
	setTimeout(continueEpilogue, delay);
	itemIdx++;
}

function continueEpilogue(){
	exportRoot.play();
}

function initFAQuestion(mc){
	if (itemIdx==0){
		for(var i=0; i<config.final_assessment.length; i++){
			config.final_assessment[i].used = false;
		}
		fa_score = 0;
	}
	var idx;
	do{
		idx = Math.floor(Math.random()*config.final_assessment.length);
	}while(config.final_assessment[idx].used);
	//idx = 15;
	config.final_assessment[idx].used = true;
	var question = config.final_assessment[idx];
	fa_correct = question.correct;
	mc.faprogress_mc.text_txt.text = "FINAL ASSESSMENT: Question " + (itemIdx+1) + " of 10";
	mc.faquestion_mc.text_txt.text = question.question;
	var mcs = new Array(mc.faanswer1_mc, mc.faanswer2_mc, mc.faanswer3_mc, mc.faanswer4_mc, mc.faanswer5_mc);
	var letters = new Array("A", "B", "C", "D", "E");
	if (question.order!=null && question.order) fa_order = new Array();
	for(var i=0; i<5; i++){
		if (i<question.answers.length){
			mcs[i].letter_txt.text = letters[i];
			mcs[i].text_txt.text = question.answers[i];
			mcs[i].order_txt.text = "";
			mcs[i].gotoAndStop(0);
			mcs[i].visible = true;
			mcs[i].removeAllEventListeners();
			mcs[i].order = (question.order!=null && question.order);
			mcs[i].removeAllEventListeners();
			mcs[i].on("click", faAnswerPressed);
			mcs[i].cursor = "pointer";
		}else{
			mcs[i].visible = false;
		}
	}
	dbLog("Correct answers are " + JSON.stringify(fa_correct));
	itemIdx++;
}

function faAnswerPressed(event){
	var mc = event.currentTarget;
	if (mc.order){
		var pos = fa_order.indexOf(mc);
		if (pos!=-1){
			//Already in the list so remove it
			fa_order.splice(pos, 1);
		}
		fa_order.push(mc);
		for(var i=0; i<fa_order.length; i++){
			fa_order[i].order_txt.text = new String(i+1);
			fa_order[i].gotoAndStop(1);
		}
	}else{
		if (mc.currentFrame!=0){
			mc.gotoAndStop(0);
		}else{
			mc.gotoAndStop(1);
		}
	}
}

function checkFAAnswer(event){
	var mc = exportRoot;
	var answer = new Array();
	var mcs = new Array(mc.faanswer1_mc, mc.faanswer2_mc, mc.faanswer3_mc, mc.faanswer4_mc, mc.faanswer5_mc);
	var score = 1;
	var order = false;
	for(var i=0; i<5; i++){
		if (!mcs[i].visible) continue;
		if (mcs[i].order){
			order = true;
			answer.push(mcs[i].order_txt.text);
		}else{
			answer.push(mcs[i].currentFrame==1);
			if (mcs[i].currentFrame==0 && fa_correct.indexOf(i)!=-1) score = 0; 
			if (mcs[i].currentFrame==1 && fa_correct.indexOf(i)==-1) score = 0; 
		}
	}
	if (order){
		var ordered = new Array();
		for(var i=0; i<answer.length; i++){
			var idx = parseInt(answer[i]) - 1;
			ordered[idx] = i;
		}
		var str1 = JSON.stringify(ordered);
		var str2 = JSON.stringify(fa_correct);
		if (str1 != str2) score = 0;
		dbLog("checkFAAnswer order answer:" + str1 + " correct:" + str2 + " score:" + score);
	}else{
		dbLog("checkFAAnswer score:" + score);
	}
	fa_score += score;
	if (fa_answers == null) fa_answers = new Array();
	fa_answers.push(answer);
	if (itemIdx<10){
		dbLog(JSON.stringify("checkFAAnswer: fa_answers length:" + fa_answers.length));
		mc.gotoAndPlay("fa_question");
	}else{
		dbLog(JSON.stringify(fa_answers) + " score=" + fa_score);
		if (fa_score>=7){
			mc.play();
		}else{
			mc.gotoAndPlay("course_fail");
		}
	}
	mc.facontinue_btn.removeAllEventListeners();
}

function initBackgroundButtons(){
	var mc = exportRoot.background_mc;
	mc.bg1_btn.gotoAndStop(1);
	mc.bg2_btn.gotoAndStop(3);
	mc.bg3_btn.gotoAndStop(5);
}

function initBackground(mc){
	setTimeout( initBackgroundButtons, 100 );
	
	mc.bg1_btn.cursor = 'pointer';
	mc.bg2_btn.cursor = 'pointer';
	mc.bg3_btn.cursor = 'pointer';
	
	mc.bg1_btn.removeAllEventListeners();
	mc.bg1_btn.on("click", function(event){
		var btn = event.currentTarget;
		var parent = btn.parent;
		parent.bg_mc.gotoAndStop(0);
		parent.gotoAndPlay("showBackground");
		btn.gotoAndStop(0);
		parent.bg2_btn.gotoAndStop(3);
		parent.bg3_btn.gotoAndStop(5);
	});
	mc.bg2_btn.removeAllEventListeners();
	mc.bg2_btn.on("click", function(event){
		var btn = event.currentTarget;
		var parent = btn.parent;
		parent.bg_mc.gotoAndStop(1);
		parent.gotoAndPlay("showBackground");
		btn.gotoAndStop(2);
		parent.bg1_btn.gotoAndStop(1);
		parent.bg3_btn.gotoAndStop(5);
	});
	mc.bg3_btn.removeAllEventListeners();
	mc.bg3_btn.on("click", function(event){
		var btn = event.currentTarget;
		var parent = btn.parent;
		parent.bg_mc.gotoAndStop(2);
		parent.gotoAndPlay("showBackground");
		btn.gotoAndStop(4);
		parent.bg2_btn.gotoAndStop(3);
		parent.bg1_btn.gotoAndStop(1);
	});
	mc.close_btn.removeAllEventListeners();
	mc.close_btn.on("click", function(event){
		var parent = event.currentTarget.parent;
		parent.bg1_btn.gotoAndStop(1);
		parent.bg2_btn.gotoAndStop(3);
		parent.bg3_btn.gotoAndStop(5);
		parent.gotoAndPlay("close");
	});
}

function initBiogButtons(mc, noTammy){
	if (noTammy==null) noTammy = false;
	mc.james_btn.stop();
	mc.james_btn.cursor = 'pointer';
	mc.james_btn.removeAllEventListeners();
	mc.james_btn.removeAllEventListeners();
	mc.james_btn.on("click", function(event){
		var btn = event.currentTarget;
		var parent = btn.parent;
		parent.biog = "james";
		if (checkCompleted("alison")){
			parent.gotoAndPlay("showBiog");
		}else{
			parent.gotoAndPlay("showBiog2");
		}
		btn.gotoAndStop(1);
		parent.tammy_btn.gotoAndStop(0);
		parent.alison_btn.gotoAndStop(0);
		parent.kevin_btn.gotoAndStop(0);
	});
	if (!noTammy){
		mc.tammy_btn.stop();
		mc.tammy_btn.cursor = 'pointer';
		mc.tammy_btn.removeAllEventListeners();
		mc.tammy_btn.removeAllEventListeners();
		mc.tammy_btn.on("click", function(event){
			var btn = event.currentTarget;
			var parent = btn.parent;
			parent.biog = "tammy";
			if (checkCompleted("alison")){
				parent.gotoAndPlay("showBiog");
			}else{
				parent.gotoAndPlay("showBiog2");
			}
			btn.gotoAndStop(1);
			parent.james_btn.gotoAndStop(0);
			parent.alison_btn.gotoAndStop(0);
			parent.kevin_btn.gotoAndStop(0);
		});
	}
	mc.alison_btn.stop();
	mc.alison_btn.cursor = 'pointer';
	mc.alison_btn.removeAllEventListeners();
	mc.alison_btn.removeAllEventListeners();
	mc.alison_btn.on("click", function(event){
		var btn = event.currentTarget;
		var parent = btn.parent;
		parent.biog = "alison";
		if (checkCompleted("alison")){
			parent.gotoAndPlay("showBiog");
		}else{
			parent.gotoAndPlay("showBiog2");
		}
		btn.gotoAndStop(1);
		parent.tammy_btn.gotoAndStop(0);
		parent.james_btn.gotoAndStop(0);
		parent.kevin_btn.gotoAndStop(0);
	});
	mc.kevin_btn.stop();
	mc.kevin_btn.cursor = 'pointer';
	mc.kevin_btn.removeAllEventListeners();
	mc.kevin_btn.removeAllEventListeners();
	mc.kevin_btn.on("click", function(event){
		var btn = event.currentTarget;
		var parent = btn.parent;
		parent.biog = "kevin";
		if (checkCompleted("alison")){
			parent.gotoAndPlay("showBiog");
		}else{
			parent.gotoAndPlay("showBiog2");
		}
		btn.gotoAndStop(1);
		parent.tammy_btn.gotoAndStop(0);
		parent.alison_btn.gotoAndStop(0);
		parent.james_btn.gotoAndStop(0);
	});
	mc.close_btn.removeAllEventListeners();
	mc.close_btn.on("click", function(event){
		var parent = event.currentTarget.parent;
		if (checkCompleted("alison")){
			parent.gotoAndPlay("close");
		}else{
			parent.gotoAndPlay("close2");
		}
	});
}

function initBiog(mc){
	var biog = mc.parent.biog;
	var info = config[biog][0];
	mc.name_txt.text = info.name;
	mc.position_txt.text = info.position;
	mc.biog_txt.text = info.biog;
}

function initGlossary(mc){
	var glossary = config.glossary;
	var items_mc = mc.glossary_mc.text_txt;
	var glossary_mc;
	
	for(var i=0; i<glossary.length; i++){
		if (i==0){
			glossary_mc = items_mc.glossary1_mc;
		}else{
			glossary_mc = new lib.glossary_item();
			items_mc.addChild(glossary_mc);
			glossary_mc.x = 0;
			glossary_mc.y = items_mc.glossary1_mc.y + i*160;
		}
		glossary_mc.title_txt.text = glossary[i].name;
		glossary_mc.desc_txt.text = glossary[i].desc;
	}
	
	var height = 456;
	var textHeight = 160 * glossary.length;
	initScrollbar(mc.glossary_mc, height, textHeight, 153);
	mc.close_btn.removeAllEventListeners();
	mc.close_btn.on("click", function(event){
		var parent = event.currentTarget.parent;
		parent.gotoAndPlay("close");
	});
}

function policyPressed(){
	if (config.policy_url!=null){
		dbLog("policyPressed link:" + config.policy_url);
		window.open(config.policy_url);
	}else{
		dbLog("policyPressed no link");
	}
}

function biographiesPressed(){
	dbLog("biographiesPressed");
	exportRoot.background_mc.visible = false;
	exportRoot.glossary_mc.visible = false;
	exportRoot.biographies_mc.visible = true;
	exportRoot.biographies_mc.gotoAndPlay(0);
}

function glossaryPressed(){
	dbLog("glossaryPressed");
	exportRoot.biographies_mc.visible = false;
	exportRoot.background_mc.visible = false;
	exportRoot.glossary_mc.visible = true;
	exportRoot.glossary_mc.gotoAndPlay(0);
}

function blotterPressed(){
	dbLog("blotterPressed");
	exportRoot.blotter_mc.visible = true;
	exportRoot.blotter_mc.play();
}

function showMiniBlotter(){
	dbLog("showMiniBlotter");
	exportRoot.miniblotter_mc.play();
}

function continuePressed(){
	dbLog("continuePressed");
}

function initTitle(mc){
	dbLog("initTitle");
	mc.title_mc.text_txt.text = section.scenario[0].title;
	mc.desc_mc.text_txt.text = section.scenario[0].desc;
	var frm = 0;
	switch(section.person){
		case "alison":
		frm = 1;
		break;
		case "tammy":
		frm = 2;
		break;
		case "kevin":
		frm = 3;
		break;
	}
	mc.titleImg_mc.gotoAndStop(frm);
	var height = 295;
	var textHeight = mc.desc_mc.text_txt.getMeasuredHeight();
	dbLog("initTitle text height=" + textHeight + " scrollbar height=" + height);	
	initScrollbar(mc.desc_mc, height, textHeight);
	mc.continue_btn.visible = true;
	mc.continue_btn.removeAllEventListeners();
	mc.continue_btn.on("click", function(event){
		results = { correct:0, score:0, count:0 };
		exportRoot.play();
	});
	scormSave();
}

function scormSave(){
	var json = new Object();
	json.completed = completed;
	json.section = section;
	json.title = true;
	json.finalExamCount = finalExamCount;
	if (scorm!=null && scorm.initialized){
		scorm.set(JSON.stringify(json));
	}else if (typeof(Storage) !== "undefined") {
		localStorage.setItem("data", JSON.stringify(json));
	}
}

function scrollPanelUp(){
	var thumbY = activeScrollbar.thumb_mc.y - 5;
	if (thumbY<17) thumbY = 17;
	activeScrollbar.thumb_mc.y = thumbY;
	updatePanelAfterThumbMove();
	
}

function scrollPanelDown(){
	var thumbY = activeScrollbar.thumb_mc.y + 5;
	if (thumbY>activeScrollbar.thumb_mc.maxY) thumbY = activeScrollbar.thumb_mc.maxY;
	activeScrollbar.thumb_mc.y = thumbY;
	updatePanelAfterThumbMove();
}

function updatePanelAfterThumbMove(){
	var range = activeScrollbar.parent.panel_mc.minY;
	var maxY = activeScrollbar.thumb_mc.maxY;
	var thumbY = activeScrollbar.thumb_mc.y;
	var textY = ((thumbY - 17)/maxY) * range;
	var orgY = activeScrollbar.thumb_mc.text_txt.orgY;
	activeScrollbar.thumb_mc.text_txt.y = textY + orgY;
}

function initScrollbar( mc, height, textHeight, orgY, blotter ){
	mc.scrollbar_mc.visible = (textHeight>height);
	if (orgY==null) orgY = 0;
	if (blotter!=null && blotter){
		mc.items_mc.orgY = orgY;
		mc.items_mc.y = orgY;
	}else{
		mc.text_txt.orgY = orgY;
		mc.text_txt.y = orgY;
	}
	if (!mc.scrollbar_mc.visible){
		mc.panel_mc.off("pressmove");
		mc.panel_mc.off("pressup");
		return;
	}
	mc.scrollbar_mc.thumb_mc.y = 17;
	mc.scrollbar_mc.thumb_mc.text_txt = (blotter!=null && blotter) ? mc.items_mc : mc.text_txt;
	mc.scrollbar_mc.thumb_mc.maxY = height - 17;
	mc.scrollbar_mc.thumb_mc.textHeight = textHeight;
	mc.scrollbar_mc.thumb_mc.visibleHeight = height;
	mc.panel_mc.scrollbar = mc.scrollbar_mc;
	mc.panel_mc.text_txt = (blotter!=null && blotter) ? mc.items_mc : mc.text_txt;
	mc.panel_mc.minY = -(textHeight - height) - 10;
	
	if (mc.scrollbar_mc.scrollup_btn != null){
		var btn = mc.scrollbar_mc.scrollup_btn;
		btn.removeAllEventListeners();
		btn.on("mousedown", function(event){
			scrollUpID = setInterval(scrollPanelUp, 50);
			activeScrollbar = event.currentTarget.parent;
			window.onmouseup = function(event){
				dbLog("window mouseup");
				clearInterval(scrollUpID);
				activeScrollbar = null;
				window.onmouseup = null;
			};
		});
	}
	
	if (mc.scrollbar_mc.scrolldown_btn != null){
		var btn = mc.scrollbar_mc.scrolldown_btn;
		btn.removeAllEventListeners();
		btn.on("mousedown", function(event){
			scrollDownID = setInterval(scrollPanelDown, 50);
			activeScrollbar = event.currentTarget.parent;
			window.onmouseup =  function(event){
				dbLog("window mouseup");
				clearInterval(scrollDownID);
				activeScrollbar = null;
				window.onmouseup = null;
			};
		});
	}
	mc.panel_mc.removeAllEventListeners();
	mc.panel_mc.on("pressmove", function(event){
		if (press==null){
			press = { textY:event.currentTarget.text_txt.y, stageY:event.stageY };
		}else{
			var textY = event.stageY - press.stageY + press.textY - event.currentTarget.text_txt.orgY;
			if (textY<=10 && textY>=event.currentTarget.minY){
				event.currentTarget.text_txt.y = textY + event.currentTarget.text_txt.orgY;
				var prop = textY/(event.currentTarget.minY);
				var thumbY = prop * event.currentTarget.scrollbar.thumb_mc.maxY + 17;
				if (thumbY<17) thumbY = 17;
				if (thumbY>event.currentTarget.scrollbar.thumb_mc.maxY) thumbY = event.currentTarget.scrollbar.thumb_mc.maxY;
				event.currentTarget.scrollbar.thumb_mc.y = thumbY;
			}
		}
	});
	mc.panel_mc.on("pressup", function(event){
		press = null;
	});
	mc.scrollbar_mc.thumb_mc.removeAllEventListeners();
	mc.scrollbar_mc.thumb_mc.on("pressmove", function(event){
		var thumb = event.currentTarget;
		var scrollbar = thumb.parent;
		var pt = scrollbar.globalToLocal(event.stageX, event.stageY);
		if (pt.y>=17 && pt.y<=thumb.maxY){
			 thumb.y = pt.y;
			var prop = (pt.y-17)/(thumb.maxY-17);
			var textY = (thumb.textHeight-thumb.visibleHeight) * prop;
			thumb.text_txt.y = thumb.text_txt.orgY - textY;
		}
	});
}

function setCharacterFade(mc, full, frm){
	var amount = (full) ? 1.0 : 0.3;
	
	createjs.Tween.get(mc).to({alpha:amount},10);	
	if (frm!=null) mc.gotoAndStop(frm);
}

function initPersonAnimation(mc){
	dbLog("initPersonAnimation");
	var frm = 0;
	if (sprites["coach"]!=null && sprites.loaded["coach"] != null && sprites.loaded.coach.loaded) sprites.coach.visible = false;
	switch(section.person){
		case "james":
		frm = 0;
		mc.person_mc.x = 750;
		showSprite("james");
		break;
		case "alison":
		frm = 1;
		mc.person_mc.x = 750;
		showSprite("alison");
		break;
		case "tammy":
		frm = 2;
		mc.person_mc.x = 270;
		showSprite("tammy");
		break;
		case "kevin":
		frm = 3;
		mc.person_mc.x = 270;
		showSprite("kevin");
		break;
	}
	if (!sprites.loaded[section.person].loaded){
		mc.characters_mc.visible = false;
		mc.stop();
		mc.loading_mc.visible = true;
		setTimeout(checkSpriteLoaded, 200);
	}else{
		exportRoot.characters_mc.visible = true;
		mc.loading_mc.visible = false;
		sprites[section.person].visible = true;
	}
	mc.bgs_mc.gotoAndStop(frm);
}

function checkSpriteLoaded(){
	//dbLog("checkSpriteLoaded");
	if (sprites.loaded[section.person].loaded){
		exportRoot.characters_mc.visible = true;
		sprites[section.person].visible = true;
		exportRoot.play();
	}else{
		setTimeout(checkSpriteLoaded, 200);
	}
}

function blotterCollected(person, id){
	//Return true if this blotter segment is already in the list
	for(var i=0; i<blotter_content.length; i++){
		if (blotter_content[i].person==person && blotter_content[i].id==id) return true;
	}
	
	return false;
}

function showBlotterAfterCoaching(){
	/*var items = new Array();
	
	for(var i=0; i<section.scenario.length; i++){
		var current = section.scenario[i];
		if (current.blotter != null && current.blotter!=""){
			items.push({ person:current.person, id:section.section, text:current.blotter, coaching:current.coaching, collected:blotterCollected(current.person, i) });
		}
	}
	
	blotter_content = items;*/
	continue_after_blotter = true;
	blotterPressed();
	
	var score = (results.score/results.count)/2;
	if (score<0.4){
		nextSection = "initPerson";
		section.section = 1;
		exportRoot.progress_mc.scenario_mc.gotoAndStop(0);
	}else{
		nextSection = "start";
	}
}

function initPerson(mc){
	dbLog("initPerson");
	var current = section.scenario[section.section];
	
	if (section.section==0){
		if (!checkCompleted(section.person)) completed.push(section.person);
		var frm = Math.floor((completed.length/4) * 50);
		if (frm>49) frm = 49;
		exportRoot.progress_mc.course_mc.gotoAndStop(frm);
		exportRoot.gotoAndPlay("coaching");
		return;
	}
	
	if (current.speaker == "player"){
		mc.panel_mc.name_txt.text = "Player";
		mc.visible = false;
	}else{
		mc.visible = true;
		mc.panel_mc.name_txt.text = section.scenario[0].name;
	}
	mc.panel_mc.text_txt.text = section.scenario[section.section].dialogue;
	var height = 271;
	var textHeight = mc.panel_mc.text_txt.getMeasuredHeight();
	initScrollbar(mc.panel_mc, height, textHeight, 65);
	var frm = Math.floor((section.section/section.scenario.length) * 50);
	if (frm>49) frm = 49;
	exportRoot.progress_mc.scenario_mc.gotoAndStop(frm);
}

function pauseForDialogue(){
	dbLog("pauseForDialogue id:" + section.section + " speaker:" + section.scenario[section.section].speaker);
	
	if (section.scenario[section.section].speaker == "player"){
		if (speaker_snd!=null) speaker_snd.stop();
		if (rand>0.95){
			setAnimation( "head", "nod" );
		}else if (rand>0.9){
			setAnimation( "head", "shake" );
		}else if (rand<0.85){
			setAnimation( "head", "tilt" );
		}else if (rand<0.8){
			setAnimation( "head", "static" );
		}
		
		showPlayerChoice();
		
	}else{
		playSound(section.scenario[section.section].speaker, section.scenario[section.section].id);
		var rand = Math.random();
		if (rand>0.7){
			setAnimation( "body", "left_arm" );
		}else if (rand>0.5){
			setAnimation( "body", "hands" );
		}else if (rand>0.4){
			setAnimation( "body", "shrug" );
		}else if (rand>0.3){
			setAnimation( "body", "point" );
		}else{
			setAnimation( "body", "right_arm" );
		}
		exportRoot.stop();
		//showChoices();
	}
}

function showPlayerChoice(){
	if (section.section==1){
		exportRoot.play();
		exportRoot.continue_btn.visible = false;
	}
	//setTimeout(continueAfterPlayerChoice, 1500);
}

function showChoices(){
	if (section.scenario[section.section].choices[0]==0){
		//Scenario complete
		if (!checkCompleted(section.person)) completed.push(section.person);
		var frm = Math.floor((completed.length/4) * 50);
		if (frm>49) frm = 49;
		exportRoot.progress_mc.course_mc.gotoAndStop(frm);
		exportRoot.gotoAndPlay("start");
	}else{
		exportRoot.play();
		exportRoot.continue_btn.visible = false;
	}
}

function slideInChoice(){
	var i = section.choiceId;
	section.choiceId++;
	if (i==0){
		createjs.Tween.get(exportRoot.choice1_mc).to({y:550}, 300);
	}else if (i==1){
		createjs.Tween.get(exportRoot.choice2_mc).to({y:550}, 300);
	}else if (i==2){
		createjs.Tween.get(exportRoot.choice3_mc).to({y:550}, 300);
	}
	if (section.choiceId<3) setTimeout(slideInChoice, 200);
}

function showHelpOverlay(){
	exportRoot.helpoverlay_mc.visible = true;
}

function initChoices(mc){
	dbLog("initChoices mc:" + mc);
	
	clearTimeout(showHelpID);
	var current = section.scenario[section.section];
	var choices; 
	
	if (mc!=null && current.blotter != null && current.blotter!="" && !blotterCollected(section.person, section.section)){
		blotter_content.push({ person:section.person, id:section.section, text:current.blotter, coaching:current.coaching, collected:true });
		showMiniBlotter();
		choices = [ mc.choice1_mc, mc.choice2_mc, mc.choice3_mc ];
		for(var i=0; i<choices.length; i++) choices[i].y = 752;
		mc.person_mc.visible = false;
		return;
	}
	
	if (mc==null) mc = exportRoot;
	choices = [ mc.choice1_mc, mc.choice2_mc, mc.choice3_mc ];
	if (section.section!=1) mc.person_mc.visible = true;
	
	section.choiceId = 0;
	
	var choiceData = new Array();
	
	for(var i=0; i<current.choices.length; i++){
		choiceData.push({id:current.choices[i], correct:(i==0), score:(2-i)});
	}
	
	if (choiceData.length>1){
		for(var i=0; i<3; i++){
			var choice = choiceData.splice(Math.floor(Math.random()*choiceData.length), 1);
			choiceData.push(choice[0]);
		}
	}
	
	if (help_overlay_shown_count<3){
		showHelpID = setTimeout(showHelpOverlay, 1200);
		var frm = current.choices.length-1;
		if (frm==0){
			mc.helpoverlay_mc.title_txt.text = "Select next";
		}else{
			mc.helpoverlay_mc.title_txt.text = "Select your choice";
		}
		//dbLog("initChoices help overlay gotoAndStop:" + frm + " duration:" + mc.helpoverlay_mc.timeline.duration);
		mc.helpoverlay_mc.gotoAndStop(frm);
		help_overlay_shown_count++;
	}
		
	for(var i=0; i<choices.length; i++){
		choices[i].y = 752;
		if (section.section==1 && section.scenario[section.section].speaker=="player" && i==0){
			choices[i].visible = true;
			choices[i].text_txt.text = section.scenario[section.section].dialogue;
			var height = 126;
			var textHeight = choices[i].text_txt.getMeasuredHeight();
			choices[i].title_txt.text = "Choice " + (i+1);
			choices[i].alpha = 1.0;
			choices[i].go_btn.visible = true;
			choices[i].go_btn.removeAllEventListeners();
			choices[i].linkId = choiceData[i].id;
			choices[i].go_btn.removeAllEventListeners();
			choices[i].go_btn.on("click", function(event){
				var mc = event.currentTarget.parent;
				mc.gotoAndStop(1);
				var speaker = section.scenario[section.section].speaker;
				section.section = mc.linkId;//mc.linkId;
				exportRoot.helpoverlay_mc.visible = false;
				mc.go_btn.visible = false;
				continueAfterPlayerChoice();
			});
			setTimeout(slideInChoice, 10);
			initScrollbar( choices[i], height, textHeight, 58.5 );
		}else if (i<current.choices.length){
			//dbLog("initChoices setting choice " + i);
			choices[i].visible = true;
			choices[i].correct = choiceData[i].correct;
			choices[i].score = choiceData[i].score;
			choices[i].scoring = current.choices.length>1;
			choices[i].gotoAndStop(0);
			choices[i].text_txt.text = section.scenario[choiceData[i].id].dialogue;
			var height = 126;
			var textHeight = choices[i].text_txt.getMeasuredHeight();
			choices[i].title_txt.text = "Choice " + (i+1);
			choices[i].linkId = choiceData[i].id;
			choices[i].alpha = 1.0;
			choices[i].go_btn.visible = true;
			choices[i].go_btn.removeAllEventListeners();
			choices[i].go_btn.on("click", function(event){
				var mc = event.currentTarget.parent;
				mc.gotoAndStop(1);
				if (mc.scoring){
					results.count++;
					if (mc.correct) results.correct++;
					results.score += mc.score;
				}
				var speaker = section.scenario[section.section].speaker;
				section.section = (speaker=="player") ? mc.linkId : section.scenario[mc.linkId].choices[0];//mc.linkId;
				exportRoot.helpoverlay_mc.visible = false;
				mc.go_btn.visible = false;
				var mcs = [ exportRoot.choice1_mc, exportRoot.choice2_mc, exportRoot.choice3_mc ];
				for(var i=0; i<mcs.length; i++){
					if (mcs[i]==mc) continue;
					mcs[i].alpha = 0.5;
				}
				setTimeout(continueAfterPlayerChoice, 1500);
			});
			if (i==0){
				setTimeout(slideInChoice, 10);
				dbLog("initChoices text_txt.y:" + choices[i].text_txt.y);
			}
			initScrollbar( choices[i], height, textHeight, 58.5 );
		}else{
			choices[i].visible = false;
		}
	}
	
	choices[0].title_txt.visible = (current.choices.length>1);
	
	if (scorm!=null && scorm.initialized){
		var json = new Object();
		json.completed = completed;
		json.section = section;
		json.results = results;
		json.title = false;
		score.set(JSON.stringify(json));
	}
}

function continueAfterPlayerChoice(){
	exportRoot.gotoAndPlay("person");
}

function initProgress(mc){
	dbLog("initProgress");
}

function initCoaching(mc){
	dbLog("initCoaching");
	if (!sprites.loaded.coach.loaded){
		exportRoot.characters_mc.visible = false;
		if (sprites["coach"]!=null) sprites.coach.visible = false;
		exportRoot.stop();
		exportRoot.loading_mc.visible = true;
		exportRoot.bgs_mc.visible = false;
		setTimeout(checkCoachLoaded, 200);
		showSprite("coach");	
		return;
	}else{
		exportRoot.characters_mc.visible = true;
		sprites.coach.visible = true;
		exportRoot.loading_mc.visible = false;
		exportRoot.bgs_mc.visible = true;
	}
	var score = (results.score/results.count)/2;
	var option = (score>0.6) ? {id:2, txt:"high"} : {id:1, txt:"med"};
	if (score<0.3){
		option = {id:0, txt:"low"};
		exportRoot.coaching_continue_btn.text_txt.text = "Try Again >";
		continue_after_coaching = false;
	}else{
		exportRoot.coaching_continue_btn.text_txt.text = "Continue >";
		continue_after_coaching = true;
	}
	playSound("coach", option.txt);
	sprites.speaking = true;
	
	//mc = exportRoot.coaching_mc;
	mc.text_txt.text = config.coaching[section.person][option.id];
	var height = 271;
	var textHeight = mc.text_txt.getMeasuredHeight();
	initScrollbar(mc, height, textHeight, 65);
	exportRoot.coaching_continue_btn.removeAllEventListeners();
	exportRoot.coaching_continue_btn.on("click", function(e){
		if (speaker_snd!=null) speaker_snd.stop();
		exportRoot.characters_mc.visible = false;
		if (exportRoot.coaching_continue_btn.text_txt.text=="Try Again >"){
			results.correct = results.score = results.count = 0;
			section.section = 1;
			nextSection = "initPerson";	
			exportRoot.play();
			blotter_content = new Array();
		}else{
			showBlotterAfterCoaching();
			clearSprite(section.person);
		}
	});
	
	showSprite("coach");	
	exportRoot.bgs_mc.gotoAndStop(4);
	
	setAnimation("head", "talk", "coach");
	setAnimation("body", "point", "coach");
}

function checkCoachLoaded(){
	if (sprites.loaded["coach"] != null && !sprites.loaded.coach.loaded){
		setTimeout(checkCoachLoaded, 200);
	}else{
		initCoaching(exportRoot.coaching_mc);
		exportRoot.play();
	}
}

function initBlotter(mc){
	dbLog("initBlotter");
	
	var items = new Array();
	
	if (section!=null && section.person!=null && checkCompleted(section.person)){	
		for(var i=0; i<section.scenario.length; i++){
			var current = section.scenario[i];
			if (current.blotter != null && current.blotter!="" && current.blotter_repeat==null){
				items.push({ person:section.person, id:section.section, text:current.blotter, coaching:current.coaching, collected:blotterCollected(section.person, i) });
			}
		}
		showBlotterMessage = true;
	}else{
		showBlotterMessage = false;
		items = blotter_content;
	}
	
	if (mc.items!=null){
		for(var i=0; i<mc.items.length; i++){
			mc.items_mc.removeChild(mc.items[i]);
		}
	}
	
	mc.items = new Array();
	var top;
	var item_mc;
	var itemHeight = 260;
	
	if (items.length==0){
		item_mc = mc.items_mc.item1_mc;
		item_mc.visible = false;
	}else{
		for(var i=0; i<items.length; i++){
			if (i==0){
				item_mc = mc.items_mc.item1_mc;
				item_mc.visible = true;
				top = item_mc.y - 70;
			}else{
				item_mc = new lib.blotter_coaching_item();
				mc.items_mc.addChild(item_mc);
				top += itemHeight;
				item_mc.y = top - 70;
				top -= 50;
				mc.items.push(item_mc);
			}
			item_mc.blotter_txt.text = items[i].text;
			item_mc.coaching_txt.text = items[i].coaching;
			item_mc.gotoAndStop(0);
			item_mc.result_mc.visible = true;
			if (items[i].collected!=null && items[i].collected){
				item_mc.result_mc.gotoAndStop(0);
				//item_mc.result_mc.removeAllEventListeners();
			}else{
				item_mc.result_mc.gotoAndStop(1);
				//item_mc.result_mc.removeAllEventListeners();
				//item_mc.result_mc.on("click", function(event){
				//	var mc = event.currentTarget.parent;
				//	mc.play();
				//});
			}
		}
	}
	
	var height = 467;
	var textHeight = top + itemHeight;
	
	initScrollbar( mc, height, textHeight, 100, true );
}

function initMiniBlotter(mc){
	dbLog("initMiniBlotter");
		
	content = blotter_content[blotter_content.length-1];
	
	mc.items_mc.text_txt.text = content.text
	var height = 458;
	var padding = 20;
	var textHeight = mc.items_mc.text_txt.getMeasuredHeight();
	mc.items_mc.coaching_txt.text = content.coaching;
	mc.items_mc.coaching_txt.y = mc.items_mc.text_txt.y + textHeight + padding;
	textHeight += (mc.items_mc.coaching_txt.getMeasuredHeight() + padding);
	
	initScrollbar( mc, height, textHeight, 350, true );
}

function checkCompleted(name){
	var result = false;
	for(var i=0; i<completed.length; i++){
		if (name == completed[i]){
			result = true;
			break;
		}
	}
	dbLog("checkCompleted name:" + name + " result:" + result);
	return result;
}

function playSound(speaker, id){
	if (speaker_snd!=null) speaker_snd.stop();
	speaker = speaker.substr(0,1).toUpperCase() + speaker.substr(1);
	var	person = section.person.substr(0,1).toUpperCase() + section.person.substr(1);
	if (speaker!=""){
		var snd;
		if (speaker=="Coach"){
			snd = "audio/Coach/" + person + id;
		}else if (id<10){
			snd = "audio/" + speaker + "/" + speaker + "0" + id;
		}else{
			snd = "audio/" + speaker + "/" + speaker + id;
		}
		dbLog("playSound " + snd);
		speaker_snd = new buzz.sound( snd, {formats: [ "mp3" ]});
		sprites.speaking = true;
		var pos = snd.lastIndexOf("/");
		if (pos!=-1) snd = snd.substr(pos + 1);
		dbLog("playSound " + snd);
		if (config.pauses[snd]!=null){
			pauseInfo = { pauses:config.pauses[snd], paused:true, canplay:false };
		}else{
			pauseInfo = null;
		}
		if (exportRoot.loading_mc!=null) exportRoot.loading_mc.visible = true;
		speaker_snd.bind("canplay", function() {
				//dbLog("Sound canplay");
				if (pauseInfo==null){
					setAnimation( "head", "talk" );
				}else{
					pauseInfo.canplay = true;
				}
				if (exportRoot.loading_mc!=null) exportRoot.loading_mc.visible = false;
			})
		speaker_snd.bind("ended", function() {
				//dbLog("Sound ended");
				//if (Math.random()>0.7){
				//	setAnimation( "head", "nod" );
				//}else if (Math.random()>0.7){
				//	setAnimation( "head", "tilt" );
				//}else{
					setAnimation( "head", "static" );
				//}
				sprites.speaking = false;
				if (!sprites.coaching) showChoices();//This will either show the choices or return to the start screen
			});
		if (pauseInfo!=null){
			speaker_snd.bind("timeupdate", function() {
					//dbLog("Sound time:" + this.getTime());
					if (!pauseInfo.canplay) return;
					var tm = this.getTime();
					var paused = false;
					for(var i=0; i<pauseInfo.pauses.length; i++){
						if (tm>pauseInfo.pauses[i].start && tm<pauseInfo.pauses[i].end){
							paused = true;
							break;
						}
					}
					if (paused && !pauseInfo.paused){
						setAnimation("head", "static");
						pauseInfo.paused = true;
						//dbLog("Head static");
					}else if (!paused && pauseInfo.paused){
						setAnimation("head", "talk");
						pauseInfo.paused = false;
						//dbLog("Head talk");
					}
				});
		}
		speaker_snd.play();
	}
}
