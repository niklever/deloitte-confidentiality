<?
	$debug = (empty($_REQUEST['debug'])) ? false : true;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>deloitte_confidentiality</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<script src="http://code.createjs.com/easeljs-0.7.1.min.js"></script>
<script src="http://code.createjs.com/tweenjs-0.5.1.min.js"></script>
<script src="http://code.createjs.com/movieclip-0.7.1.min.js"></script>
<script src="http://code.createjs.com/preloadjs-0.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="deloitte_confidentiality.js"></script>
<script src="activity.js"></script>
<script src="sprites.js"></script>
<script src="buzz.min.js"></script>
<script>


var canvas, stage, exportRoot, progress_el, buzz_snd;
var useDebugFunc = <? echo $debug; ?>;

function isTouchDevice(){
    return ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}

function init() {
	canvas = document.getElementById("canvas");
	images = images||{};

	var loader = new createjs.LoadQueue(false);
	loader.addEventListener("fileload", handleFileLoad);
	loader.addEventListener("complete", handleComplete);
	loader.loadManifest(lib.properties.manifest);
	loader.addEventListener('progress', handleProgress);
	progress_el = document.getElementById("progress");
}

function handleProgress(event) {
	percent = 100*event.progress - 100;
	progress_el.style.left = percent + '%';
	//dbLog("progress " + percent);
}

function handleFileLoad(evt) {
	if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
}

function handleComplete() {
	document.getElementById("progress-bar").style.display = 'none';
	
	exportRoot = new lib.deloitte_confidentiality();

	stage = new createjs.Stage(canvas);
	stage.enableMouseOver();
	stage.addChild(exportRoot);
	if (isTouchDevice()) createjs.Touch.enable(stage);
	
	initSprites();
	stage.update();

	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
}
</script>
<link href="styles.css" rel="stylesheet" type="text/css">
</head>

<body onload="init();" style="background-color:#D4D4D4">
<div id="container">
    <div id="progress-bar">
        <div id="progress"></div>
        <img id="loader" src="images/loader.gif" class="hide" />
    </div>
    <canvas id="canvas" width="1024" height="650" style="background-color:#FFFFFF"></canvas>
    <div id='videoPlaceholder'>
        <video id="video" width="1024" height="620" preload="none" controls>';
            <source src="video/deloitte_confidentiality.mp4" type="video/mp4" />';
            <source src="video/deloitte_confidentiality.webm" type="video/webm />';
            Your browser does not support the video tag.<br />Please upgrade your browser
        </video>
    </div>
</div>
</body>
</html>