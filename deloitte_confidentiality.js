(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 1024,
	height: 650,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/alisontitle.png", id:"alisontitle"},
		{src:"images/BG_Alison.jpg", id:"BG_Alison"},
		{src:"images/BG_Coach.jpg", id:"BG_Coach"},
		{src:"images/BG_James.jpg", id:"BG_James"},
		{src:"images/BG_Kevin.jpg", id:"BG_Kevin"},
		{src:"images/BG_Tammy_VideoCall.png", id:"BG_Tammy_VideoCall"},
		{src:"images/bio_pic_alison.png", id:"bio_pic_alison"},
		{src:"images/bio_pic_james.png", id:"bio_pic_james"},
		{src:"images/bio_pic_kevin.png", id:"bio_pic_kevin"},
		{src:"images/bio_pic_tammy.png", id:"bio_pic_tammy"},
		{src:"images/blotter_bg1.png", id:"blotter_bg1"},
		{src:"images/blotter_bg2.png", id:"blotter_bg2"},
		{src:"images/character1.png", id:"character1"},
		{src:"images/character2.png", id:"character2"},
		{src:"images/character3.png", id:"character3"},
		{src:"images/character4.png", id:"character4"},
		{src:"images/conversation_title_image.png", id:"conversation_title_image"},
		{src:"images/correct.png", id:"correct"},
		{src:"images/deloite_cover.png", id:"deloite_cover"},
		{src:"images/deloitteLogo.png", id:"deloitteLogo"},
		{src:"images/feedback_panel.png", id:"feedback_panel"},
		{src:"images/green_tick.png", id:"green_tick"},
		{src:"images/icon_background.png", id:"icon_background"},
		{src:"images/icon_biographies.png", id:"icon_biographies"},
		{src:"images/icon_blotter.png", id:"icon_blotter"},
		{src:"images/icon_close.png", id:"icon_close"},
		{src:"images/icon_glossary.png", id:"icon_glossary"},
		{src:"images/icon_pause.png", id:"icon_pause"},
		{src:"images/icon_policy.png", id:"icon_policy"},
		{src:"images/icon_question.png", id:"icon_question"},
		{src:"images/incorrect.png", id:"incorrect"},
		{src:"images/kevintitle.png", id:"kevintitle"},
		{src:"images/news_bg.png", id:"news_bg"},
		{src:"images/note_button.png", id:"note_button"},
		{src:"images/pen.png", id:"pen"},
		{src:"images/resources_bg.jpg", id:"resources_bg"},
		{src:"images/tammytitle.png", id:"tammytitle"},
		{src:"images/tick.png", id:"tick"}
	]
};



lib.ssMetadata = [];


lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.alisontitle = function() {
	this.initialize(img.alisontitle);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,519,286);


(lib.BG_Alison = function() {
	this.initialize(img.BG_Alison);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,564);


(lib.BG_Coach = function() {
	this.initialize(img.BG_Coach);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,564);


(lib.BG_James = function() {
	this.initialize(img.BG_James);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,564);


(lib.BG_Kevin = function() {
	this.initialize(img.BG_Kevin);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,564);


(lib.BG_Tammy_VideoCall = function() {
	this.initialize(img.BG_Tammy_VideoCall);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,564);


(lib.bio_pic_alison = function() {
	this.initialize(img.bio_pic_alison);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,163,171);


(lib.bio_pic_james = function() {
	this.initialize(img.bio_pic_james);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,153,161);


(lib.bio_pic_kevin = function() {
	this.initialize(img.bio_pic_kevin);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,177,171);


(lib.bio_pic_tammy = function() {
	this.initialize(img.bio_pic_tammy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,152,161);


(lib.blotter_bg1 = function() {
	this.initialize(img.blotter_bg1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,388,634);


(lib.blotter_bg2 = function() {
	this.initialize(img.blotter_bg2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,653);


(lib.character1 = function() {
	this.initialize(img.character1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,232,369);


(lib.character2 = function() {
	this.initialize(img.character2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,209,373);


(lib.character3 = function() {
	this.initialize(img.character3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,245,375);


(lib.character4 = function() {
	this.initialize(img.character4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,258,385);


(lib.conversation_title_image = function() {
	this.initialize(img.conversation_title_image);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,510,286);


(lib.correct = function() {
	this.initialize(img.correct);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,38,40);


(lib.deloite_cover = function() {
	this.initialize(img.deloite_cover);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,728);


(lib.deloitteLogo = function() {
	this.initialize(img.deloitteLogo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,306,57);


(lib.feedback_panel = function() {
	this.initialize(img.feedback_panel);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,173);


(lib.green_tick = function() {
	this.initialize(img.green_tick);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,32,27);


(lib.icon_background = function() {
	this.initialize(img.icon_background);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,45,38);


(lib.icon_biographies = function() {
	this.initialize(img.icon_biographies);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,30,38);


(lib.icon_blotter = function() {
	this.initialize(img.icon_blotter);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,28,38);


(lib.icon_close = function() {
	this.initialize(img.icon_close);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,32,32);


(lib.icon_glossary = function() {
	this.initialize(img.icon_glossary);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,38);


(lib.icon_pause = function() {
	this.initialize(img.icon_pause);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,32,32);


(lib.icon_policy = function() {
	this.initialize(img.icon_policy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,37,38);


(lib.icon_question = function() {
	this.initialize(img.icon_question);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,24,32);


(lib.incorrect = function() {
	this.initialize(img.incorrect);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,38,40);


(lib.kevintitle = function() {
	this.initialize(img.kevintitle);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,519,286);


(lib.news_bg = function() {
	this.initialize(img.news_bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,844,585);


(lib.note_button = function() {
	this.initialize(img.note_button);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,160,159);


(lib.pen = function() {
	this.initialize(img.pen);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,129,248);


(lib.resources_bg = function() {
	this.initialize(img.resources_bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,653);


(lib.tammytitle = function() {
	this.initialize(img.tammytitle);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,519,286);


(lib.tick = function() {
	this.initialize(img.tick);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,22,25);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.resources_bg();
	this.instance.setTransform(-512,-326.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-512,-326.5,1024,653);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.resources_bg();
	this.instance.setTransform(-512,-326.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-512,-326.5,1024,653);


(lib.try_again = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("Complete", "bold 34px 'Frutiger Next Pro Bold'", "#565656");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 176;
	this.text.setTransform(106.5,23.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#81BC00").ss(6,1,1).p("AvLm1IeXAAQBkAAAABkIAAKjQAABkhkAAI+XAAQhkAAAAhkIAAqjQAAhkBkAAg");
	this.shape.setTransform(107.2,43.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FBFBFB").s().p("AvLG1QhkABAAhlIAAqiQAAhjBkgBIeXAAQBkABAABjIAAKiQAABlhkgBg");
	this.shape_1.setTransform(107.2,43.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3,-3,220.5,93.6);


(lib.topBarBg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhP/AEXIAAouMCf/AAAIAAIug");
	this.shape.setTransform(512,28);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1024,56);


(lib.thumb = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2092BC").s().p("AgiCcQgPgNAAgSIAAj5QAAgSAPgOQAPgNATAAQAUAAAOANQAQAOgBASIAAD5QABASgQANQgOAOgUAAQgTAAgPgOg");
	this.shape.setTransform(5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10,34);


(lib.select_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("Begin your interview now:", "45px 'Frutiger Next Pro Medium'", "#002775");
	this.text.lineHeight = 47;
	this.text.lineWidth = 532;
	this.text.setTransform(-48,2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#002775").ss(7.5,2,1).p("EgnyAAAMBPlAAA");
	this.shape.setTransform(209.6,50.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50,0,536,54.4);


(lib.scrollBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2092BC").s().p("AhaBiIBYjDIBdDDg");
	this.shape.setTransform(9.1,9.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,18.3,19.8);


(lib.resources_bg_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.resources_bg();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1024,653);


(lib.progress_bar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(50));

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AnxA9IAAh5IPbAAIAAB5g");
	var mask_graphics_1 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_2 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_3 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_4 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_5 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_6 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_7 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_8 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_9 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_10 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_11 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_12 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_13 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_14 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_15 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_16 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_17 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_18 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_19 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_20 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_21 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_22 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_23 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_24 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_25 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_26 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_27 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_28 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_29 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_30 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_31 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_32 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_33 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_34 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_35 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_36 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_37 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_38 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_39 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_40 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_41 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_42 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_43 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_44 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_45 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_46 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_47 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_48 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");
	var mask_graphics_49 = new cjs.Graphics().p("AntA9IAAh5IPbAAIAAB5g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-49.8,y:6.2}).wait(1).to({graphics:mask_graphics_1,x:-48.1,y:6.2}).wait(1).to({graphics:mask_graphics_2,x:-46.1,y:6.2}).wait(1).to({graphics:mask_graphics_3,x:-44.1,y:6.2}).wait(1).to({graphics:mask_graphics_4,x:-42,y:6.2}).wait(1).to({graphics:mask_graphics_5,x:-40,y:6.2}).wait(1).to({graphics:mask_graphics_6,x:-38,y:6.2}).wait(1).to({graphics:mask_graphics_7,x:-35.9,y:6.2}).wait(1).to({graphics:mask_graphics_8,x:-33.9,y:6.2}).wait(1).to({graphics:mask_graphics_9,x:-31.9,y:6.2}).wait(1).to({graphics:mask_graphics_10,x:-29.8,y:6.2}).wait(1).to({graphics:mask_graphics_11,x:-27.8,y:6.2}).wait(1).to({graphics:mask_graphics_12,x:-25.8,y:6.2}).wait(1).to({graphics:mask_graphics_13,x:-23.8,y:6.2}).wait(1).to({graphics:mask_graphics_14,x:-21.7,y:6.2}).wait(1).to({graphics:mask_graphics_15,x:-19.7,y:6.2}).wait(1).to({graphics:mask_graphics_16,x:-17.7,y:6.2}).wait(1).to({graphics:mask_graphics_17,x:-15.6,y:6.2}).wait(1).to({graphics:mask_graphics_18,x:-13.6,y:6.2}).wait(1).to({graphics:mask_graphics_19,x:-11.6,y:6.2}).wait(1).to({graphics:mask_graphics_20,x:-9.5,y:6.2}).wait(1).to({graphics:mask_graphics_21,x:-7.5,y:6.2}).wait(1).to({graphics:mask_graphics_22,x:-5.5,y:6.2}).wait(1).to({graphics:mask_graphics_23,x:-3.4,y:6.2}).wait(1).to({graphics:mask_graphics_24,x:-1.4,y:6.2}).wait(1).to({graphics:mask_graphics_25,x:0.6,y:6.2}).wait(1).to({graphics:mask_graphics_26,x:2.7,y:6.2}).wait(1).to({graphics:mask_graphics_27,x:4.7,y:6.2}).wait(1).to({graphics:mask_graphics_28,x:6.7,y:6.2}).wait(1).to({graphics:mask_graphics_29,x:8.8,y:6.2}).wait(1).to({graphics:mask_graphics_30,x:10.8,y:6.2}).wait(1).to({graphics:mask_graphics_31,x:12.8,y:6.2}).wait(1).to({graphics:mask_graphics_32,x:14.9,y:6.2}).wait(1).to({graphics:mask_graphics_33,x:16.9,y:6.2}).wait(1).to({graphics:mask_graphics_34,x:18.9,y:6.2}).wait(1).to({graphics:mask_graphics_35,x:21,y:6.2}).wait(1).to({graphics:mask_graphics_36,x:23,y:6.2}).wait(1).to({graphics:mask_graphics_37,x:25,y:6.2}).wait(1).to({graphics:mask_graphics_38,x:27.1,y:6.2}).wait(1).to({graphics:mask_graphics_39,x:29.1,y:6.2}).wait(1).to({graphics:mask_graphics_40,x:31.1,y:6.2}).wait(1).to({graphics:mask_graphics_41,x:33.2,y:6.2}).wait(1).to({graphics:mask_graphics_42,x:35.2,y:6.2}).wait(1).to({graphics:mask_graphics_43,x:37.2,y:6.2}).wait(1).to({graphics:mask_graphics_44,x:39.3,y:6.2}).wait(1).to({graphics:mask_graphics_45,x:41.3,y:6.2}).wait(1).to({graphics:mask_graphics_46,x:43.3,y:6.2}).wait(1).to({graphics:mask_graphics_47,x:45.4,y:6.2}).wait(1).to({graphics:mask_graphics_48,x:47.4,y:6.2}).wait(1).to({graphics:mask_graphics_49,x:49.4,y:6.2}).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BCD302").s().p("AntA9IAAh5IPbAAIAAB5g");
	this.shape.setTransform(49.4,6.2);

	this.shape.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(50));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C8B2D").s().p("AntA9IAAh5IPbAAIAAB5g");
	this.shape_1.setTransform(49.4,6.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,98.9,12.4);


(lib.pen_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.pen();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,129,248);


(lib.panel_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(86,86,86,0.008)").s().p("Egj6ATrMAAAgnVMBH1AAAMAAAAnVg");
	this.shape.setTransform(230,153.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,27.5,460,252);


(lib.panel_bg_alpha70 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.698)").s().p("EglmAb7MAAAg31MBLNAAAMAAAA31g");
	this.shape.setTransform(240.7,178.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,481.4,357.5);


(lib.opening_screen_bottom = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("An online simulation \nfor understanding the \nimportance of \nconfidentiality and \nDeloitte business", "18px 'Frutiger Next Pro Light'", "#FFFFFF");
	this.text.lineHeight = 20;
	this.text.lineWidth = 170;
	this.text.setTransform(483.1,45.6);

	this.text_1 = new cjs.Text("Confidentiality", "59px 'Frutiger Next Pro Light'", "#FFFFFF");
	this.text_1.lineHeight = 61;
	this.text_1.lineWidth = 354;
	this.text_1.setTransform(28.7,108.2);

	this.instance = new lib.deloitteLogo();
	this.instance.setTransform(37,36);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AAArEIAAWJ");
	this.shape.setTransform(431.2,103.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance},{t:this.text_1},{t:this.text}]}).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A2DE").s().p("EhP/AZdMAAAgy5MCf/AAAMAAAAy5g");
	this.shape_1.setTransform(512,163);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1024,326);


(lib.opening_bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.deloite_cover();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1024,728);


(lib.OKBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("YES", "bold 18px 'Frutiger Next Pro Bold'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 18;
	this.text.lineWidth = 100;
	this.text.setTransform(53.9,6.8);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(4));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DE").s().p("AofCbIAAk1IQ/AAIAAE1g");
	this.shape.setTransform(54.5,15.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00B4F5").s().p("AofCbIAAk1IQ/AAIAAE1g");
	this.shape_1.setTransform(54.5,15.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,108.9,32.8);


(lib.NoBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("NO", "bold 18px 'Frutiger Next Pro Bold'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 18;
	this.text.lineWidth = 100;
	this.text.setTransform(53.9,6.8);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(4));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DE").s().p("AofCbIAAk1IQ/AAIAAE1g");
	this.shape.setTransform(54.5,15.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00B4F5").s().p("AofCbIAAk1IQ/AAIAAE1g");
	this.shape_1.setTransform(54.5,15.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,108.9,32.8);


(lib.neuralConnection = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFF00").ss(1,1,1).p("ABaAAQAAAjgbAZQgaAZglAAQgkAAgbgZQgagZAAgjQAAgiAagZQAbgZAkAAQAlAAAaAZQAbAZAAAig");
	this.shape.setTransform(9,8.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FBFBFB").s().p("Ag+A7QgbgZAAgiQAAghAbgZQAagaAkAAQAlAAAaAaQAbAZAAAhQAAAigbAZQgaAaglAAQgkAAgagag");
	this.shape_1.setTransform(9,8.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,20,19);


(lib.missed_correct = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.tick();

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,22,25);


(lib.miniBlotterPanelMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.coaching_txt = new cjs.Text("Blotter text goes here", "italic 18px 'Frutiger Next Pro'", "#082C77");
	this.coaching_txt.name = "coaching_txt";
	this.coaching_txt.lineHeight = 20;
	this.coaching_txt.lineWidth = 341;
	this.coaching_txt.setTransform(2,228);

	this.text_txt = new cjs.Text("Blotter text goes here", "18px 'Frutiger Next Pro Medium'", "#082C77");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 20;
	this.text_txt.lineWidth = 341;
	this.text_txt.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_txt},{t:this.coaching_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,345,451);


(lib.marked = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.correct();

	this.instance_1 = new lib.incorrect();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,38,40);


(lib.loadingMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#999999").s().p("AgJK0QgeAAgdgDIAAksQAdAFAeAAIAJAAIAegBIAAEqIgeABIgJAAgABUGCQAdgFAcgLQATgIASgIIBzETIglAQQgcALgcAJgAkiJ2QgagMgbgQIB8kRQAZAQAbANQATAJATAHIh8EQIglgQgADiFEQAXgQAVgVIAIgIIAWgXIDSDTIgVAWIgIAJQgWAUgVASgAnpHpIgNgOQgUgVgSgVIDZjOQARAWATAWIAJAIIAXAWIjYDNIgSgRgAFPDSQAPgXALgZQAJgWAHgVIEUByIgRArQgLAagMAYgAqGD6QgJgZgIgZIEYhrQAGAaAJAZQAJAWALAUIkXBrIgTgrgAGHA5QADgYAAgaIAAgHQAAgUgCgVIErAAIABApIAAAHIgCAygAqxA5QgCgYAAgaIAAgHQAAgUACgVIEqAAQgCAVAAAUIAAAHQAAAaADAYgAF1iDQgKgZgMgYIEahnQALAYAJAZQAIAWAGAWIkaBoQgFgXgHgWgAqTjVQAIgaAKgZIATgqIEUB3QgMAUgIAVQgKAZgGAbgAoTm6QASgVAVgVIADgFIAbgZIDRDVIgZAXIgFAGQgVAWgQAXgAEbkRIgFgFQgSgSgUgPIDYjQIAhAeIAJAJIAdAgIjYDQQgMgRgQgQgAlWpaQAagPAbgNIAlgPIB5ESQgTAGgSAKQgbAMgZARgACblrQgbgLgegHIB0kEQADgJAHgGQAsAPAqAVQAAAKgFAKIhxD/QgTgJgSgJgAhEqpIAAgHQAigDAiAAIAdABIABAJIAAEhIgegBIgJAAQgeAAgdAGg");
	this.shape.setTransform(69.3,69.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,138.5,138.5);


(lib.knowledgecheckimage = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer 1
	this.instance = new lib.bio_pic_james();
	this.instance.setTransform(0,24.5,1,1,-9.2);

	this.instance_1 = new lib.bio_pic_alison();
	this.instance_1.setTransform(8,5.8);

	this.instance_2 = new lib.bio_pic_tammy();
	this.instance_2.setTransform(0,24.5,1,1,-9.2);

	this.instance_3 = new lib.bio_pic_kevin();
	this.instance_3.setTransform(-0.4,6.5,1,1,-0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,176.8,183.4);


(lib.knowledge_check_question = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text_txt = new cjs.Text("Why is maintaining confidentiality a business imperative for \nDeloitte?", "bold 26px 'Frutiger Next Pro Bold'", "#002874");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 26;
	this.text_txt.lineWidth = 777;
	this.text_txt.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,781.2,107.7);


(lib.knowledge_check_header = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text_txt = new cjs.Text("KNOWLEDGE CHECK: Question 1 of 2", "bold 19px 'Frutiger Next Pro Bold'", "#575757");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 19;
	this.text_txt.lineWidth = 408;
	this.text_txt.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,412.5,27);


(lib.introMessageG = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("LEARNING OBJECTIVES", "bold 27px 'Frutiger Next Pro Bold'", "#666666");
	this.text.lineHeight = 29;
	this.text.lineWidth = 314;
	this.text.setTransform(0,-35.5);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 1
	this.intro_message_txt = new cjs.Text("Upon completion of this course, you should be able to:\n\n·                Recognize situations that may constitute “insider trading” including “tipping.”\n\n·                Identify steps to take to assess obligations on Deloitte by an NDA or click through agreements.\n\n·                Identify steps to take to assess organizational conflicts with a Target.\n\n·                Recognize situations where personal conflicts may exist and state how such conflicts should be addressed.\n\n·                Identify risks associated with virtual data rooms.\n\n·                Recognize what constitutes competitively sensitive information, state the typical protocols for dealing with\n                 competitively sensitive information, and recall why such protocols are important.\n\n·                Identify provisions that could be included in a non-disclosure agreement to which Deloitte is subject.\n\n·                Identify typical provisions in an M&A engagement letter that are designed to protect Deloitte.\n\n·                Recognize what constitutes confidential information and what consents are required to disclose such information.\n\n·                Recognize steps to take to preserve client confidential information and the need for incremental risk mitigation steps.\n\n·                Define the term “ethical walls” and recognize their importance with respect to maintaining confidentiality.\n", "16px 'Frutiger Next Pro Medium'", "#999999");
	this.intro_message_txt.name = "intro_message_txt";
	this.intro_message_txt.lineHeight = 18;
	this.intro_message_txt.lineWidth = 907;
	this.intro_message_txt.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.intro_message_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-37.5,913,544.2);


(lib.helpOverlayMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{multiple:2});

	// Layer 2
	this.title_txt = new cjs.Text("Select your choice ", "24px 'Frutiger Next Pro Medium'", "#333333");
	this.title_txt.name = "title_txt";
	this.title_txt.textAlign = "center";
	this.title_txt.lineHeight = 26;
	this.title_txt.lineWidth = 482;
	this.title_txt.setTransform(382,15.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#999999").ss(7.6,1,1).p("A9gnjIAAiWQAAhkBkAAMBLwAAAQBkAAAABkIAAFeQAABkhkAAMhLwAAAQhkAAAAhkgEgmHAF7QAACThpBoQhqBoiVAAQiVAAhphoQhqhoAAiTQAAiTBqhoQBphoCVAAQCVAABqBoQBpBoAACTgEgsCgADIAAkYQAYjEDYgEIKyAA");
	this.shape.setTransform(316,73.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Egl3AESQhkAAAAhjIAAjGIAAiXQAAhjBkgBMBLvAAAQBkABAABjIAAFdQAABjhkAAg");
	this.shape_1.setTransform(379.5,27.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.title_txt}]}).wait(3));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#999999").ss(7.6,1,1).p("AFoBoQAACThqBoQhqBoiUAAQiTAAhqhoQhqhoAAiTQAAiRBqhoQBqhoCTAAQCUAABqBoQBqBoAACRgAAAkMIAAi+");
	this.shape_2.setTransform(365,101);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#999999").ss(7.6,1,1).p("AZtheQCOAEBmBjQBqBnAACUQAACThqBnQhqBpiUAAQiVAAhqhpQhphnAAiTQAAiUBphnQBqhnCVAAQAFAAAFAAgAQLpmIFyAAQEDANgTC7IAAFAAz6D5QAACThpBoQhqBoiVAAQiUAAhqhoQhqhoAAiTQAAiTBqhmQBqhoCUAAQCVAABqBoQBpBmAACTgA5hh8IAAi+");
	this.shape_3.setTransform(528.5,86.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.8,-3.8,639.6,154.6);


(lib.green_tick_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.green_tick();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32,27);


(lib.goBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B9DA32").s().p("Aiui9IAHgKIFWDQIldC+g");
	this.shape.setTransform(17.5,20);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFF00").ss(3.4,1,1).p("Aiui9IAAGFIFdi+IlWjRg");
	this.shape_1.setTransform(17.5,20);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape},{t:this.shape_1}]},1).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,35,40);


(lib.glossary_item = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.desc_txt = new cjs.Text("A term describing a technique that allows merging rivals to share competitively sensitive information about their \nbusinesses without violating antitrust laws.  Because the merging companies cannot share such information with one \nanother before the deal closes, the “clean room” utilizes outside consultants and (in some cases) restricted members of \nmanagement, operating under the direction of antitrust counsel, to meet individually with the two parties to conduct \ndiligence and formulate plans on sensitive areas of the merger, such as product prices.", "16px 'Frutiger Next Pro Medium'", "#575757");
	this.desc_txt.name = "desc_txt";
	this.desc_txt.lineHeight = 16;
	this.desc_txt.lineWidth = 841;
	this.desc_txt.setTransform(46,36.5);

	this.title_txt = new cjs.Text("Clean room", "bold 20px 'Frutiger Next Pro Bold'", "#002874");
	this.title_txt.name = "title_txt";
	this.title_txt.lineHeight = 22;
	this.title_txt.lineWidth = 837;
	this.title_txt.setTransform(46,2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#81BC00").ss(1,2,1).p("EhP/AAAMCf/AAA");
	this.shape.setTransform(512,153.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.title_txt},{t:this.desc_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,0,1026,154.3);


(lib.finalassessmenttitle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text_txt = new cjs.Text("FINAL ASSESSMENT: Question 1 of 20", "bold 19px 'Frutiger Next Pro Bold'", "#00A2DE");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 19;
	this.text_txt.lineWidth = 408;
	this.text_txt.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,412.5,27);


(lib.finalassessmentquestion = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#81BC00").ss(1,2,1).p("EhHkAAAMCPJAAA");
	this.shape.setTransform(458.7,149.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.text_txt = new cjs.Text("Which of the following statements is true with respect to risk mitigation steps we should take to comply with \nthe terms of an NDA signed between the buyer and seller?", "bold 17px 'Frutiger Next Pro Bold'", "#002874");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 19;
	this.text_txt.lineWidth = 912;
	this.text_txt.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,0,918.3,150.9);


(lib.faInstructionsMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		if (finalExamCount==null || finalExamCount<3){
			this.stop();
		}else{
			this.gotoAndStop(1);
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 1
	this.text = new cjs.Text("The course assessment will test your understanding of the course content. The answers you submit during this assessment are graded. You must achieve a passing score of at least 70% to receive CPE credit for the course.\n\nYou have three (3) chances to submit the exam for at least a 70% passing grade. Your scores will automatically be captured by the Deloitte Learning Center, so it is not necessary to record or document your scores manually.\n\nYou will need to complete the exam in one sitting. If you exit the exam in the middle OR you navigate to another page in the course, your answers will be lost. After you submit the exam, detailed feedback is not provided, including which questions were answered correctly or incorrectly, in order to comply with NASBA guidelines for CPE credit. If you do not score at least 70% or higher, please return and review those sections of the course with which you may have had difficulty.\n\nIf you are unable to pass the final exam in three attempts, please send an e-mail to  US DeloitteLearning Center (US - Wilton) to request that your course be reset . If you encounter any technical problems, please contact the Call Center. (The US Call Center number is 1-800-DELOITTE, #2).\n\nNASBA guidelines require that you complete the final exam for this course within one year of enrolling. If you do not comply with this guideline, you will not receive CPE credit for your work and will have to complete the entire course again.", "22px 'Frutiger Next Pro Medium'", "#2092BC");
	this.text.textAlign = "center";
	this.text.lineHeight = 23;
	this.text.lineWidth = 960;
	this.text.setTransform(319,-68);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1).to({x:306.5,y:68,text:"\nYou have been unable to pass the final exam in three attempts, \nplease send an e-mail to US DeloitteLearning Center (US - Wilton) \nto request that your course be reset. \n\nIf you encounter any technical problems, please contact the Call Center. (The US Call Center number is 1-800-DELOITTE, #2).\n",lineWidth:733},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-162.9,-70,963.9,853);


(lib.etextMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.result_txt = new cjs.Text("This is the text that will be displayed.", "bold 36px 'Frutiger Next Pro Bold'", "#FFFFFF");
	this.result_txt.name = "result_txt";
	this.result_txt.textAlign = "center";
	this.result_txt.lineHeight = 37;
	this.result_txt.lineWidth = 752;
	this.result_txt.setTransform(415,216.1);

	this.text_txt = new cjs.Text("This is the text that will be displayed.", "36px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_txt.name = "text_txt";
	this.text_txt.textAlign = "center";
	this.text_txt.lineHeight = 37;
	this.text_txt.lineWidth = 899;
	this.text_txt.setTransform(421.3,-111.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_txt},{t:this.result_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30.4,-113.9,903.4,445.9);


(lib.epilogueBG = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EhQDAwgMAAAhg/MCgHAAAMAAABg/g");
	this.shape.setTransform(512.5,310.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1025,621);


(lib.courseFailMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("You didn't score enough to pass the course.\n\nYou are only allowed three attempts to pass the final assessment.  If you do not pass the test within three attempts, you will not be granted CPE credit for the course.  Please exit the course entirely, by clicking the Finish button on the Quiz Results page or the Exit button on the upper right of your screen and then re-launch the course.  When you re-launch the course, answer ‘no’ to the question ‘Would you like to resume your presentation?’ in order to re-start the course entirely.", "24px 'Frutiger Next Pro Medium'", "#2092BC");
	this.text.textAlign = "center";
	this.text.lineHeight = 25;
	this.text.lineWidth = 616;
	this.text.setTransform(197,-430.2);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 1
	this.score_txt = new cjs.Text("0%", "42px 'Frutiger Next Pro Medium'", "#00A2DD");
	this.score_txt.name = "score_txt";
	this.score_txt.lineHeight = 43;
	this.score_txt.lineWidth = 130;
	this.score_txt.setTransform(191.9,-72.3);

	this.text_1 = new cjs.Text("Score:", "42px 'Frutiger Next Pro Medium'", "#00A2DD");
	this.text_1.textAlign = "right";
	this.text_1.lineHeight = 43;
	this.text_1.lineWidth = 130;
	this.text_1.setTransform(171,-72.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_1},{t:this.score_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113,-432.2,620,404);


(lib.conversation_title_image_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer 1
	this.instance = new lib.conversation_title_image();

	this.instance_1 = new lib.alisontitle();

	this.instance_2 = new lib.tammytitle();

	this.instance_3 = new lib.kevintitle();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,510,286);


(lib.conversation_title = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text_txt = new cjs.Text("AERS Advisory Partner, M&A Transaction Services (\"MATS\"), New York Conversation Between James Simmons and Player", "24px 'Frutiger Next Pro Medium'", "#002775");
	this.text_txt.name = "text_txt";
	this.text_txt.textAlign = "right";
	this.text_txt.lineHeight = 26;
	this.text_txt.lineWidth = 431;
	this.text_txt.setTransform(433,-37);

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-39,435,106);


(lib.conversation_character = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.closePopupBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("X", "bold 17px 'Frutiger Next Pro Bold'", "#FFFFFF");
	this.text.lineHeight = 19;
	this.text.lineWidth = 12;
	this.text.setTransform(8,7);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DD").s().p("AheBfQgogoAAg3QAAg2AogoQAogoA2AAQA3AAAoAoQAoAoAAA2QAAA3goAoQgoAog3AAQg2AAgogog");
	this.shape.setTransform(13.5,13.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,27,27);


(lib.closePanelBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DD").s().p("AoCHVIH1uqIIPOqg");
	this.shape.setTransform(51.5,47);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,103,94);


(lib.btn_start = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("start now  >", "24px 'Frutiger Next Pro Medium'", "#003A74");
	this.text.lineHeight = 26;
	this.text.lineWidth = 140;
	this.text.setTransform(28.9,5.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AtxDEIAAmHIbjAAIAAGHg");
	this.shape.setTransform(88.2,19.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,176.5,39.2);


(lib.btn_skip = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("skip  >", "24px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text.textAlign = "right";
	this.text.lineHeight = 26;
	this.text.lineWidth = 75;
	this.text.setTransform(144,8.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DE").s().p("AnQDEIAAmHIOhAAIAAGHg");
	this.shape.setTransform(107.5,19.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(61,0,93.1,39.2);


(lib.btn_play = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag1hLIBsBOIhsBJg");
	this.shape.setTransform(19.1,18.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8FC8E6").s().p("Ai5C5IAAlyIFyAAIAAFygAgxBHIBshHIhshQg");
	this.shape_1.setTransform(18.6,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,37.2,37.2);


(lib.btn_pause = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.icon_pause();
	this.instance.setTransform(2.7,2.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8FC8E6").s().p("Ai5C5IAAlyIFyAAIAAFyg");
	this.shape.setTransform(18.6,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,37.2,37.2);


(lib.btn_help = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.icon_question();
	this.instance.setTransform(6.6,2.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B6DCEE").s().p("Ai5C5IAAlyIFyAAIAAFyg");
	this.shape.setTransform(18.6,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,37.2,37.2);


(lib.btn_finish = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text_txt = new cjs.Text("finish  >", "24px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_txt.name = "text_txt";
	this.text_txt.textAlign = "right";
	this.text_txt.lineHeight = 26;
	this.text_txt.lineWidth = 126;
	this.text_txt.setTransform(120.1,7.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DE").s().p("AogDEIAAmHIRBAAIAAGHg");
	this.shape.setTransform(75.5,19.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.text_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,0,138,39.2);


(lib.btn_continuefa = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("continue  >", "24px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text.lineHeight = 26;
	this.text.lineWidth = 126;
	this.text.setTransform(19.1,4.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DE").s().p("AryDEIAAmHIXlAAIAAGHg");
	this.shape.setTransform(75.5,19.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,151.1,39.2);


(lib.btn_continue_feedback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text_txt = new cjs.Text("continue  >", "18px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_txt.name = "text_txt";
	this.text_txt.textAlign = "center";
	this.text_txt.lineHeight = 20;
	this.text_txt.lineWidth = 126;
	this.text_txt.setTransform(51.2,-16,0.686,0.963);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,2,1).p("AnzhvIPnAAIAADfIvnAAg");
	this.shape.setTransform(50,-4.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A2DE").s().p("AnzBwIAAjfIPmAAIAADfg");
	this.shape_1.setTransform(50,-4.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-17.9,102,31.2);


(lib.btn_continue = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text_txt = new cjs.Text("continue  >", "24px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 26;
	this.text_txt.lineWidth = 126;
	this.text_txt.setTransform(19.1,4.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DE").s().p("AryDEIAAmHIXlAAIAAGHg");
	this.shape.setTransform(75.5,19.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.text_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,151.1,39.2);


(lib.btn_close_main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(4,1,1).p("AhQg3ICgAAAhQAAICgAAAhPA4ICgAA");
	this.shape.setTransform(18.6,17.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A2DE").s().p("Ai5C5IAAlyIFyAAIAAFyg");
	this.shape_1.setTransform(18.6,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,37.2,37.2);


(lib.btn_close = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.icon_close();
	this.instance.setTransform(99.3,4.6);

	this.text = new cjs.Text("close  ", "24px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text.lineHeight = 26;
	this.text.lineWidth = 57;
	this.text.setTransform(41.1,4.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00A2DE").s().p("AryDEIAAmHIXlAAIAAGHg");
	this.shape.setTransform(75.5,19.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.text},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,151.1,39.2);


(lib.blotter_panel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("You have missed the note from this part of the \nconversation", "14px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text.lineHeight = 14;
	this.text.lineWidth = 308;
	this.text.setTransform(16.1,57.4);

	this.text_1 = new cjs.Text("You have missed the note from this part of the \nconversation:", "14px 'Frutiger Next Pro Medium'", "#002775");
	this.text_1.lineHeight = 14;
	this.text_1.lineWidth = 308;
	this.text_1.setTransform(16.1,12.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_1},{t:this.text}]}).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B7E4").s().p("A6qK2IAA1sMA1VAAAIAAVsg");
	this.shape.setTransform(170.7,69.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,341.5,139);


(lib.blotter_full_screen = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Blotter point revealed", "16px 'Frutiger Next Pro Medium'", "#55B5E5");
	this.text.lineHeight = 18;
	this.text.lineWidth = 95;
	this.text.setTransform(739.8,75.3);

	this.instance = new lib.tick();
	this.instance.setTransform(716,73.3);

	this.text_1 = new cjs.Text("Blotter", "bold 34px 'Frutiger Next Pro Bold'", "#082C77");
	this.text_1.lineHeight = 36;
	this.text_1.lineWidth = 120;
	this.text_1.setTransform(62.1,66.5);

	this.text_2 = new cjs.Text("Coaching", "26px 'Frutiger Next Pro Medium'", "#082C77");
	this.text_2.lineHeight = 28;
	this.text_2.lineWidth = 116;
	this.text_2.setTransform(495.6,71.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.instance},{t:this.text}]}).wait(1));

	// Layer 1
	this.instance_1 = new lib.blotter_bg2();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1024,653);


(lib.BlankBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.blotter_bg1, null, new cjs.Matrix2D(1,0,0,1,-194,-317)).s().p("AnBCaIAAk0IODAAIAAE0g");
	this.shape.setTransform(45,15.5);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.biography = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.biog_txt = new cjs.Text("James is an accounting partner in Deloitte’s M&A Transaction Services practice in New York City. He \njoined the firm 4 years ago from Johnstone, the Boston accounting firm where he also earned his \nCPA. Prior to Johnstone, James was at Boston College studying for his MBA in finance. Originally, his \nplans were to become a lawyer and go into his father’s law firm, but he decided in his junior year \nthat he wanted to start making money and make a name on his own.\n\nJames is regarded within Deloitte in the States as a rising star with considerable potential as an oil \nand gas M&A specialist. He is excellent at the nuts and bolts of M&A work – financial due diligence, \nidentifying potential deal breakers and deal execution. He is respected for his skills in this area, \nalthough at times can be demanding of colleagues and impatient with clients. Often his style comes \nacross as uncompromising, and his desire to get the deal done causes him to ignore the important \ndetails.\n\nJames has worked with Matt Green, Head of Business Development for WEC US, on four of WEC’s \nlast seven global acquisitions, but this is the first divestment. James and Matt have a good working \nrelationship. Around the same age and with similar ambitions, they see each other as being able to \nhelp their mutual career goals. \n\nJames has worked closely with Kevin Short, who also has been involved in all the recent WEC \nacquisitions. Kevin has positioned himself as a valuable mentor to James, but this has not always \nbeen readily received. James has felt that Kevin is too protective of his relationship with WEC and \ntoo methodical in his business dealings, which James feels has been a hindrance to getting the deals \ndone.", "16px 'Frutiger Next Pro Medium'", "#575757");
	this.biog_txt.name = "biog_txt";
	this.biog_txt.lineHeight = 16;
	this.biog_txt.lineWidth = 704;
	this.biog_txt.setTransform(2,90.4);

	this.position_txt = new cjs.Text("US M&A Transaction Services Engagement Leader\nWEC, New York", "20px 'Frutiger Next Pro Medium'", "#002874");
	this.position_txt.name = "position_txt";
	this.position_txt.lineHeight = 20;
	this.position_txt.lineWidth = 636;
	this.position_txt.setTransform(2,36.8);

	this.name_txt = new cjs.Text("James Simmons", "bold 26px 'Frutiger Next Pro Bold'", "#002874");
	this.name_txt.name = "name_txt";
	this.name_txt.lineHeight = 26;
	this.name_txt.lineWidth = 630;
	this.name_txt.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.name_txt},{t:this.position_txt},{t:this.biog_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,707.7,547.2);


(lib.bg1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 1
	this.instance = new lib.BG_James();
	this.instance.setTransform(-0.1,55);

	this.instance_1 = new lib.BG_Alison();
	this.instance_1.setTransform(-0.1,55);

	this.instance_2 = new lib.BG_Tammy_VideoCall();
	this.instance_2.setTransform(0,54.4);

	this.instance_3 = new lib.BG_Kevin();
	this.instance_3.setTransform(-0.1,55);

	this.instance_4 = new lib.BG_Coach();
	this.instance_4.setTransform(-0.1,55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.1,55,1024,564);


(lib.assessment_panel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(68,68,68,0.008)").s().p("EhHxAFUIAAqnMCPjAAAIAAKng");
	this.shape.setTransform(458.5,33.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// Layer 4
	this.order_txt = new cjs.Text("1", "bold 35px 'Frutiger Next Pro Bold'", "#575757");
	this.order_txt.name = "order_txt";
	this.order_txt.lineHeight = 36;
	this.order_txt.lineWidth = 29;
	this.order_txt.setTransform(875,3.8);

	this.timeline.addTween(cjs.Tween.get(this.order_txt).wait(2));

	// Layer 2
	this.letter_txt = new cjs.Text("A.", "bold 37px 'Frutiger Next Pro Bold'", "#00A2DE");
	this.letter_txt.name = "letter_txt";
	this.letter_txt.lineHeight = 39;
	this.letter_txt.lineWidth = 38;
	this.letter_txt.setTransform(13.8,13.3);

	this.text_txt = new cjs.Text("The M&A engagement leader and each of the deal team members should read and understand the terms of the non-disclosure and click through agreements so that he/she can assess the restrictions imposed on Deloitte, effectively consult with OGC on the exceptions taken and articulate Deloitte’s position to the client.", "15px 'Frutiger Next Pro Medium'", "#575757");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 17;
	this.text_txt.lineWidth = 777;
	this.text_txt.setTransform(63.3,10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_txt},{t:this.letter_txt}]}).wait(2));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#81BC00").ss(1,2,1).p("EhHqgFdMCPVAAAIAAK7MiPVAAAg");
	this.shape_1.setTransform(458.8,35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4F6FB").s().p("EhHqAFeIAAq7MCPVAAAIAAK7g");
	this.shape_2.setTransform(458.8,35.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2},{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,919.5,72.1);


(lib.tammy_pic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 2
	this.instance = new lib.green_tick_1("synched",0);
	this.instance.setTransform(115.9,118.2,1,1,4.7,0,0,16,13.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.bio_pic_tammy();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,152,161);


(lib.scrollbar8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AgiHQQgPgOAAgVIAAtZQAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVIAANZQABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,48);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10,96);


(lib.scrollbar7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.scrolldown_btn = new lib.scrollBtn();
	this.scrolldown_btn.setTransform(4.9,482,1,1,0,180,0,9.1,9.8);
	new cjs.ButtonHelper(this.scrolldown_btn, 0, 1, 1);

	this.scrollup_btn = new lib.scrollBtn();
	this.scrollup_btn.setTransform(4.9,-14.1,1,1,0,0,0,9.1,9.8);
	new cjs.ButtonHelper(this.scrollup_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.scrollup_btn},{t:this.scrolldown_btn}]}).wait(1));

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("EgAiAkUQgPgOAAgVMAAAhHhQAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVMAAABHhQABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,234);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.2,-23.9,18.3,515.7);


(lib.scrollbar6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AgidSQgPgOAAgVMAAAg5dQAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVMAAAA5dQABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,189);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10,378);


(lib.scrollbar5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("EgAiAjYQgPgOAAgVMAAAhFpQAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVMAAABFpQABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,228);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10,456);


(lib.scrollbar4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AgiJmQgPgOAAgVIAAyFQAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVIAASFQABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,63);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10,126);


(lib.scrollbar3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.scrolldown_btn = new lib.scrollBtn();
	this.scrolldown_btn.setTransform(4.9,473,1,1,0,180,0,9.1,9.8);
	new cjs.ButtonHelper(this.scrolldown_btn, 0, 1, 1);

	this.scrollup_btn = new lib.scrollBtn();
	this.scrollup_btn.setTransform(4.9,-14.1,1,1,0,0,0,9.1,9.8);
	new cjs.ButtonHelper(this.scrollup_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.scrollup_btn},{t:this.scrolldown_btn}]}).wait(1));

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("EgAiAjiQgPgOAAgVMAAAhF9QAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVMAAABF9QABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,229);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.2,-23.9,18.3,506.7);


(lib.scrollbar2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AgiVFQgPgOAAgVMAAAgpDQAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVMAAAApDQABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,136.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10,273);


(lib.scrollbar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.thumb_mc = new lib.thumb();
	this.thumb_mc.setTransform(5,17,1,1,0,0,0,5,17);

	this.timeline.addTween(cjs.Tween.get(this.thumb_mc).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AgiWzQgPgOAAgVMAAAgsfQAAgVAPgPQAPgOATAAQAUAAAOAOQAQAPgBAVMAAAAsfQABAVgQAOQgOAPgUAAQgTAAgPgPg");
	this.shape.setTransform(5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10,295);


(lib.progress = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("scenario progress:", "17px 'Frutiger Next Pro Medium'", "#565656");
	this.text.textAlign = "right";
	this.text.lineHeight = 19;
	this.text.lineWidth = 143;
	this.text.setTransform(398.5,2);

	this.text_1 = new cjs.Text("course completion:", "17px 'Frutiger Next Pro Medium'", "#565656");
	this.text_1.textAlign = "right";
	this.text_1.lineHeight = 19;
	this.text_1.lineWidth = 143;
	this.text_1.setTransform(144.9,2);

	this.scenario_mc = new lib.progress_bar();
	this.scenario_mc.setTransform(451.5,12.3,1,1,0,0,0,49.4,6.2);

	this.course_mc = new lib.progress_bar();
	this.course_mc.setTransform(200,12.3,1,1,0,0,0,49.4,6.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.course_mc},{t:this.scenario_mc},{t:this.text_1},{t:this.text}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,501,25.8);


(lib.personPanelMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(27.6,65,0.932,0.925,0,0,0,-1,0);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1));

	// Layer 2
	this.scrollbar_mc = new lib.scrollbar2();
	this.scrollbar_mc.setTransform(467,213.1,1,1,0,0,0,5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(1));

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EghlAVKMAAAgqTMBDKAAAMAAAAqTg");
	mask.setTransform(242.3,200.5);

	// Layer 4
	this.text_txt = new cjs.Text("Text here", "22px 'Frutiger Next Pro Medium'", "#565656");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 24;
	this.text_txt.lineWidth = 424;
	this.text_txt.setTransform(30.9,67.5);

	this.text_txt.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

	// Layer 5
	this.name_txt = new cjs.Text("Name of person:", "43px 'Frutiger Next Pro Medium'", "#002775");
	this.name_txt.name = "name_txt";
	this.name_txt.lineHeight = 45;
	this.name_txt.lineWidth = 420;
	this.name_txt.setTransform(30.1,7.9,1.038,1);

	this.instance = new lib.panel_bg_alpha70("synched",0);
	this.instance.setTransform(240.7,194.9,1,1.042,0,0,0,240.7,188.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.name_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.8,481.4,372.3);


(lib.person_panel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		this.close_btn.on("click", function(event){
			event.currentTarget.parent.play();
		});
	}
	this.frame_10 = function() {
		this.stop();
	}
	this.frame_19 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10).call(this.frame_10).wait(9).call(this.frame_19).wait(1));

	// Layer 7
	this.close_btn = new lib.closePanelBtn();
	this.close_btn.setTransform(9.5,338.7,0.27,0.27);
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(1).to({visible:false},0).wait(9).to({skewX:180,y:31.7,visible:true},0).wait(1).to({y:363.7,visible:false},0).wait(9));

	// Layer 6
	this.panel_mc = new lib.personPanelMC();
	this.panel_mc.setTransform(241,184,1,1,0,0,0,240.7,184.3);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1).to({y:-149},9).wait(1).to({y:184},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.3,-2.2,481.4,372.3);


(lib.note_button_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.green_tick_1("synched",0);
	this.instance.setTransform(64.3,114.3,1,1,0,0,0,16,13.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1));

	// Layer 2
	this.text = new cjs.Text("Whirling\nEnergies \nCorporation", "16px 'Frutiger Next Pro Medium'", "#002874");
	this.text.lineHeight = 16;
	this.text.lineWidth = 100;
	this.text.setTransform(15.5,36.1);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(2).to({text:"Deloitte's\nRelationship\nwith WEC"},0).wait(2).to({text:"Recent\nEngagement\nwith WEC"},0).wait(2));

	// Layer 1
	this.instance_1 = new lib.note_button();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,160,159);


(lib.news = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_2 = function() {
		initScrollbar(this, 380, 720, 114);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2).call(this.frame_2).wait(1));

	// Layer 5
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(102.9,114,1.398,1.566,0,0,0,-0.1,28.4);
	this.panel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(2).to({_off:false},0).wait(1));

	// Layer 4
	this.scrollbar_mc = new lib.scrollbar6();
	this.scrollbar_mc.setTransform(756,262.1,1,1,0,0,0,5,147.5);
	this.scrollbar_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(2).to({_off:false},0).wait(1));

	// Layer 7
	this.main_txt = new cjs.Text("Whirling Energies Corporation (WEC) is one of the largest energy companies in the world. \nOur headquarters are in Houston, Texas, but while North America is our home and \nprovides the majority of our production, we are active in almost 20 countries and in a wide \nrange of geologic and geographic settings, including some of the world’s most challenging \nareas. We have a proven track record of responsibly and efficiently exploring for and \nproducing oil and natural gas. Our production streams include light oil, heavy oil, oil sands, \nnatural gas liquids, conventional natural gas, coal bed methane, shale gas and oil, and \nliquefied natural gas (LNG).\n\nLast year, WEC reported revenues of $57 billion with an operating income of $18 billion. \nWe currently have assets of over $105 billion and an equity base of $42 billion. Our highly \nskilled and dedicated workforce now totals over 17,000 employees.\n\nWEC is a company that has come together over the last decade as a result of numerous \nmergers in the global oil and gas industry. Our merger activity continues aggressively \ntoday, both from a geographic as well as a product perspective. New acquisitions require \nintegration and a related strategic refocus, which leads to selective diversifications. \nOverall, however, our ultimate aim is to be the leader in helping the world meet current \nand future energy needs.", "16px 'Frutiger Next Pro Medium'", "#575757");
	this.main_txt.name = "main_txt";
	this.main_txt.lineHeight = 16;
	this.main_txt.lineWidth = 645;
	this.main_txt.setTransform(103.7,178.8);

	this.subhead_txt = new cjs.Text("Helping to meet our world’s energy needs", "17px 'Frutiger Next Pro Medium'", "#002874");
	this.subhead_txt.name = "subhead_txt";
	this.subhead_txt.textAlign = "center";
	this.subhead_txt.lineHeight = 17;
	this.subhead_txt.lineWidth = 635;
	this.subhead_txt.setTransform(426.9,139.9);

	this.headline_txt = new cjs.Text("Whirling Energies Corporation", "bold 26px 'Frutiger Next Pro Bold'", "#002874");
	this.headline_txt.name = "headline_txt";
	this.headline_txt.textAlign = "center";
	this.headline_txt.lineHeight = 26;
	this.headline_txt.lineWidth = 635;
	this.headline_txt.setTransform(426.9,106.3);

	this.title_txt = new cjs.Text("Extracts from latest Whirling Energies Corporation (WEC) Annual Report:", "bold 18px 'Frutiger Next Pro Bold'", "#002874");
	this.title_txt.name = "title_txt";
	this.title_txt.textAlign = "center";
	this.title_txt.lineHeight = 18;
	this.title_txt.lineWidth = 635;
	this.title_txt.setTransform(426.9,65.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.title_txt},{t:this.headline_txt},{t:this.subhead_txt,p:{y:139.9,text:"Helping to meet our world’s energy needs",font:"17px 'Frutiger Next Pro Medium'",lineHeight:17}},{t:this.main_txt,p:{y:178.8,text:"Whirling Energies Corporation (WEC) is one of the largest energy companies in the world. \nOur headquarters are in Houston, Texas, but while North America is our home and \nprovides the majority of our production, we are active in almost 20 countries and in a wide \nrange of geologic and geographic settings, including some of the world’s most challenging \nareas. We have a proven track record of responsibly and efficiently exploring for and \nproducing oil and natural gas. Our production streams include light oil, heavy oil, oil sands, \nnatural gas liquids, conventional natural gas, coal bed methane, shale gas and oil, and \nliquefied natural gas (LNG).\n\nLast year, WEC reported revenues of $57 billion with an operating income of $18 billion. \nWe currently have assets of over $105 billion and an equity base of $42 billion. Our highly \nskilled and dedicated workforce now totals over 17,000 employees.\n\nWEC is a company that has come together over the last decade as a result of numerous \nmergers in the global oil and gas industry. Our merger activity continues aggressively \ntoday, both from a geographic as well as a product perspective. New acquisitions require \nintegration and a related strategic refocus, which leads to selective diversifications. \nOverall, however, our ultimate aim is to be the leader in helping the world meet current \nand future energy needs.",lineWidth:645,x:103.7,font:"16px 'Frutiger Next Pro Medium'",color:"#575757",textAlign:"",lineHeight:16}}]}).to({state:[{t:this.subhead_txt,p:{y:64.9,text:"Deloitte’s Relationship with WEC",font:"bold 26px 'Frutiger Next Pro Bold'",lineHeight:26}},{t:this.main_txt,p:{y:115.8,text:"WEC and Deloitte began their relationship in 1994 when Deloitte was asked to provide tax \nstructuring services to Dallas Oil & Gas Corporation on its significant merger with European \nEnergies, Inc. to form Whirling Energies Corporation. The tax relationship expanded as a \nresult and in recent years Deloitte has been involved on a number of consulting related \nprojects, including some (but not all) of its subsequent acquisitions and divestments. WEC \nis a large global relationship client for Deloitte. Other Deloitte member firms have worked \nwith WEC companies (subsidiaries and affiliates) around the world in recent years on \nspecific and somewhat narrowly focused projects.  The total WEC global relationship is \napproximately $30 million in annual fees with Deloitte, down from approximately $45 \nmillion in the prior year.\n\nThree years ago, the former CEO of WEC, Robert Duffy, retired and was succeeded by \nLarry Mann, who began a further round of aggressive global acquisitions, but was more \ndisposed to spreading the advisory services around a small number of consultants. As a \nresult, Deloitte now competes for WEC’s advisory business with other global consultancy \nfirms.\n\nThe US LCSP for WEC is Kevin Short, a lead energy partner for Deloitte. Kevin is based in \nHouston where WEC is headquartered. James Simmons is a Deloitte Advisory partner in \nthe New York M&A Transaction Services practice and has worked with WEC on some of \ntheir previous acquisitions. James’ main contact at WEC is Matt Green, Head of Business \nDevelopment, and a direct report of the COO, Lew Smith.",lineWidth:646,x:103.7,font:"16px 'Frutiger Next Pro Medium'",color:"#575757",textAlign:"",lineHeight:16}}]},1).to({state:[{t:this.main_txt,p:{y:64.9,text:"Recent Engagement with WEC",lineWidth:635,x:426.9,font:"bold 26px 'Frutiger Next Pro Bold'",color:"#002874",textAlign:"center",lineHeight:26}}]},1).wait(1));

	// Layer 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgywAfIMAAAg+PMBlhAAAMAAAA+Pg");
	mask.setTransform(426.1,313);

	// Layer 2
	this.text_txt = new cjs.Text("In the first quarter of this year, Matt Green met James Simmons in New York and \nmentioned that WEC was strongly considering divesting British Specialty Chemicals Ltd \n(BSC), a wholly owned subsidiary based in Glasgow. The company was acquired as part of \na larger acquisition four years ago of British Liquid Gases plc (now WEC UK). The specialty \nchemical products and services of BSC do not fit the strategic focus of WEC.\n\nIn engaging Deloitte for the sell-side advisory mandate, Matt Green mentioned that the \nmanagement at BSC was aware that WEC was looking to sell the company. However, he \nstated that no other parties outside key senior managers at WEC US, WEC UK and BSC \nwere aware of this proposed transaction and that he expected Deloitte to commit to \nWEC’s high standards of confidentiality.\n\nJames Simmons assembled a sell-side advisory team in New York and London to work on \nthe transaction. He had junior people under him in New York and Alison Hendry was on-\nsite in London.  Alison is a UK Consulting Partner who had just completed a 15 month \nproject for WEC UK related to its implementation of organizational structure plans for the \nIT function.  Alison had been working closely with WEC UK’s HR Director, Claire Fields. \nGiven that BSC management was aware that WEC was looking to sell the subsidiary and \ngiven Alison’s relationship with WEC UK, James felt that she would be a good fit.\n\nTwo months after the appointment, Matt Green contacted James to advise him that WEC \nand BSC had mutually agreed to hold off on the divestment. James immediately contacted \nhis team to advise them of the situation, and they all stopped work on the M&A services \nboth in the US and the UK. \n\nLast week, James Simmons received a voicemail from an irate Matt Green in Houston that \nsaid, “This is not the way we do business. You breached our trust and you have lost all \ncredibility with management at WEC. Please put your house in order, James.” Puzzled, \nJames tried to contact Matt. He was told that he was not available to take his call. \n\nAs James grew more concerned, he received a call from the US LCSP in Houston, Kevin \nShort.  Larry Mann, CEO of WEC had made a formal complaint to him directly about the \nconduct of Deloitte’ client relationship team over the last few months and was putting an \nembargo on any global business with Deloitte for 6 months pending the outcome of \nDeloitte’s investigation into this matter. Kevin reached out to Larry and WEC executives \nlast week but none of them have responded to his messages.", "16px 'Frutiger Next Pro Medium'", "#575757");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 16;
	this.text_txt.lineWidth = 645;
	this.text_txt.setTransform(103.7,115.8);

	this.text = new cjs.Text("Recent Engagement with WEC", "bold 26px 'Frutiger Next Pro Bold'", "#002874");
	this.text.textAlign = "center";
	this.text.lineHeight = 26;
	this.text.lineWidth = 635;
	this.text.setTransform(426.9,64.9);

	this.text_txt.mask = this.text.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.text},{t:this.text_txt}]},2).wait(1));

	// Layer 1
	this.instance = new lib.news_bg();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,844,585);


(lib.neuralConnectionAnim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.neuralConnection();
	this.instance.setTransform(0,-0.5,1,1,0,0,0,9,8.5);
	this.instance.alpha = 0;
	this.instance.shadow = new cjs.Shadow("rgba(255,255,255,1)",0,0,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1},4).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.5,-13.5,33,32);


(lib.loadingAnimMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.loadingMC();
	this.instance.setTransform(69.2,69.2,1,1,0,0,0,69.2,69.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:360},24).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,138.5,138.5);


(lib.kevin_pic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 2
	this.instance = new lib.green_tick_1("synched",0);
	this.instance.setTransform(124.6,113.4,1,1,-10,0,0,16,13.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.bio_pic_kevin();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,177,171);


(lib.james_pic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 2
	this.instance = new lib.green_tick_1("synched",0);
	this.instance.setTransform(103.4,115.3,1,1,3.7,0,0,16.1,13.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.bio_pic_james();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,153,161);


(lib.intro_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(229.6,147,1,1,0,0,0,230,147);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1));

	// Layer 3
	this.scrollbar_mc = new lib.scrollbar();
	this.scrollbar_mc.setTransform(466.6,147.5,1,1,0,0,0,5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(1));

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Egj4AW/MAAAgt9MBHxAAAMAAAAt9g");
	mask.setTransform(229.7,147.1);

	// Layer 1
	this.text_txt = new cjs.Text("Text here", "18px 'Frutiger Next Pro Medium'", "#565656");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 20;
	this.text_txt.lineWidth = 455;
	this.text_txt.setTransform(2,2);

	this.text_txt.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,0,472,295);


(lib.glossaryItemsMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.glossary1_mc = new lib.glossary_item();
	this.glossary1_mc.setTransform(512,77,1,1,0,0,0,512,77);

	this.timeline.addTween(cjs.Tween.get(this.glossary1_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.6,0,1025.3,154);


(lib.glossary_full_screen = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.scrollbar_mc = new lib.scrollbar5();
	this.scrollbar_mc.setTransform(1000,263,1,1,0,0,0,5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(1));

	// Layer 5
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(27.6,65,2.086,1.927,0,0,0,-1,0);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1));

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EhKqAsrMAAAhZVMCVVAAAMAAABZVg");
	mask.setTransform(511,352.8);

	// Layer 3
	this.text_txt = new lib.glossaryItemsMC();
	this.text_txt.setTransform(512,193.3,1,1,0,0,0,512,77);

	this.text_txt.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

	// Layer 1
	this.instance = new lib.blotter_bg2();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1024,653);


(lib.feedbackScroller = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(-1.3,-18.4,1.898,0.427,0,0,0,-1.2,0);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1));

	// Layer 3
	this.scrollbar_mc = new lib.scrollbar8();
	this.scrollbar_mc.setTransform(885.6,147.5,1,1,0,0,0,5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(1));

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EhERAHPIAAudMCIjAAAIAAOdg");
	mask.setTransform(437,46.4);

	// Layer 5
	this.text_txt = new cjs.Text("That is correct.  When competitively sensitive information is involved in a transaction, there are often incremental steps required \nwith respect to handling such information including the addition of a separate clean team and additional reviews of Deloitte’s work \nproduct by anti-trust counsel prior to sharing the information with the client.  These additional steps and resources required should \nbe built into the M&A fees and it is important for the engagement leader to anticipate this and discuss with his/her client in a \ntimely fashion.", "15px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 15;
	this.text_txt.lineWidth = 870;
	this.text_txt.setTransform(2,2);

	this.text_txt.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-6.6,890.6,107.6);


(lib.feedback_panel_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.scrollpanel_mc = new lib.feedbackScroller();
	this.scrollpanel_mc.setTransform(450.5,93.1,0.982,1,0,0,0,437,46.4);

	this.timeline.addTween(cjs.Tween.get(this.scrollpanel_mc).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4F6FB").s().p("ADMA1QgKgFgFgJQgGgKAAgNQAAgNAGgHQAGgKAKgFQAKgEANgBIALACIAKACIgBAPIgIgDIgIgBQgIABgFADQgHADgCAFQgDAFAAAIQAAAHADAHQADAGAFADQAGADAIABIAIgBIAIgCIABAPQgFACgGABIgLAAQgNAAgKgFgABsA3QgGgDgDgFQgFgFAAgIQAAgLAHgGQAFgGALgCQAJgCAMgBIAEAAIAFABIAAgDQAAgGgFgEQgEgDgKAAIgLABIgNAFIgBgPIAOgEIAOgCQALABAIADQAJACAFAIQAEAGAAAKIAAAfIAAALIABAJIgVAAIAAgNQgEAIgGACQgHAEgKAAQgHAAgGgDgACCATQgGACgEADQgEACgBAHQAAAFAEAEQADACAGAAQAGABAFgEQAEgDACgFQACgGAAgFIAAgEIgFAAIgDAAIgJABgAAkA2QgGgDgFgIIAAAOIgUAAIAAhyIAWAAIAAAwQADgGAHgEQAHgEAHgBQAMABAIAFQAHAHAEAJQADAHAAALQAAALgDAJQgEAKgIAGQgGAGgNAAQgIAAgHgEgAAhgDQgEADgBAFIgBALIABAJQAAAFACAEQACADAEADQADACAFABQAHAAAEgEQADgFACgGIABgMIgBgMQgCgEgDgDQgEgFgHAAQgHAAgEAFgAhOA0QgIgGgDgKQgDgJgBgLQABgLADgHQADgJAIgHQAHgFAMgBQAIABAGAEQAHAEADAGIAAgwIAWAAIAABbIAAAMIABALIgUAAIgBgOQgEAIgHADQgGAEgJAAQgMAAgHgGgAg/gDQgEADAAAFIgCALIABAJQAAAFACAEQACAEAEADQADACAGAAQAGAAAEgEQAEgFABgGQACgGAAgGQAAgGgCgFQgBgFgEgDQgEgFgGAAQgIAAgEAFgAimAvQgMgLAAgUQAAgMAFgHQAFgKAJgFQAJgGALAAQAOAAAHAGQAIAFAEAKQADAGAAANIAAAFIg1AAQAAALAHAFQAGAHANgBQAGABAGgCIAKgDIABAOIgMAEIgNABQgXAAgLgLgAh6AIQgBgEgBgEQgCgEgDgDQgDgDgGAAQgGAAgEADQgEADgCAEQgCAEAAAEIAiAAIAAAAgAj8AvQgMgLAAgUQAAgMAGgHQAFgKAIgFQAKgGALAAQANAAAIAGQAHAFAEAKQADAGAAANIAAAFIg1AAQAAALAHAFQAGAHANgBQAGABAHgCIAKgDIAAAOIgLAEIgOABQgWAAgMgLgAjQAIQAAgEgCgEQgBgEgDgDQgDgDgGAAQgGAAgFADQgEADgBAEQgCAEgBAEIAiAAIAAAAgAE9A5IgggqIAAAqIgVAAIAAhyIAVAAIAABCIAegfIAZAAIggAiIAkAtgAlXA5IAAhpIBFAAIAAAQIgtAAIAAAeIArAAIAAAOIgrAAIAAAtg");
	this.shape.setTransform(63.5,27.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#002874").s().p("AqjBlIAAjJIVHAAIAADJg");
	this.shape_1.setTransform(88.2,27.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 1
	this.instance = new lib.feedback_panel();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1024,173);


(lib.coursecompleteMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.end_btn.on("click", endCourse);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 3
	this.end_btn = new lib.BlankBtn();
	this.end_btn.setTransform(-113,-263.4,6.889,3.273);
	new cjs.ButtonHelper(this.end_btn, 0, 1, 2, false, new lib.BlankBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.end_btn).wait(1));

	// Layer 2
	this.text = new cjs.Text("Thank you completing this course.  You will receive an email from US National Learning Evaluations requesting that you participate in a brief Deloitte course evaluation.  Your feedback will help us continuously improve and measure the effectiveness of our learning programs and we appreciate your participation.\n\nClick here to print your certificate and send your passing score to the learning management system.  If you do not follow these steps, your completion may not be recorded.", "24px 'Frutiger Next Pro Medium'", "#2092BC");
	this.text.textAlign = "center";
	this.text.lineHeight = 26;
	this.text.lineWidth = 616;
	this.text.setTransform(197,-430.2);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 1
	this.score_txt = new cjs.Text("0%", "42px 'Frutiger Next Pro Medium'", "#00A2DD");
	this.score_txt.name = "score_txt";
	this.score_txt.lineHeight = 43;
	this.score_txt.lineWidth = 185;
	this.score_txt.setTransform(209.5,-106);

	this.text_1 = new cjs.Text("Score:", "42px 'Frutiger Next Pro Medium'", "#00A2DD");
	this.text_1.textAlign = "right";
	this.text_1.lineHeight = 43;
	this.text_1.lineWidth = 185;
	this.text_1.setTransform(198,-107);

	this.text_2 = new cjs.Text("Course Complete", "42px 'Frutiger Next Pro Medium'", "#00A2DD");
	this.text_2.textAlign = "center";
	this.text_2.lineHeight = 43;
	this.text_2.lineWidth = 388;
	this.text_2.setTransform(196,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.score_txt}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113,-432.2,620,478.3);


(lib.ConfirmRedoMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.no_btn.mouseChildren = this.yes_btn.mouseChildren = false;
		
		this.no_btn.on('click', function(event){
			event.currentTarget.parent.parent.gotoAndStop(0);
			event.stopPropagation();
		});
		
		this.yes_btn.on('click', function(event){
			event.stopPropagation();
			switch (event.currentTarget.parent.parent.parent.currentFrame){
				case 0:
					jamesPressed(true);
					break;
				case 1:
					alisonPressed(true);
					break;
				case 2:
					tammyPressed(true);
					break;
				case 3:
					kevinPressed(true);
					break;
			}
			event.currentTarget.parent.parent.gotoAndStop(0);
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 2
	this.text = new cjs.Text("Would you like to redo the interview?", "24px 'Frutiger Next Pro'", "#999999");
	this.text.textAlign = "center";
	this.text.lineHeight = 24;
	this.text.lineWidth = 242;
	this.text.setTransform(135.4,17.7);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 3
	this.no_btn = new lib.NoBtn();
	this.no_btn.setTransform(77.3,86.2,1,1,0,0,0,54.5,15.6);
	new cjs.ButtonHelper(this.no_btn, 0, 1, 2, false, new lib.NoBtn(), 3);

	this.yes_btn = new lib.OKBtn();
	this.yes_btn.setTransform(194.7,86.2,1,1,0,0,0,54.5,15.6);
	new cjs.ButtonHelper(this.yes_btn, 0, 1, 2, false, new lib.OKBtn(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.yes_btn},{t:this.no_btn}]}).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#999999").ss(4,1,1).p("AzgonMAnBAAAQBkAAAABlIAALzQAABkhkAAIx5AAIhXCTIhTiTIyeAAQhkAAAAhkIAArzQAAhlBkAAg");
	this.shape.setTransform(135,68.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhCGUIyeAAQhkABAAhlIAArzQAAhjBkgBMAnBAAAQBkABAABjIAALzQAABlhkgBIx5AAIhXCUg");
	this.shape_1.setTransform(135,68.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,11.6,273.9,114.4);


(lib.ConfirmHomeMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.no_btn.on('click', function(event){
			event.currentTarget.parent.parent.gotoAndStop(0);
		});
		
		this.yes_btn.on('click', function(event){
			if (speaker_snd!=null){
				speaker_snd.stop();
				delete speaker_snd;
				setAnimation( "head", "static" );
			}
			exportRoot.gotoAndPlay("start");
			event.currentTarget.parent.parent.gotoAndStop(0);
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 2
	this.text = new cjs.Text("Are you sure you want to leave this interview?", "24px 'Frutiger Next Pro'", "#999999");
	this.text.textAlign = "center";
	this.text.lineHeight = 24;
	this.text.lineWidth = 242;
	this.text.setTransform(135.4,17.7);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 3
	this.no_btn = new lib.NoBtn();
	this.no_btn.setTransform(77.3,86.2,1,1,0,0,0,54.5,15.6);
	new cjs.ButtonHelper(this.no_btn, 0, 1, 2, false, new lib.NoBtn(), 3);

	this.yes_btn = new lib.OKBtn();
	this.yes_btn.setTransform(194.7,86.2,1,1,0,0,0,54.5,15.6);
	new cjs.ButtonHelper(this.yes_btn, 0, 1, 2, false, new lib.OKBtn(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.yes_btn},{t:this.no_btn}]}).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#999999").ss(4,1,1).p("AzgmZMAjaAAAIBRiIIBQCIIBGAAQBkAAAABkIAALzQAABkhkAAMgnBAAAQhkAAAAhkIAArzQAAhkBkAAg");
	this.shape.setTransform(135,54.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AzgIiQhkAAAAhkIAArzQAAhkBkAAMAjaAAAIBRiIIBPCIIBHAAQBkAAAABkIAALzQAABkhkAAg");
	this.shape_1.setTransform(135,54.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,273.9,113.3);


(lib.ConfirmAnimMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_9 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(9).call(this.frame_9).wait(1));

	// Layer 1
	this.confirm_mc = new lib.ConfirmRedoMC();
	this.confirm_mc.setTransform(135.8,108.3,0.14,0.14,0,0,0,135.3,121.1);
	this.confirm_mc.alpha = 0;
	this.confirm_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.confirm_mc).wait(1).to({_off:false},0).to({regX:137.8,regY:120,scaleX:1,scaleY:1,x:137.8,y:106.4,alpha:1},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.coacing_panel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Coaching:", "44px 'Frutiger Next Pro Medium'", "#002775");
	this.text.lineHeight = 46;
	this.text.lineWidth = 420;
	this.text.setTransform(33.2,11.4);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 6
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(27.6,37,0.932,1.071,0,0,0,-1,0);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1));

	// Layer 3
	this.scrollbar_mc = new lib.scrollbar2();
	this.scrollbar_mc.setTransform(467,213.1,1,1,0,0,0,5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(1));

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EghlAVKMAAAgqTMBDKAAAMAAAAqTg");
	mask.setTransform(242.3,200.5);

	// Layer 5
	this.text_txt = new cjs.Text("Text here", "22px 'Frutiger Next Pro Medium'", "#565656");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 24;
	this.text_txt.lineWidth = 424;
	this.text_txt.setTransform(30.9,67.5);

	this.text_txt.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(183,219,239,0.698)").s().p("Egl/AbTMAAAg2lMBL/AAAMAAAA2lg");
	this.shape.setTransform(243.3,173.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1,486.6,349.6);


(lib.choice_panel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 8
	this.go_btn = new lib.goBtn();
	this.go_btn.setTransform(278.7,32.4,1,1,0,0,0,17.5,20);
	new cjs.ButtonHelper(this.go_btn, 0, 1, 2, false, new lib.goBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.go_btn).to({_off:true},1).wait(1));

	// Layer 3
	this.title_txt = new cjs.Text("Choice 1", "bold 26px 'Frutiger Next Pro Bold'", "#082C77");
	this.title_txt.name = "title_txt";
	this.title_txt.lineHeight = 28;
	this.title_txt.lineWidth = 212;
	this.title_txt.setTransform(27.7,18);

	this.timeline.addTween(cjs.Tween.get(this.title_txt).wait(2));

	// Layer 6
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(23.5,59,0.538,0.43,0,0,0,-0.1,1.1);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(2));

	// Layer 5
	this.scrollbar_mc = new lib.scrollbar4();
	this.scrollbar_mc.setTransform(289,206.9,1,1,0,0,0,5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(2));

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AzXJzIAAzmMAmvAAAIAATmg");
	mask.setTransform(147.2,122.2);

	// Layer 2
	this.text_txt = new cjs.Text("He reminded me about", "16px 'Frutiger Next Pro Medium'", "#565656");
	this.text_txt.name = "text_txt";
	this.text_txt.lineHeight = 18;
	this.text_txt.lineWidth = 243;
	this.text_txt.setTransform(26.5,60.7);

	this.text_txt.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.text_txt).wait(2));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A4CPpIAA/RMAwFAAAIAAfRg");
	this.shape.setTransform(153.9,100.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4F6FB").s().p("A4CPpIAA/RMAwFAAAIAAfRg");
	this.shape_1.setTransform(153.9,100.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,307.9,200.3);


(lib.characters = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.confirm_mc = new lib.ConfirmAnimMC();
	this.confirm_mc.setTransform(-18.4,195.7);

	this.timeline.addTween(cjs.Tween.get(this.confirm_mc).wait(4));

	// Layer 3
	this.tryagain_mc = new lib.try_again();
	this.tryagain_mc.setTransform(113,350.7,1,1,0,0,0,107.2,43.8);
	this.tryagain_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.tryagain_mc).wait(4));

	// Layer 2
	this.text = new cjs.Text("MATS Advisory Partner ", "17px 'Frutiger Next Pro Medium'", "#565656");
	this.text.textAlign = "center";
	this.text.lineHeight = 19;
	this.text.lineWidth = 188;
	this.text.setTransform(115.2,29.8);

	this.text_1 = new cjs.Text("James Simmons", "bold 24px 'Frutiger Next Pro Bold'", "#565656");
	this.text_1.textAlign = "center";
	this.text_1.lineHeight = 26;
	this.text_1.lineWidth = 188;
	this.text_1.setTransform(115.2,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_1,p:{text:"James Simmons",x:115.2,lineWidth:188}},{t:this.text,p:{text:"MATS Advisory Partner "}}]}).to({state:[{t:this.text_1,p:{text:"Alison Hendry",x:115.2,lineWidth:188}},{t:this.text,p:{text:"Human Capital\nConsulting Partner "}}]},1).to({state:[{t:this.text_1,p:{text:"Tammy Cheoung",x:115.7,lineWidth:233}},{t:this.text,p:{text:"FAS Corporate\nFinance Partner "}}]},1).to({state:[{t:this.text_1,p:{text:"Kevin Short",x:115.2,lineWidth:188}},{t:this.text,p:{text:"Business Tax \nServices Partner"}}]},1).wait(1));

	// Layer 1
	this.instance = new lib.character1();
	this.instance.setTransform(0,78.7);

	this.instance_1 = new lib.character2();
	this.instance_1.setTransform(10.4,74.1);

	this.instance_2 = new lib.character3();
	this.instance_2.setTransform(-8,71.8);

	this.instance_3 = new lib.character4();
	this.instance_3.setTransform(-14.9,61.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,232,447.7);


(lib.btn_pause_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 1
	this.instance = new lib.btn_pause();
	this.instance.setTransform(18.6,18.6,1,1,0,0,0,18.6,18.6);
	new cjs.ButtonHelper(this.instance, 0, 1, 1);

	this.instance_1 = new lib.btn_play();
	this.instance_1.setTransform(18.6,18.6,1,1,0,0,0,18.6,18.6);
	new cjs.ButtonHelper(this.instance_1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,37.2,37.2);


(lib.bottom_bar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.background_btn.on("click", function(event){
			backgroundPressed();
		});
		this.policy_btn.on("click", function(event){
			policyPressed();
		});
		this.biographies_btn.on("click", function(event){
			biographiesPressed();
		});
		this.glossary_btn.on("click", function(event){
			glossaryPressed();
		});
		this.blotter_btn.on("click", function(event){
			blotterPressed();
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 3
	this.blotter_btn = new lib.BlankBtn();
	this.blotter_btn.setTransform(953.2,18.3,1.485,1,0,0,0,45.1,15.5);
	new cjs.ButtonHelper(this.blotter_btn, 0, 1, 2, false, new lib.BlankBtn(), 3);

	this.glossary_btn = new lib.BlankBtn();
	this.glossary_btn.setTransform(676,19.4,1.609,1,0,0,0,45.1,15.5);
	new cjs.ButtonHelper(this.glossary_btn, 0, 1, 2, false, new lib.BlankBtn(), 3);

	this.biographies_btn = new lib.BlankBtn();
	this.biographies_btn.setTransform(499.5,19.4,2.055,1,0,0,0,45.1,15.5);
	new cjs.ButtonHelper(this.biographies_btn, 0, 1, 2, false, new lib.BlankBtn(), 3);

	this.policy_btn = new lib.BlankBtn();
	this.policy_btn.setTransform(309,19.4,2.055,1,0,0,0,45.1,15.5);
	new cjs.ButtonHelper(this.policy_btn, 0, 1, 2, false, new lib.BlankBtn(), 3);

	this.background_btn = new lib.BlankBtn();
	this.background_btn.setTransform(104.2,20.2,2.055,1,0,0,0,45.1,15.5);
	new cjs.ButtonHelper(this.background_btn, 0, 1, 2, false, new lib.BlankBtn(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.background_btn},{t:this.policy_btn},{t:this.biographies_btn},{t:this.glossary_btn},{t:this.blotter_btn}]}).wait(1));

	// Layer 2
	this.text = new cjs.Text("Blotter", "bold 26px 'Frutiger Next Pro Bold'", "#002874");
	this.text.lineHeight = 28;
	this.text.lineWidth = 100;
	this.text.setTransform(920.8,7.2);

	this.instance = new lib.icon_blotter();
	this.instance.setTransform(887.8,-1.1);

	this.instance_1 = new lib.icon_glossary();
	this.instance_1.setTransform(599.3,-2.3);

	this.instance_2 = new lib.icon_biographies();
	this.instance_2.setTransform(406.8,-3.6);

	this.instance_3 = new lib.icon_policy();
	this.instance_3.setTransform(206.4,-4.2);

	this.instance_4 = new lib.icon_background();
	this.instance_4.setTransform(0.3,-2.3);

	this.text_1 = new cjs.Text("Glossary", "19px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_1.lineHeight = 21;
	this.text_1.lineWidth = 108;
	this.text_1.setTransform(635.5,5.4);

	this.text_2 = new cjs.Text("Biographies", "19px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_2.lineHeight = 21;
	this.text_2.lineWidth = 139;
	this.text_2.setTransform(443.1,5.4);

	this.text_3 = new cjs.Text("Policy Guidance", "19px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_3.lineHeight = 21;
	this.text_3.lineWidth = 154;
	this.text_3.setTransform(245.4,5.4);

	this.text_4 = new cjs.Text("Background", "19px 'Frutiger Next Pro Medium'", "#FFFFFF");
	this.text_4.lineHeight = 21;
	this.text_4.lineWidth = 136;
	this.text_4.setTransform(49.6,5.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_4},{t:this.text_3},{t:this.text_2},{t:this.text_1},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance},{t:this.text}]}).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#81BC00").s().p("EhP/AEEIAAoHMCf/AAAIAAIHg");
	this.shape.setTransform(512,26.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-4.2,1024,56.4);


(lib.blotterPopup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.close_btn = new lib.closePopupBtn();
	this.close_btn.setTransform(431.5,-13.4,1,1,0,0,0,13.5,13.5);
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(1));

	// Layer 2
	this.text = new cjs.Text("As you learn important information during your interviews, it will be noted on the Blotter. The Blotter will also include additional coaching as to why the information learned is significant. The number of blotter points revealed will determine the quality of your interview and whether a re-interview is required, so choose your questions carefully. \n\nIf your interview goes well, but all blotter points are not revealed, you will have a chance to review the additional blotter points and coaching notes at the end of the interview. To review the Blotter points revealed during the course of the interview, click on the Blotter link in the lower right of your screen.", "16px 'Frutiger Next Pro'", "#333333");
	this.text.lineHeight = 18;
	this.text.lineWidth = 428;
	this.text.setTransform(2,7);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Egi0AWaQhkAAAAhuMAAAgpXQAAhuBkAAMBFpAAAQBkAAAABuMAAAApXQAABuhkAAg");
	this.shape.setTransform(216,113.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(51,51,51,0.498)").s().p("EhQNAy7MAAAhl1MCgbAAAMAAABl1g");
	this.shape_1.setTransform(212.5,109.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-301,-216.5,1027,652);


(lib.blotterMessage = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.close_btn = new lib.closePopupBtn();
	this.close_btn.setTransform(430.5,-6.4,1,1,0,0,0,13.5,13.5);
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(1));

	// Layer 3
	this.text = new cjs.Text("You can now review the blotter points that were not revealed during the interview", "33px 'Frutiger Next Pro Medium'", "#55B5E5");
	this.text.lineHeight = 35;
	this.text.lineWidth = 366;
	this.text.setTransform(34,28);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Egi0AUYQhkAAAAhkMAAAglnQAAhkBkAAMBFpAAAQBkAAAABkMAAAAlnQAABkhkAAg");
	this.shape.setTransform(216,107.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.9,-22.9,466,261);


(lib.blotter_side = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.text = new cjs.Text("Blotter", "bold 37px 'Frutiger Next Pro Bold'", "#082C77");
	this.text.lineHeight = 39;
	this.text.lineWidth = 120;
	this.text.setTransform(22.9,62.8);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 6
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(19.5,111,0.757,1.553,0,0,0,-0.1,1);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1));

	// Layer 5
	this.scrollbar_mc = new lib.scrollbar3();
	this.scrollbar_mc.setTransform(373.7,254.6,1,1,0,0,0,5,147.5);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(1));

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgbVAjsMAAAhHXMA2qAAAMAAABHXg");
	mask.setTransform(192.5,337.5);

	// Layer 2
	this.items_mc = new lib.miniBlotterPanelMC();
	this.items_mc.setTransform(193.7,335.3,1,1,0,0,0,175.1,228.2);

	this.items_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.items_mc).wait(1));

	// Layer 1
	this.instance = new lib.blotter_bg1();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,388,634);


(lib.blotter_panel_in = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.blotter_panel("synched",0);
	this.instance.setTransform(0,0,0.073,0.073);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({scaleX:1,scaleY:1},9).to({scaleX:0.07,scaleY:0.07},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.blotter_coaching_item = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_9 = function() {
		this.stop();
		this.close_btn.cursor = "pointer";
		this.close_btn.on("click", function(event){
			var parent = event.currentTarget.parent;
			parent.gotoAndStop(0);
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(9).call(this.frame_9).wait(1));

	// Layer 4
	this.close_btn = new lib.BlankBtn();
	this.close_btn.setTransform(41,16.5,3.022,3.548);
	this.close_btn._off = true;
	new cjs.ButtonHelper(this.close_btn, 0, 1, 2, false, new lib.BlankBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(8).to({_off:false},0).wait(2));

	// Layer 2
	this.instance = new lib.blotter_panel_in("synched",0,false);
	this.instance.setTransform(211.7,86,1,1,0,0,0,170.7,69.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(9));

	// Layer 6
	this.result_mc = new lib.missed_correct();
	this.result_mc.setTransform(29.1,2.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#81BC00").ss(1,2,1).p("EhMuAAAMCZdAAA");
	this.shape.setTransform(490,195.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.result_mc}]}).wait(10));

	// Layer 5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EhEqAPpIAA/RMCRSAAAIAAfRg");
	mask.setTransform(490.5,95.4);

	// Layer 1
	this.blotter_txt = new cjs.Text("Text goes here", "14px 'Frutiger Next Pro Medium'", "#565656");
	this.blotter_txt.name = "blotter_txt";
	this.blotter_txt.lineHeight = 16;
	this.blotter_txt.lineWidth = 343;
	this.blotter_txt.setTransform(64.1,2);

	this.coaching_txt = new cjs.Text("Text goes here", "14px 'Frutiger Next Pro Medium'", "#565656");
	this.coaching_txt.name = "coaching_txt";
	this.coaching_txt.lineHeight = 16;
	this.coaching_txt.lineWidth = 482;
	this.coaching_txt.setTransform(495.2,4.6);

	this.blotter_txt.mask = this.coaching_txt.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.coaching_txt},{t:this.blotter_txt}]}).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.1,0,984.2,196.6);


(lib.backgroundMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,showBackground:20,close:35});

	// timeline functions:
	this.frame_0 = function() {
		initBackground(this);
		this.stop();
	}
	this.frame_19 = function() {
		//Init Whirling
		this.bg_mc.gotoAndStop(0);
		this.bg1_btn.gotoAndStop(0);
	}
	this.frame_34 = function() {
		this.stop();
	}
	this.frame_39 = function() {
		this.visible = false;
		this.gotoAndStop(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(19).call(this.frame_19).wait(15).call(this.frame_34).wait(5).call(this.frame_39).wait(1));

	// Layer 9
	this.close_btn = new lib.btn_close();
	this.close_btn.setTransform(589,-242,1,1,0,0,0,75.5,19.6);
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(9).to({x:437},6).to({_off:true},20).wait(5));

	// Layer 1
	this.bg3_btn = new lib.note_button_1();
	this.bg3_btn.setTransform(-593,99.7,1,1,0,0,0,80,79.5);

	this.timeline.addTween(cjs.Tween.get(this.bg3_btn).wait(12).to({x:-432},6).wait(17).to({alpha:0},4).wait(1));

	// Layer 2
	this.bg2_btn = new lib.note_button_1();
	this.bg2_btn.setTransform(-593,-39.5,1,1,0,0,0,80,79.5);

	this.timeline.addTween(cjs.Tween.get(this.bg2_btn).wait(9).to({x:-432},6).wait(20).to({alpha:0},4).wait(1));

	// Layer 3
	this.bg1_btn = new lib.note_button_1();
	this.bg1_btn.setTransform(-593,-178.7,1,1,0,0,0,80,79.5);

	this.timeline.addTween(cjs.Tween.get(this.bg1_btn).wait(6).to({x:-432},6).wait(23).to({alpha:0},4).wait(1));

	// bg
	this.bg_mc = new lib.news();
	this.bg_mc.setTransform(74.9,588.1,1,1,0,0,0,422,292.5);

	this.timeline.addTween(cjs.Tween.get(this.bg_mc).wait(20).to({y:32.5},12).to({_off:true},3).wait(5));

	// bg
	this.instance = new lib.resources_bg_1();
	this.instance.setTransform(0,1.5,1,1,0,0,0,512,326.5);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1},6).wait(29).to({alpha:0},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-673,-325,1337.6,1205.6);


(lib.answer = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Layer 3
	this.instance = new lib.marked("synched",1);
	this.instance.setTransform(191.3,3.3);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(1).to({startPosition:0},0).wait(1));

	// Layer 2
	this.answer_txt = new cjs.Text("Alison should have immediately \ncontacted Victor Yeo, Head of \nCorporate Strategy at SCP, to \nseek SCP’s consent to contact \nBSC management in order to get \nBSC’s consent for her to \nparticipate in the SCP \nengagement with Tammy. ", "14px 'Frutiger Next Pro Medium'", "#575757");
	this.answer_txt.name = "answer_txt";
	this.answer_txt.lineHeight = 14;
	this.answer_txt.lineWidth = 204;
	this.answer_txt.setTransform(13.8,51.4);

	this.letter_txt = new cjs.Text("A.", "bold 22px 'Frutiger Next Pro Bold'", "#002874");
	this.letter_txt.name = "letter_txt";
	this.letter_txt.lineHeight = 22;
	this.letter_txt.lineWidth = 100;
	this.letter_txt.setTransform(15,7.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.letter_txt},{t:this.answer_txt}]}).wait(3));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ax/S6MAAAglzMAj/AAAMAAAAlzg");
	this.shape.setTransform(115.3,121.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4F6FB").s().p("Ax/S6MAAAglzMAj/AAAMAAAAlzg");
	this.shape_1.setTransform(115.3,121.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,230.6,242.2);


(lib.alison_pic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 2
	this.instance = new lib.green_tick_1("synched",0);
	this.instance.setTransform(122.7,114.8,1,1,-6,0,0,16.1,13.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.bio_pic_alison();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,163,171);


(lib.top_bar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.help_btn.on("click", function(event){
			helpPressed();
		});
		
		this.pause_btn.on("click", function(event){
			pausePressed(event.currentTarget);
		});
		
		this.home_btn.on("click", function(event){
			if (exportRoot.currentFrame<150) return;
			event.currentTarget.parent.gotoAndPlay(1);
		});
		
		this.stop();
	}
	this.frame_9 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(9).call(this.frame_9).wait(1));

	// Layer 4
	this.instance = new lib.ConfirmHomeMC();
	this.instance.setTransform(984.5,51.4,0.14,0.14,0,0,0,135,45.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({regX:134.9,regY:45.1,scaleX:1,scaleY:1,x:881.2,y:97.9},8).wait(1));

	// Layer 2
	this.home_btn = new lib.btn_close_main();
	this.home_btn.setTransform(997.9,25.8,1,1,0,0,0,18.6,18.6);
	new cjs.ButtonHelper(this.home_btn, 0, 1, 1);

	this.pause_btn = new lib.btn_pause_mc();
	this.pause_btn.setTransform(952.9,25.8,1,1,0,0,0,18.6,18.6);

	this.help_btn = new lib.btn_help();
	this.help_btn.setTransform(907.9,25.8,1,1,0,0,0,18.6,18.6);
	new cjs.ButtonHelper(this.help_btn, 0, 1, 1);

	this.text = new cjs.Text("Confidentiality", "25px 'Frutiger Next Pro Medium'", "#88C11B");
	this.text.lineHeight = 27;
	this.text.lineWidth = 198;
	this.text.setTransform(125.5,12.9);

	this.text_1 = new cjs.Text("Deloitte ", "bold 26px 'Frutiger Next Pro Bold'", "#0A307A");
	this.text_1.textAlign = "right";
	this.text_1.lineHeight = 28;
	this.text_1.lineWidth = 117;
	this.text_1.setTransform(118,12.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_1},{t:this.text},{t:this.help_btn},{t:this.pause_btn},{t:this.home_btn}]}).wait(10));

	// Layer 1
	this.instance_1 = new lib.topBarBg();
	this.instance_1.setTransform(512,28,1,1,0,0,0,512,28);
	this.instance_1.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11,-11,1050,82);


(lib.neuralMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 46
	this.instance = new lib.neuralConnectionAnim("single",14);
	this.instance.setTransform(-94.1,211.4,0.5,0.5,0,0,0,9,8.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(52).to({_off:false},0).to({x:-171.5,y:212},3).to({x:-522.5,y:-190},1).to({x:-172.5,y:-187},7).to({y:11},4).to({x:-430.5,y:13},5).to({x:-429.5,y:161},5).to({x:-522.5,y:11},1).to({x:-405.9},2).to({x:-347.5},1).to({x:-172.5},3).to({y:-88},5).to({x:-26.5,y:-87},4).to({x:-29.5,y:-285},5).to({x:450.5},5).to({y:-75},5).to({x:490.5},4).to({x:540.5,y:84},1).to({x:350.5},6).to({x:298.5,y:144},5).to({x:91.5},5).to({x:87.5,y:-138},5).to({x:-25.5},4).to({y:-90},5).to({x:-172.5},5).to({y:-255},5).to({x:-393.5},4).to({x:-27.5,y:-368},1).to({y:-138},6).to({x:87.5},4).to({x:92.5,y:143},5).to({x:142.5},5).to({x:145.5,y:211},4).to({x:197.5,y:269},1).to({x:299.5,y:144},5).to({x:3.5},5).to({x:-42.5,y:211},5).to({x:-68.3,y:211.2},1).wait(1));

	// Layer 45
	this.instance_1 = new lib.neuralConnectionAnim("single",14);
	this.instance_1.setTransform(477.1,84,0.5,0.5,0,0,0,9,8.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(52).to({_off:false},0).to({x:540.5},2).to({x:490.5,y:-75},1).to({x:450.5},4).to({y:-285},5).to({x:-29.5},5).to({x:-26.5,y:-87},5).to({x:-172.5,y:-88},4).to({y:-48.4},2).to({y:-28.6},1).to({y:11},2).to({x:-522.5},6).to({x:-429.5,y:161},1).to({x:-430.5,y:13},5).to({x:-172.5,y:11},5).to({y:-187},4).to({x:-522.5,y:-190},7).to({x:-171.5,y:212},1).to({x:-42.5,y:211},5).to({x:3.5,y:144},5).to({x:299.5},5).to({x:197.5,y:269},5).to({x:145.5,y:211},1).to({x:142.5,y:143},4).to({x:92.5},5).to({x:87.5,y:-138},5).to({x:-27.5},4).to({y:-368},6).to({x:-393.5,y:-255},1).to({x:-172.5},4).to({y:-90},5).to({x:-25.5},5).to({y:-138},5).to({x:87.5},4).to({x:91.5,y:144},5).to({x:298.5},5).to({x:350.5,y:84},5).to({x:445.5},3).wait(1));

	// Layer 44
	this.instance_2 = new lib.neuralConnectionAnim("single",14);
	this.instance_2.setTransform(91.5,144,0.5,0.5,0,0,0,9,8.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(52).to({_off:false},0).to({x:87.5,y:-138},5).to({x:-25.5},4).to({y:-90},5).to({x:-172.5},5).to({y:-255},5).to({x:-393.5},4).to({x:-27.5,y:-368},1).to({y:-138},6).to({x:87.5},4).to({x:92.5,y:143},5).to({x:142.5},5).to({x:145.5,y:211},4).to({x:197.5,y:269},1).to({x:299.5,y:144},5).to({x:3.5},5).to({x:-42.5,y:211},5).to({x:-171.5,y:212},5).to({x:-522.5,y:-190},1).to({x:-172.5,y:-187},7).to({y:11},4).to({x:-430.5,y:13},5).to({x:-429.5,y:161},5).to({x:-522.5,y:11},1).to({x:-172.5},6).to({y:-88},5).to({x:-26.5,y:-87},4).to({x:-29.5,y:-285},5).to({x:450.5},5).to({y:-75},5).to({x:490.5},4).to({x:540.5,y:84},1).to({x:350.5},6).to({x:298.5,y:144},5).to({x:132.9},4).wait(1));

	// Layer 43
	this.instance_3 = new lib.neuralConnectionAnim("single",14);
	this.instance_3.setTransform(258.7,194,0.5,0.5,0,0,0,9,8.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(52).to({_off:false},0).to({x:197.5,y:269},3).to({x:145.5,y:211},1).to({x:142.5,y:143},4).to({x:92.5},5).to({x:87.5,y:-138},5).to({x:-27.5},4).to({y:-368},6).to({x:-393.5,y:-255},1).to({x:-172.5},4).to({y:-90},5).to({x:-25.5},5).to({y:-138},5).to({x:87.5},4).to({x:91.5,y:144},5).to({x:298.5},5).to({x:350.5,y:84},5).to({x:540.5},6).to({x:490.5,y:-75},1).to({x:450.5},4).to({y:-285},5).to({x:-29.5},5).to({x:-26.5,y:-87},5).to({x:-172.5,y:-88},4).to({y:11},5).to({x:-522.5},6).to({x:-429.5,y:161},1).to({x:-430.5,y:13},5).to({x:-172.5,y:11},5).to({y:-187},4).to({x:-522.5,y:-190},7).to({x:-171.5,y:212},1).to({x:-42.5,y:211},5).to({x:3.5,y:144},5).to({x:299.5},5).to({x:279.1,y:169},1).wait(1));

	// Layer 42
	this.instance_4 = new lib.neuralConnectionAnim("single",14);
	this.instance_4.setTransform(-347.5,11,0.5,0.5,0,0,0,9,8.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(52).to({_off:false},0).to({x:-172.5},3).to({y:-88},5).to({x:-26.5,y:-87},4).to({x:-29.5,y:-285},5).to({x:450.5},5).to({y:-75},5).to({x:490.5},4).to({x:540.5,y:84},1).to({x:350.5},6).to({x:298.5,y:144},5).to({x:91.5},5).to({x:87.5,y:-138},5).to({x:-25.5},4).to({y:-90},5).to({x:-172.5},5).to({y:-255},5).to({x:-393.5},4).to({x:-27.5,y:-368},1).to({y:-138},6).to({x:87.5},4).to({x:92.5,y:143},5).to({x:142.5},5).to({x:145.5,y:211},4).to({x:197.5,y:269},1).to({x:299.5,y:144},5).to({x:3.5},5).to({x:-42.5,y:211},5).to({x:-171.5,y:212},5).to({x:-522.5,y:-190},1).to({x:-172.5,y:-187},7).to({y:11},4).to({x:-430.5,y:13},5).to({x:-429.5,y:161},5).to({x:-522.5,y:11},1).to({x:-405.9},2).wait(1));

	// Layer 41
	this.instance_5 = new lib.neuralConnectionAnim("single",14);
	this.instance_5.setTransform(-172.5,-28.6,0.5,0.5,0,0,0,9,8.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(52).to({_off:false},0).to({y:11},2).to({x:-522.5},6).to({x:-429.5,y:161},1).to({x:-430.5,y:13},5).to({x:-172.5,y:11},5).to({y:-187},4).to({x:-522.5,y:-190},7).to({x:-171.5,y:212},1).to({x:-42.5,y:211},5).to({x:3.5,y:144},5).to({x:299.5},5).to({x:197.5,y:269},5).to({x:145.5,y:211},1).to({x:142.5,y:143},4).to({x:92.5},5).to({x:87.5,y:-138},5).to({x:-27.5},4).to({y:-368},6).to({x:-393.5,y:-255},1).to({x:-172.5},4).to({y:-90},5).to({x:-25.5},5).to({y:-138},5).to({x:87.5},4).to({x:91.5,y:144},5).to({x:298.5},5).to({x:350.5,y:84},5).to({x:540.5},6).to({x:490.5,y:-75},1).to({x:450.5},4).to({y:-285},5).to({x:-29.5},5).to({x:-26.5,y:-87},5).to({x:-172.5,y:-88},4).to({y:-48.4},2).wait(1));

	// Layer 40
	this.instance_6 = new lib.neuralConnectionAnim("single",14);
	this.instance_6.setTransform(-27.5,-368,0.5,0.5,0,0,0,9,8.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(52).to({_off:false},0).to({y:-138},6).to({x:87.5},4).to({x:92.5,y:143},5).to({x:142.5},5).to({x:145.5,y:211},4).to({x:197.5,y:269},1).to({x:299.5,y:144},5).to({x:3.5},5).to({x:-42.5,y:211},5).to({x:-171.5,y:212},5).to({x:-522.5,y:-190},1).to({x:-172.5,y:-187},7).to({y:11},4).to({x:-430.5,y:13},5).to({x:-429.5,y:161},5).to({x:-522.5,y:11},1).to({x:-172.5},6).to({y:-88},5).to({x:-26.5,y:-87},4).to({x:-29.5,y:-285},5).to({x:450.5},5).to({y:-75},5).to({x:490.5},4).to({x:540.5,y:84},1).to({x:350.5},6).to({x:298.5,y:144},5).to({x:91.5},5).to({x:87.5,y:-138},5).to({x:-25.5},4).to({y:-90},5).to({x:-172.5},5).to({y:-255},5).to({x:-393.5},4).wait(1));

	// Layer 39
	this.instance_7 = new lib.neuralConnectionAnim("single",14);
	this.instance_7.setTransform(-393.5,-255,0.5,0.5,0,0,0,9,8.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(52).to({_off:false},0).to({x:-172.5},4).to({y:-90},5).to({x:-25.5},5).to({y:-138},5).to({x:87.5},4).to({x:91.5,y:144},5).to({x:298.5},5).to({x:350.5,y:84},5).to({x:540.5},6).to({x:490.5,y:-75},1).to({x:450.5},4).to({y:-285},5).to({x:-29.5},5).to({x:-26.5,y:-87},5).to({x:-172.5,y:-88},4).to({y:11},5).to({x:-522.5},6).to({x:-429.5,y:161},1).to({x:-430.5,y:13},5).to({x:-172.5,y:11},5).to({y:-187},4).to({x:-522.5,y:-190},7).to({x:-171.5,y:212},1).to({x:-42.5,y:211},5).to({x:3.5,y:144},5).to({x:299.5},5).to({x:197.5,y:269},5).to({x:145.5,y:211},1).to({x:142.5,y:143},4).to({x:92.5},5).to({x:87.5,y:-138},5).to({x:-27.5},4).to({y:-368},6).wait(1));

	// Layer 38
	this.instance_8 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_8.setTransform(453,-281.5,1,1,0,0,0,9,8.5);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(51).to({_off:false},0).wait(149));

	// Layer 37
	this.instance_9 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_9.setTransform(489,191,1,1,0,0,0,9,8.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(52).to({_off:false},0).wait(148));

	// Layer 36
	this.instance_10 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_10.setTransform(491,87.5,1,1,0,0,0,9,8.5);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(49).to({_off:false},0).wait(151));

	// Layer 35
	this.instance_11 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_11.setTransform(495,-71.5,1,1,0,0,0,9,8.5);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(45).to({_off:false},0).wait(155));

	// Layer 34
	this.instance_12 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_12.setTransform(303,147.5,1,1,0,0,0,9,8.5);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(44).to({_off:false},0).wait(156));

	// Layer 33
	this.instance_13 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_13.setTransform(202,272.5,1,1,0,0,0,9,8.5);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(43).to({_off:false},0).wait(157));

	// Layer 32
	this.instance_14 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_14.setTransform(355,87.5,1,1,0,0,0,9,8.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(43).to({_off:false},0).wait(157));

	// Layer 31
	this.instance_15 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_15.setTransform(455,-69.5,1,1,0,0,0,9,8.5);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(43).to({_off:false},0).wait(157));

	// Layer 29
	this.instance_16 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_16.setTransform(302,87.5,1,1,0,0,0,9,8.5);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(41).to({_off:false},0).wait(159));

	// Layer 28
	this.instance_17 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_17.setTransform(187,-134.5,1,1,0,0,0,9,8.5);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(39).to({_off:false},0).wait(161));

	// Layer 27
	this.instance_18 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_18.setTransform(186,-134.5,1,1,0,0,0,9,8.5);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(38).to({_off:false},0).wait(162));

	// Layer 26
	this.instance_19 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_19.setTransform(147,147.5,1,1,0,0,0,9,8.5);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(37).to({_off:false},0).wait(163));

	// Layer 25
	this.instance_20 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_20.setTransform(150,214.5,1,1,0,0,0,9,8.5);
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(36).to({_off:false},0).wait(164));

	// Layer 24
	this.instance_21 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_21.setTransform(98,213.5,1,1,0,0,0,9,8.5);
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(34).to({_off:false},0).wait(166));

	// Layer 23
	this.instance_22 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_22.setTransform(96,147.5,1,1,0,0,0,9,8.5);
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(34).to({_off:false},0).wait(166));

	// Layer 22
	this.instance_23 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_23.setTransform(92,-134.5,1,1,0,0,0,9,8.5);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(34).to({_off:false},0).wait(166));

	// Layer 30
	this.instance_24 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_24.setTransform(7,147.5,1,1,0,0,0,9,8.5);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(30).to({_off:false},0).wait(170));

	// Layer 21
	this.instance_25 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_25.setTransform(-26,-283.5,1,1,0,0,0,9,8.5);
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(30).to({_off:false},0).wait(170));

	// Layer 20
	this.instance_26 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_26.setTransform(-23,-134.5,1,1,0,0,0,9,8.5);
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(29).to({_off:false},0).wait(171));

	// Layer 19
	this.instance_27 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_27.setTransform(-38,214.5,1,1,0,0,0,9,8.5);
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(27).to({_off:false},0).wait(173));

	// Layer 18
	this.instance_28 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_28.setTransform(-167,214.5,1,1,0,0,0,9,8.5);
	this.instance_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(23).to({_off:false},0).wait(177));

	// Layer 17
	this.instance_29 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_29.setTransform(-22,-84.5,1,1,0,0,0,9,8.5);
	this.instance_29._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(23).to({_off:false},0).wait(177));

	// Layer 16
	this.instance_30 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_30.setTransform(-168,14.5,1,1,0,0,0,9,8.5);
	this.instance_30._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(24).to({_off:false},0).wait(176));

	// Layer 15
	this.instance_31 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_31.setTransform(-169,-182.4,1,1,0,0,0,9,8.5);
	this.instance_31._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(26).to({_off:false},0).wait(174));

	// Layer 14
	this.instance_32 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_32.setTransform(-168,-135.4,1,1,0,0,0,9,8.5);
	this.instance_32._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(23).to({_off:false},0).wait(177));

	// Layer 13
	this.instance_33 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_33.setTransform(-168,-250.4,1,1,0,0,0,9,8.5);
	this.instance_33._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(23).to({_off:false},0).wait(177));

	// Layer 12
	this.instance_34 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_34.setTransform(-168,-135.4,1,1,0,0,0,9,8.5);
	this.instance_34._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(23).to({_off:false},0).wait(177));

	// Layer 11
	this.instance_35 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_35.setTransform(-169,-251.4,1,1,0,0,0,9,8.5);
	this.instance_35._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(23).to({_off:false},0).wait(177));

	// Layer 10
	this.instance_36 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_36.setTransform(-169,-135.4,1,1,0,0,0,9,8.5);
	this.instance_36._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(23).to({_off:false},0).wait(177));

	// Layer 9
	this.instance_37 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_37.setTransform(-168,-249.4,1,1,0,0,0,9,8.5);
	this.instance_37._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(23).to({_off:false},0).wait(177));

	// Layer 8
	this.instance_38 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_38.setTransform(-168,-85,1,1,0,0,0,9,8.5);
	this.instance_38._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_38).wait(17).to({_off:false},0).wait(183));

	// Layer 7
	this.instance_39 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_39.setTransform(-388.5,-186.2,1,1,0,0,0,9,8.5);
	this.instance_39._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_39).wait(17).to({_off:false},0).wait(183));

	// Layer 6
	this.instance_40 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_40.setTransform(-168,-86,1,1,0,0,0,9,8.5);
	this.instance_40._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_40).wait(17).to({_off:false},0).wait(183));

	// Layer 5
	this.instance_41 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_41.setTransform(-389,-251.5,1,1,0,0,0,9,8.5);
	this.instance_41._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_41).wait(14).to({_off:false},0).wait(186));

	// Layer 4
	this.instance_42 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_42.setTransform(-426,165.5,1,1,0,0,0,9,8.5);
	this.instance_42._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_42).wait(14).to({_off:false},0).wait(186));

	// Layer 3
	this.instance_43 = new lib.neuralConnectionAnim("synched",0,false);
	this.instance_43.setTransform(-426,17.5,1,1,0,0,0,9,8.5);
	this.instance_43._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_43).wait(13).to({_off:false},0).wait(187));

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EiOpAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk8BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsABoreDgQhfAdhQBBQitCMAUDjQHrGQKnAWQBYACA+AkQiwEflzBiQmNBpl1CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_1 = new cjs.Graphics().p("EiM1AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_2 = new cjs.Graphics().p("EiLBAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_3 = new cjs.Graphics().p("EiJNAyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_4 = new cjs.Graphics().p("EiHZAyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_5 = new cjs.Graphics().p("EiFmAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_6 = new cjs.Graphics().p("EiDyAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_7 = new cjs.Graphics().p("EiB+AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_8 = new cjs.Graphics().p("EiAKAyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_9 = new cjs.Graphics().p("Eh+WAyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_10 = new cjs.Graphics().p("Eh8jAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_11 = new cjs.Graphics().p("Eh6vAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_12 = new cjs.Graphics().p("Eh47AyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_13 = new cjs.Graphics().p("Eh3HAyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_14 = new cjs.Graphics().p("Eh1TAyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_15 = new cjs.Graphics().p("EhzgAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_16 = new cjs.Graphics().p("EhxsAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_17 = new cjs.Graphics().p("Ehv4AyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_18 = new cjs.Graphics().p("EhuEAyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_19 = new cjs.Graphics().p("EhsRAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk8BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsABoreDgQhfAdhQBBQitCMAUDjQHrGQKnAWQBYACA+AkQiwEflzBiQmNBpl1CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_20 = new cjs.Graphics().p("EhqdAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_21 = new cjs.Graphics().p("EhopAyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_22 = new cjs.Graphics().p("Ehm1AyxMAAAhlNMCfzAAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlKBjQk7BfjfDWQhSBPASB1MApaAOsQAkFYmMA2QsABordDgQhfAdhRBBQitCMAUDjQHrGQKnAWQBZACA9AkQiwEflzBiQmNBpl0CyQhLAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_23 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_24 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_25 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_26 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_27 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_28 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_29 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_30 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_31 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_32 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_33 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_34 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_35 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_36 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_37 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_38 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_39 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_40 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_41 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_42 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_43 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_44 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_45 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_46 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_47 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_48 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_49 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_50 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_51 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_52 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_53 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_54 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_55 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_56 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");
	var mask_graphics_57 = new cjs.Graphics().p("Ehm0AyxMAAAhlNMCf0AAAIAAgKIG4AAIAAgKIEEAAILQEiIAoAAIBGBQIAABQIhQBaQg3AZgPAPIhaAAIpYB4IAABaIHgDcQAqEUlLBjQk7BfjeDWQhSBPASB1MApaAOsQAjFYmLA2QsBBordDgQhfAdhRBBQitCMAVDjQHrGQKmAWQBZACA+AkQixEflyBiQmOBpl0CyQhKAjgnBNIAAA8IKyEsIuEH+IAAA8IlyDJIAABtg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-913,y:-36}).wait(1).to({graphics:mask_graphics_1,x:-901.4,y:-36}).wait(1).to({graphics:mask_graphics_2,x:-889.8,y:-36}).wait(1).to({graphics:mask_graphics_3,x:-878.3,y:-36}).wait(1).to({graphics:mask_graphics_4,x:-866.7,y:-36}).wait(1).to({graphics:mask_graphics_5,x:-855.1,y:-36}).wait(1).to({graphics:mask_graphics_6,x:-843.5,y:-36}).wait(1).to({graphics:mask_graphics_7,x:-831.9,y:-36}).wait(1).to({graphics:mask_graphics_8,x:-820.4,y:-36}).wait(1).to({graphics:mask_graphics_9,x:-808.8,y:-36}).wait(1).to({graphics:mask_graphics_10,x:-797.2,y:-36}).wait(1).to({graphics:mask_graphics_11,x:-785.6,y:-36}).wait(1).to({graphics:mask_graphics_12,x:-774.1,y:-36}).wait(1).to({graphics:mask_graphics_13,x:-762.5,y:-36}).wait(1).to({graphics:mask_graphics_14,x:-750.9,y:-36}).wait(1).to({graphics:mask_graphics_15,x:-739.3,y:-36}).wait(1).to({graphics:mask_graphics_16,x:-727.7,y:-36}).wait(1).to({graphics:mask_graphics_17,x:-716.2,y:-36}).wait(1).to({graphics:mask_graphics_18,x:-704.6,y:-36}).wait(1).to({graphics:mask_graphics_19,x:-693,y:-36}).wait(1).to({graphics:mask_graphics_20,x:-681.4,y:-36}).wait(1).to({graphics:mask_graphics_21,x:-669.8,y:-36}).wait(1).to({graphics:mask_graphics_22,x:-658.3,y:-36}).wait(1).to({graphics:mask_graphics_23,x:-635.2,y:-36}).wait(1).to({graphics:mask_graphics_24,x:-612.1,y:-36}).wait(1).to({graphics:mask_graphics_25,x:-588.9,y:-36}).wait(1).to({graphics:mask_graphics_26,x:-565.8,y:-36}).wait(1).to({graphics:mask_graphics_27,x:-542.6,y:-36}).wait(1).to({graphics:mask_graphics_28,x:-519.5,y:-36}).wait(1).to({graphics:mask_graphics_29,x:-496.3,y:-36}).wait(1).to({graphics:mask_graphics_30,x:-473.1,y:-36}).wait(1).to({graphics:mask_graphics_31,x:-450,y:-36}).wait(1).to({graphics:mask_graphics_32,x:-426.8,y:-36}).wait(1).to({graphics:mask_graphics_33,x:-403.7,y:-36}).wait(1).to({graphics:mask_graphics_34,x:-380.5,y:-36}).wait(1).to({graphics:mask_graphics_35,x:-357.4,y:-36}).wait(1).to({graphics:mask_graphics_36,x:-334.2,y:-36}).wait(1).to({graphics:mask_graphics_37,x:-311,y:-36}).wait(1).to({graphics:mask_graphics_38,x:-287.9,y:-36}).wait(1).to({graphics:mask_graphics_39,x:-264.7,y:-36}).wait(1).to({graphics:mask_graphics_40,x:-241.6,y:-36}).wait(1).to({graphics:mask_graphics_41,x:-218.4,y:-36}).wait(1).to({graphics:mask_graphics_42,x:-195.2,y:-36}).wait(1).to({graphics:mask_graphics_43,x:-172.1,y:-36}).wait(1).to({graphics:mask_graphics_44,x:-148.9,y:-36}).wait(1).to({graphics:mask_graphics_45,x:-125.8,y:-36}).wait(1).to({graphics:mask_graphics_46,x:-102.6,y:-36}).wait(1).to({graphics:mask_graphics_47,x:-79.5,y:-36}).wait(1).to({graphics:mask_graphics_48,x:-56.3,y:-36}).wait(1).to({graphics:mask_graphics_49,x:-33.1,y:-36}).wait(1).to({graphics:mask_graphics_50,x:-10,y:-36}).wait(1).to({graphics:mask_graphics_51,x:13.2,y:-36}).wait(1).to({graphics:mask_graphics_52,x:36.3,y:-36}).wait(1).to({graphics:mask_graphics_53,x:59.5,y:-36}).wait(1).to({graphics:mask_graphics_54,x:82.6,y:-36}).wait(1).to({graphics:mask_graphics_55,x:105.8,y:-36}).wait(1).to({graphics:mask_graphics_56,x:129,y:-36}).wait(1).to({graphics:mask_graphics_57,x:152.1,y:-36}).wait(143));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFF00").ss(1,1,1).p("AYX34IAHH4IS9AGAYewAIANQUAJN6iIAMKdIPFAFEBB7gqoIACDjINGADEBCMgGGIADH+EA8dgGEIFvgCEBIZgGJIOKgFEBIfgKmIgGEdIgkYnIgYQDIgECqEBB9gnFMAAPAg/IGNgDEA2XANwIj8EyIVagEIK+gDEBAXAihIHGAAIRcAAEAqYASkIgDk+EAlLASlIFNgBIIDgCInvJZIUlACEAqfAcKIAAgPIANAAgEAqfAb7IgHpXAKNb2IH7ABIgVsPAjkb0INxACIAMKeIAEDZABFVPIkpGlEAKZAmUIIBAEIFfADEASlAs1IgLmdIgSqhIYXAEEAF7AmRIEeADEAazAvLIhCBQEAazAvLISqAAEAqpAsNIgKwDIvsTBEAIbAvLISYAAAJZwFMAA0Ar7EhGmgigIE2ACIAAKOIAAQyEhBwgpOIAAGwMAiiAAQIEEACEhBwgYQI2CgQEgM8gnWIEKABIgGpFEgfOgkYIAACKIAAKYIAAHlIWtAGIgR3KMBKvAAQEgkigIkIFUADIWyAMIJXAFEg0ogQYIVaAHIAAHwIAAPlMgoXAANIAaXOIAHFwAoamiIgChzIgFn2IR6AGA0w3uIqegIMgiigAaEgfOAmYIAABxEgksAmbIFegDEgK7AmKIkhGXEgK7AmKIDdgDA2yHCIocACIAAfUIUTgOEhMOAedIFDACMAgdAAIEhHlAHRIxTAFEhHsAAyIAHGfApgbzIF8ABInXKW");
	this.shape.setTransform(23,-39);

	this.shape.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(200));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-547,-350,37.3,622);


(lib.helpMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"open":1,"close":13});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		initHelp(this);
	}
	this.frame_12 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.visible = false;
		this.gotoAndStop(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(12).call(this.frame_12).wait(9).call(this.frame_21).wait(1));

	// Layer 3
	this.close_btn = new lib.btn_close();
	this.close_btn.setTransform(588,-242,1,1,0,0,0,75.5,19.6);
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(3).to({x:438},7).to({_off:true},3).wait(9));

	// bg
	this.items_mc = new lib.glossary_full_screen();
	this.items_mc.setTransform(4.5,-598.5,1,1,0,0,0,516.5,326.5);

	this.timeline.addTween(cjs.Tween.get(this.items_mc).wait(1).to({y:1.5},8).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-512.6,-925,1176.2,702.6);


(lib.glossaryMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"open":1,"close":13});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		initGlossary(this);
	}
	this.frame_12 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.visible = false;
		this.gotoAndStop(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(12).call(this.frame_12).wait(9).call(this.frame_21).wait(1));

	// Layer 3
	this.close_btn = new lib.btn_close();
	this.close_btn.setTransform(588,-242,1,1,0,0,0,75.5,19.6);
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(3).to({x:438},7).to({_off:true},3).wait(9));

	// bg
	this.glossary_mc = new lib.glossary_full_screen();
	this.glossary_mc.setTransform(4.5,-598.5,1,1,0,0,0,516.5,326.5);

	this.timeline.addTween(cjs.Tween.get(this.glossary_mc).wait(1).to({y:1.5},8).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-512.6,-925,1176.2,702.6);


(lib.blotterCoachingItemsMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.item1_mc = new lib.blotter_coaching_item();
	this.item1_mc.setTransform(513.2,87.8,1,1,0,0,0,512,87.8);

	this.timeline.addTween(cjs.Tween.get(this.item1_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.6,-4.7,983.5,201);


(lib.blotterAnim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		initMiniBlotter(this.blotter_mc);
	}
	this.frame_11 = function() {
		this.stop();
		this.close_btn.on("click", function(event){
			event.currentTarget.parent.play();
		});
		
		this.popup_mc.close_btn.removeAllEventListeners();
		this.popup_mc.close_btn.on("click", function(event){
			var mc = event.currentTarget.parent;
			mc.visible = false;
		});
	}
	this.frame_26 = function() {
		initChoices();
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(10).call(this.frame_11).wait(15).call(this.frame_26).wait(1));

	// Layer 5
	this.popup_mc = new lib.blotterPopup();
	this.popup_mc.setTransform(1,0.2,1,1,0,0,0,216,110.7);
	this.popup_mc.alpha = 0;
	this.popup_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.popup_mc).wait(6).to({_off:false},0).to({alpha:1},5).wait(16));

	// Layer 1
	this.close_btn = new lib.btn_close();
	this.close_btn.setTransform(600.9,264.8,1,1,0,0,0,75.5,19.6);
	this.close_btn._off = true;
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(6).to({_off:false},0).to({x:406.4},5).wait(9).to({x:596.4,mode:"synched",startPosition:0},6).wait(1));

	// Layer 2
	this.instance = new lib.pen_1("synched",0);
	this.instance.setTransform(95.8,337.2,1,1,75,0,0,14.7,212.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({regX:14.8,rotation:45.2,x:96.8,y:314.3},5).wait(9).to({startPosition:0},0).to({regX:14.7,rotation:75,x:95.8,y:337.2},6).wait(1));

	// Layer 3
	this.blotter_mc = new lib.blotter_side();
	this.blotter_mc.setTransform(708.5,-8,1,1,0,0,0,194,317);
	this.blotter_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.blotter_mc).wait(1).to({_off:false},0).to({x:317.5},7).wait(12).to({x:713.5},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.biographiesMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{showBiog:30,"close":39,noTammy:46,showBiog2:75,close2:84});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		if (!checkCompleted("alison")) this.gotoAndPlay("noTammy");
	}
	this.frame_29 = function() {
		initBiogButtons(this);
		this.biog = "james";
		this.james_btn.gotoAndStop(1);
	}
	this.frame_30 = function() {
		initBiog(this.biog_mc);
	}
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_45 = function() {
		this.visible = false;
		this.gotoAndStop(0);
	}
	this.frame_74 = function() {
		initBiogButtons(this, true);
		this.biog = "james";
		this.james_btn.gotoAndStop(1);
	}
	this.frame_75 = function() {
		initBiog(this.biog_mc);
	}
	this.frame_84 = function() {
		this.stop();
	}
	this.frame_90 = function() {
		this.visible = false;
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(28).call(this.frame_29).wait(1).call(this.frame_30).wait(9).call(this.frame_39).wait(6).call(this.frame_45).wait(29).call(this.frame_74).wait(1).call(this.frame_75).wait(9).call(this.frame_84).wait(6).call(this.frame_90).wait(1));

	// Layer 10
	this.close_btn = new lib.btn_close();
	this.close_btn.setTransform(438.5,-232.2,1,1,0,0,0,75.5,19.6);
	this.close_btn._off = true;
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(29).to({_off:false},0).to({_off:true},11).wait(34).to({_off:false},0).to({_off:true},11).wait(6));

	// Layer 1
	this.james_btn = new lib.james_pic();
	this.james_btn.setTransform(-412.1,-351.3,1,1,0,0,0,76.5,80.5);

	this.timeline.addTween(cjs.Tween.get(this.james_btn).wait(21).to({y:-171.3},8).wait(11).to({alpha:0},5).wait(1).to({y:-351.3,alpha:1},0).wait(20).to({y:-171.3},8).wait(11).to({alpha:0},5).wait(1));

	// Layer 2
	this.alison_btn = new lib.alison_pic();
	this.alison_btn.setTransform(-420.6,-355.3,1,1,0,0,0,81.5,85.5);

	this.timeline.addTween(cjs.Tween.get(this.alison_btn).wait(17).to({y:-43.3},8).wait(15).to({alpha:0},5).wait(1).to({y:-355.3,alpha:1},0).wait(16).to({y:-43.3},8).wait(15).to({alpha:0},5).wait(1));

	// Layer 3
	this.tammy_btn = new lib.tammy_pic();
	this.tammy_btn.setTransform(-431.4,-355.7,1,1,0,0,0,76,80.5);

	this.timeline.addTween(cjs.Tween.get(this.tammy_btn).wait(12).to({y:76.3},10).wait(18).to({alpha:0},5).to({_off:true},1).wait(45));

	// Layer 4
	this.kevin_btn = new lib.kevin_pic();
	this.kevin_btn.setTransform(-414.7,-356,1,1,0,0,0,88.5,85.5);

	this.timeline.addTween(cjs.Tween.get(this.kevin_btn).wait(7).to({y:196},13).wait(20).to({alpha:0},5).wait(1).to({y:-356,alpha:1},0).wait(6).to({y:84},13).wait(20).to({y:196,alpha:0},5).wait(1));

	// bg
	this.biog_mc = new lib.biography();
	this.biog_mc.setTransform(59.4,21.8,1,1,0,0,0,353.8,273.6);
	this.biog_mc.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.biog_mc).wait(30).to({alpha:1},8).to({_off:true},2).wait(6).to({_off:false,alpha:0},0).wait(29).to({alpha:1},8).to({_off:true},2).wait(6));

	// bg
	this.instance = new lib.resources_bg_1();
	this.instance.setTransform(0,1.5,1,1,0,0,0,512,326.5);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1},5).wait(35).to({alpha:0},5).wait(1).to({alpha:1},4).wait(35).to({alpha:0},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-512,-441.5,1024,769.5);


(lib.blotterCoachingItemsScrollMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.panel_mc = new lib.panel_mc();
	this.panel_mc.setTransform(68.1,0,1.996,1.923,0,0,0,2,28.4);

	this.timeline.addTween(cjs.Tween.get(this.panel_mc).wait(1));

	// Layer 3
	this.scrollbar_mc = new lib.scrollbar7();
	this.scrollbar_mc.setTransform(1007,187.7,1,1,0,0,0,5,189);

	this.timeline.addTween(cjs.Tween.get(this.scrollbar_mc).wait(1));

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EhM2AlzMAAAhLlMCZtAAAMAAABLlg");
	mask.setTransform(490.2,240.7);

	// Layer 1
	this.items_mc = new lib.blotterCoachingItemsMC();
	this.items_mc.setTransform(491.1,87.1,1,1,0,0,0,491.1,87.1);

	this.items_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.items_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.6,-25.2,1016.6,515.7);


(lib.blotterCoaching = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_8 = function() {
		initBlotter(this.scroll_mc);
		
		this.close_btn.removeAllEventListeners();
		this.close_btn.on("click", function(event){
			var mc = event.currentTarget.parent;
			mc.gotoAndStop(0);
			mc.visible = false;
			if (continue_after_coaching){
				nextSection = "knowledge_check";
				continue_after_coaching = false;
				exportRoot.play();
			}
		});
		
		this.msg_mc.visible = showBlotterMessage;
		this.msg_mc.close_btn.removeAllEventListeners();
		this.msg_mc.close_btn.on("click", function(event){
			var mc = event.currentTarget.parent;
			mc.visible = false;
		});
	}
	this.frame_14 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(8).call(this.frame_8).wait(6).call(this.frame_14).wait(1));

	// Layer 6
	this.msg_mc = new lib.blotterMessage();
	this.msg_mc.setTransform(-47.5,-29.5,1,1,0,0,0,168.5,71);
	this.msg_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.msg_mc).wait(8).to({_off:false},0).wait(7));

	// Layer 4
	this.close_btn = new lib.btn_close();
	this.close_btn.setTransform(599,-240,1,1,0,0,0,75.5,19.6);
	this.close_btn._off = true;
	new cjs.ButtonHelper(this.close_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.close_btn).wait(8).to({_off:false},0).to({x:404},6).wait(1));

	// Layer 1
	this.scroll_mc = new lib.blotterCoachingItemsScrollMC();
	this.scroll_mc.setTransform(-22,-126.5,1,1,0,0,0,491.1,87.1);
	this.scroll_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.scroll_mc).wait(8).to({_off:false},0).wait(7));

	// Layer 2
	this.instance = new lib.blotter_full_screen("synched",0);
	this.instance.setTransform(4.5,-598.5,1,1,0,0,0,516.5,326.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:1.5},8).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-512,-925,1024,653);


// stage content:



(lib.deloitte_confidentiality = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{intro_message:32,video:49,start:80,start4:109,title:148,initPerson:181,person:201,choices:218,coaching:242,knowledge_check:272,kc_question:288,kc_enter:316,kc_end:345,final_assessment:353,fa_instructions:369,fa_question:379,course_complete:421,course_fail:435,epilogue:447,epilogue_text:453});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
		
		this.start_btn.cursor = "pointer";
		
		this.start_btn.on("mousedown", function(event) {
			if (useDebugFunc){
				debugFunc();
			}else if (scorm!=null && scorm.initialized){
				scormStart();
			}else{
				exportRoot.play();
			}
		});
	}
	this.frame_32 = function() {
		this.intro_btn.on("click", function(event){
			exportRoot.play();
		});
	}
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_49 = function() {
		if (checkCompleted("kevin")){
			showVideo(true, "epilogue");
		}else{
			showVideo(true);
		}
	}
	this.frame_60 = function() {
		this.stop();
		this.skip_btn.on("click", function(event){
			if (checkCompleted("kevin")){
				showVideo(false);
				itemIdx = 0;
				fa_answers = new Array();
				exportRoot.gotoAndPlay("final_assessment");
			}else{
				exportRoot.play();
			}
		});
	}
	this.frame_61 = function() {
		showVideo(false);
	}
	this.frame_80 = function() {
		if (checkCompleted("kevin")){
			this.gotoAndPlay("epilogue");
		}else if (checkCompleted("alison")){
			this.gotoAndPlay("start4");
		}else{
			this.james_mc.alpha = 0;
			this.alison_mc.alpha = 0;
			this.kevin_mc.alpha = 0;
			this.james_mc.gotoAndStop(0);
			this.james_mc.cursor = "pointer";
			this.james_mc.on("click", function(event){
				jamesPressed();
			});
			if (checkCompleted("james")) this.james_mc.tryagain_mc.visible = true;
		
			this.alison_mc.gotoAndStop(1);
			if (!checkCompleted("james")){
				this.alison_mc.cursor = "arrow";
			}else{
				this.alison_mc.cursor = "pointer";
				this.alison_mc.on("click", function(event){
					alisonPressed();
				});
				if (checkCompleted("alison")) this.alison_mc.tryagain_mc.visible = true;
			}
		
			this.kevin_mc.gotoAndStop(3);
			if (!checkCompleted("alison")){
				this.kevin_mc.cursor = "arrow";
			}else{
				this.kevin_mc.on("click", function(event){
					kevinPressed();
				});
				this.kevin_mc.cursor = "pointer";
				if (checkCompleted("kevin")) this.kevin_mc.tryagain_mc.visible = true;
			}
		}
	}
	this.frame_85 = function() {
		setCharacterFade(this.james_mc, true, 0);
	}
	this.frame_91 = function() {
		setCharacterFade(this.alison_mc, checkCompleted("james"), 1);
	}
	this.frame_96 = function() {
		setCharacterFade(this.kevin_mc, checkCompleted("alison"), 3);
	}
	this.frame_107 = function() {
		this.stop();
	}
	this.frame_118 = function() {
		this.james_mc.alpha = 0;
		this.alison_mc.alpha = 0;
		this.kevin_mc.alpha = 0;
		this.tammy_mc.alpha = 0;
		this.james_mc.gotoAndStop(0);
		this.james_mc.cursor = "pointer";
		this.james_mc.on("click", function(event){
			jamesPressed();
		});
		this.james_mc.tryagain_mc.visible = true;
		setCharacterFade(this.james_mc, true, 0);
		
		this.alison_mc.gotoAndStop(1);
		this.alison_mc.cursor = "pointer";
		this.alison_mc.on("click", function(event){
			alisonPressed();
		});
		this.alison_mc.tryagain_mc.visible = true;
		
		this.tammy_mc.gotoAndStop(2);
		this.tammy_mc.on("click", function(event){
			tammyPressed();
		});
		this.tammy_mc.cursor = "pointer";
		if (checkCompleted("tammy")) this.tammy_mc.tryagain_mc.visible = true;
		
		this.kevin_mc.gotoAndStop(3);
		this.kevin_mc.cursor = "pointer";
		this.kevin_mc.on("click", function(event){
			kevinPressed();
		});
		if (checkCompleted("kevin")) this.kevin_mc.tryagain_mc.visible = true;
	}
	this.frame_123 = function() {
		setCharacterFade(this.alison_mc, true, 1);
	}
	this.frame_128 = function() {
		setCharacterFade(this.tammy_mc, true, 2);
	}
	this.frame_133 = function() {
		setCharacterFade(this.kevin_mc, checkCompleted("tammy"), 3);
	}
	this.frame_146 = function() {
		this.stop();
	}
	this.frame_153 = function() {
		initTitle(this);
	}
	this.frame_170 = function() {
		this.stop();
		//initTitle(this);
	}
	this.frame_184 = function() {
		initPersonAnimation(this);
		this.continue_btn.visible = (section.scenario[section.section].speaker == "player");
	}
	this.frame_201 = function() {
		initPerson(this.person_mc);
	}
	this.frame_209 = function() {
		//Pause for dialogue
		pauseForDialogue();
	}
	this.frame_218 = function() {
		initChoices(this);
	}
	this.frame_232 = function() {
		this.stop();
	}
	this.frame_241 = function() {
		this.gotoAndPlay(nextSection);
	}
	this.frame_243 = function() {
		initCoaching(this.coaching_mc);
	}
	this.frame_256 = function() {
		this.stop();
	}
	this.frame_271 = function() {
		sprites.coaching = false;
		this.gotoAndPlay(nextSection);
	}
	this.frame_273 = function() {
		initKnowledgeCheck(this);
	}
	this.frame_288 = function() {
		initKCQuestion(this);
	}
	this.frame_316 = function() {
		this.stop();
	}
	this.frame_328 = function() {
		if (kc_correct){
			this.continuekc_btn.text_txt.text = "Continue >";
		}else{
			this.continuekc_btn.text_txt.text = "Try Again >";
		}
	}
	this.frame_336 = function() {
		this.stop();
		this.continuekc_btn.on("click", function(){
			if (kc_correct){
				exportRoot.gotoAndPlay("kc_question");
			}else{
				exportRoot.play();
			}
		});
	}
	this.frame_344 = function() {
		this.gotoAndStop("kc_enter");
	}
	this.frame_352 = function() {
		this.gotoAndPlay(nextSection);
	}
	this.frame_361 = function() {
		this.fa_init_continue_btn.visible = (finalExamCount!=null && finalExamCount<3);
	}
	this.frame_369 = function() {
		this.stop();
		this.fa_init_continue_btn.on("click", function(event){
			exportRoot.play();
		});
	}
	this.frame_382 = function() {
		initFAQuestion(this);
	}
	this.frame_419 = function() {
		this.stop();
		this.facontinue_btn.removeAllEventListeners();
		this.facontinue_btn.on("click", checkFAAnswer );
	}
	this.frame_421 = function() {
		initCourseComplete(this.complete_mc);
	}
	this.frame_433 = function() {
		this.stop();
	}
	this.frame_435 = function() {
		initCourseComplete(this.fail_mc);
		this.finish_btn.on("click", failCourse);
		if (finalExamCount==null || isNaN(finalExamCount)){
			finalExamCount = 3;
		}else{
			finalExamCount = Number(finalExamCount);
		}
		finalExamCount++;
		scormSave();
	}
	this.frame_446 = function() {
		this.stop();
		this.attempts_txt.text = "You have " + (3 - finalExamCount) + " attempts remaining.";
	}
	this.frame_447 = function() {
		//itemIdx = 0;
		this.gotoAndPlay("video");
	}
	this.frame_453 = function() {
		initEpilogueText(this);
	}
	this.frame_465 = function() {
		this.stop();
	}
	this.frame_473 = function() {
		if (itemIdx<(config.epilogue.length-1)) this.gotoAndPlay("epilogue_text");
	}
	this.frame_478 = function() {
		itemIdx = 0;
		fa_answers = new Array();
		this.gotoAndPlay("final_assessment");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(8).call(this.frame_32).wait(7).call(this.frame_39).wait(10).call(this.frame_49).wait(11).call(this.frame_60).wait(1).call(this.frame_61).wait(19).call(this.frame_80).wait(5).call(this.frame_85).wait(6).call(this.frame_91).wait(5).call(this.frame_96).wait(11).call(this.frame_107).wait(11).call(this.frame_118).wait(5).call(this.frame_123).wait(5).call(this.frame_128).wait(5).call(this.frame_133).wait(13).call(this.frame_146).wait(7).call(this.frame_153).wait(17).call(this.frame_170).wait(14).call(this.frame_184).wait(17).call(this.frame_201).wait(8).call(this.frame_209).wait(9).call(this.frame_218).wait(14).call(this.frame_232).wait(9).call(this.frame_241).wait(2).call(this.frame_243).wait(13).call(this.frame_256).wait(15).call(this.frame_271).wait(2).call(this.frame_273).wait(15).call(this.frame_288).wait(28).call(this.frame_316).wait(12).call(this.frame_328).wait(8).call(this.frame_336).wait(8).call(this.frame_344).wait(8).call(this.frame_352).wait(9).call(this.frame_361).wait(8).call(this.frame_369).wait(13).call(this.frame_382).wait(37).call(this.frame_419).wait(2).call(this.frame_421).wait(12).call(this.frame_433).wait(2).call(this.frame_435).wait(11).call(this.frame_446).wait(1).call(this.frame_447).wait(6).call(this.frame_453).wait(12).call(this.frame_465).wait(8).call(this.frame_473).wait(5).call(this.frame_478).wait(2));

	// Layer 3
	this.progress_mc = new lib.progress();
	this.progress_mc.setTransform(626.1,-13.5,1,1,0,0,0,250.5,12.8);
	this.progress_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.progress_mc).wait(148).to({_off:false},0).to({y:26.8},5).to({_off:true},326).wait(1));

	// top bar
	this.topbar_mc = new lib.top_bar();
	this.topbar_mc.setTransform(512,-42,1,1,0,0,0,512,28);
	this.topbar_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.topbar_mc).wait(72).to({_off:false},0).to({y:28},8).wait(29).to({y:-42},0).to({y:28},7).to({_off:true},363).wait(1));

	// bottom bar
	this.instance = new lib.bottom_bar();
	this.instance.setTransform(512,677.4,1,1,0,0,0,512,26.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(72).to({_off:false},0).to({y:644.4},8).wait(29).to({y:677.4},0).to({y:644.4},7).wait(237).to({mode:"synched",startPosition:0},0).to({y:678.9},7).to({_off:true},1).wait(86).to({_off:false,y:644.4},0).to({y:678.9},5).to({_off:true},1).wait(27));

	// help
	this.help_mc = new lib.helpMC();
	this.help_mc.setTransform(512,325.2);
	this.help_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.help_mc).to({_off:true},479).wait(1));

	// glossary
	this.glossary_mc = new lib.glossaryMC();
	this.glossary_mc.setTransform(512,325.2);
	this.glossary_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.glossary_mc).to({_off:true},479).wait(1));

	// biographies
	this.biographies_mc = new lib.biographiesMC();
	this.biographies_mc.setTransform(511,325.5);
	this.biographies_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.biographies_mc).to({_off:true},479).wait(1));

	// background
	this.background_mc = new lib.backgroundMC();
	this.background_mc.setTransform(512,325);
	this.background_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.background_mc).to({_off:true},479).wait(1));

	// blotter_coaching
	this.blotter_mc = new lib.blotterCoaching();
	this.blotter_mc.setTransform(512,325.5);
	this.blotter_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.blotter_mc).to({_off:true},479).wait(1));

	// blotter
	this.miniblotter_mc = new lib.blotterAnim();
	this.miniblotter_mc.setTransform(513,325);

	this.timeline.addTween(cjs.Tween.get(this.miniblotter_mc).to({_off:true},479).wait(1));

	// blotter
	this.loading_mc = new lib.loadingAnimMC();
	this.loading_mc.setTransform(494,325.3,0.3,0.3,0,0,0,69.4,69.2);

	this.helpoverlay_mc = new lib.helpOverlayMC();
	this.helpoverlay_mc.setTransform(634,415,1,1,0,0,0,364,74);
	this.helpoverlay_mc.visible = false;

	this.continuekc_btn = new lib.btn_continue_feedback();
	this.continuekc_btn.setTransform(976.4,656.6,1,1,0,0,0,75.5,19.6);
	this.continuekc_btn._off = true;
	new cjs.ButtonHelper(this.continuekc_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.loading_mc}]},181).to({state:[{t:this.helpoverlay_mc}]},36).to({state:[{t:this.loading_mc}]},25).to({state:[]},30).to({state:[{t:this.continuekc_btn}]},56).to({state:[{t:this.continuekc_btn}]},7).to({state:[]},2).to({state:[{t:this.continuekc_btn}]},8).to({state:[{t:this.continuekc_btn}]},7).to({state:[]},1).to({state:[]},126).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.continuekc_btn).wait(328).to({_off:false},0).to({y:622.1},7).to({_off:true},2).wait(8).to({_off:false,mode:"synched",startPosition:0},0).to({alpha:0},7).to({_off:true},1).wait(127));

	// Layer 4
	this.instance_1 = new lib.select_text("synched",0);
	this.instance_1.setTransform(511.6,25,1,1,0,0,0,219.6,30.8);
	this.instance_1._off = true;

	this.person_mc = new lib.person_panel();
	this.person_mc.setTransform(690.4,-133.3,1,1,0,0,0,240.7,178.7);
	this.person_mc._off = true;

	this.kcfeedback_mc = new lib.feedback_panel_1();
	this.kcfeedback_mc.setTransform(512,707.5,1,1,0,0,0,512,86.5);
	this.kcfeedback_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(80).to({_off:false},0).to({y:117},6).to({_off:true},23).wait(7).to({_off:false,y:25},0).to({y:117},7).to({_off:true},25).wait(332));
	this.timeline.addTween(cjs.Tween.get(this.person_mc).wait(181).to({_off:false},0).wait(20).to({y:234.7},8).wait(25).to({y:-124.1},7).to({_off:true},1).wait(238));
	this.timeline.addTween(cjs.Tween.get(this.kcfeedback_mc).wait(288).to({_off:false},0).wait(31).to({y:563.5},7).wait(11).to({y:707.5},7).wait(1).to({y:563.5},0).to({alpha:0},7).to({_off:true},1).wait(127));

	// Layer 10
	this.instance_2 = new lib.introMessageG("synched",0);
	this.instance_2.setTransform(514.4,335.6,1,1,0,0,0,455.4,253.3);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.skip_btn = new lib.btn_skip();
	this.skip_btn.setTransform(1042.9,618.4,1,1,0,0,0,75.5,19.6);
	this.skip_btn._off = true;
	new cjs.ButtonHelper(this.skip_btn, 0, 1, 1);

	this.continue_btn = new lib.btn_continue();
	this.continue_btn.setTransform(1103.6,593.6,1,1,0,0,0,75.5,19.6);
	this.continue_btn._off = true;
	new cjs.ButtonHelper(this.continue_btn, 0, 1, 1);

	this.kcperson_mc = new lib.knowledgecheckimage();
	this.kcperson_mc.setTransform(133.8,-39,1,1,0,0,0,88.4,91.7);
	this.kcperson_mc._off = true;

	this.facontinue_btn = new lib.btn_continuefa();
	this.facontinue_btn.setTransform(1091.8,628.7,0.833,0.833,0,0,0,75.5,19.6);
	this.facontinue_btn._off = true;
	new cjs.ButtonHelper(this.facontinue_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(32).to({_off:false},0).to({alpha:1},7).to({alpha:0},5).to({_off:true},1).wait(435));
	this.timeline.addTween(cjs.Tween.get(this.skip_btn).wait(49).to({_off:false},0).to({x:932.9},11).to({x:1042.9},7).to({_off:true},1).wait(412));
	this.timeline.addTween(cjs.Tween.get(this.continue_btn).wait(161).to({_off:false},0).to({x:932.4},7).to({x:942.6},2).to({_off:true},10).wait(1).to({_off:false,visible:false},0).wait(31).to({x:1110.6},5).to({_off:true},1).wait(262));
	this.timeline.addTween(cjs.Tween.get(this.kcperson_mc).wait(272).to({_off:false},0).wait(7).to({y:141},9).wait(57).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.facontinue_btn).wait(414).to({_off:false},0).to({x:953.8},5).to({_off:true},1).wait(60));

	// Layer 11
	this.intro_btn = new lib.btn_continue();
	this.intro_btn.setTransform(1103.6,623.6,1,1,0,0,0,75.5,19.6);
	this.intro_btn._off = true;
	new cjs.ButtonHelper(this.intro_btn, 0, 1, 1);

	this.james_mc = new lib.characters();
	this.james_mc.setTransform(212.4,428.2,1,1,0,0,0,116,223.8);
	this.james_mc.alpha = 0;

	this.choice1_mc = new lib.choice_panel();
	this.choice1_mc.setTransform(182.8,752.2,1,1,0,0,0,153.9,130.1);
	this.choice1_mc.visible = false;

	this.kcprogress_mc = new lib.knowledge_check_header();
	this.kcprogress_mc.setTransform(422.9,91.5,1,1,0,0,0,206.2,13.5);
	this.kcprogress_mc.alpha = 0;
	this.kcprogress_mc._off = true;

	this.fa_message_mc = new lib.faInstructionsMC();
	this.fa_message_mc.setTransform(500.9,310.4,1,1,0,0,0,310,176.5);
	this.fa_message_mc.alpha = 0;
	this.fa_message_mc._off = true;

	this.faprogress_mc = new lib.finalassessmenttitle();
	this.faprogress_mc.setTransform(252.8,44.3,1,1,0,0,0,206.2,13.5);
	this.faprogress_mc._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.intro_btn}]},32).to({state:[{t:this.intro_btn}]},7).to({state:[{t:this.intro_btn}]},2).to({state:[]},1).to({state:[{t:this.james_mc,p:{x:212.4}}]},38).to({state:[]},29).to({state:[{t:this.james_mc,p:{x:142.8}}]},9).to({state:[]},30).to({state:[{t:this.choice1_mc}]},70).to({state:[]},24).to({state:[{t:this.kcprogress_mc}]},46).to({state:[{t:this.kcprogress_mc}]},9).to({state:[{t:this.kcprogress_mc}]},48).to({state:[{t:this.kcprogress_mc}]},7).to({state:[]},1).to({state:[{t:this.fa_message_mc}]},8).to({state:[{t:this.fa_message_mc}]},6).to({state:[{t:this.fa_message_mc}]},2).to({state:[{t:this.fa_message_mc}]},1).to({state:[{t:this.fa_message_mc}]},5).to({state:[{t:this.faprogress_mc}]},1).to({state:[{t:this.faprogress_mc}]},6).to({state:[{t:this.faprogress_mc}]},7).to({state:[]},31).to({state:[]},59).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.intro_btn).wait(32).to({_off:false},0).to({x:932.4},7).to({x:942.6},2).to({_off:true},1).wait(438));
	this.timeline.addTween(cjs.Tween.get(this.kcprogress_mc).wait(288).to({_off:false},0).to({alpha:1},9).wait(48).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.fa_message_mc).wait(361).to({_off:false},0).to({alpha:0.75},6).to({alpha:1},2).wait(1).to({alpha:0},5).to({_off:true},1).wait(104));
	this.timeline.addTween(cjs.Tween.get(this.faprogress_mc).wait(376).to({_off:false},0).wait(6).to({y:90.3},7).to({_off:true},31).wait(60));

	// Layer 8
	this.start_btn = new lib.btn_start();
	this.start_btn.setTransform(1154.1,614.4,1,1,0,0,0,88.2,19.6);
	this.start_btn._off = true;
	new cjs.ButtonHelper(this.start_btn, 0, 1, 1);

	this.alison_mc = new lib.characters();
	this.alison_mc.setTransform(509.1,428.2,1,1,0,0,0,116,223.8);
	this.alison_mc.alpha = 0;

	this.desc_mc = new lib.intro_text();
	this.desc_mc.setTransform(1252.6,307.2,1,1,0,0,0,229.7,147.1);
	this.desc_mc._off = true;

	this.choice2_mc = new lib.choice_panel();
	this.choice2_mc.setTransform(510.5,752.2,1,1,0,0,0,153.9,130.1);
	this.choice2_mc.visible = false;

	this.coaching_mc = new lib.coacing_panel();
	this.coaching_mc.setTransform(691.4,-167,1,1,0,0,0,243.2,212.8);
	this.coaching_mc._off = true;

	this.kcquestion_mc = new lib.knowledge_check_question();
	this.kcquestion_mc.setTransform(607.3,158.9,1,1,0,0,0,390.6,53.9);
	this.kcquestion_mc.alpha = 0;
	this.kcquestion_mc._off = true;

	this.fa_init_continue_btn = new lib.btn_continue();
	this.fa_init_continue_btn.setTransform(1104.9,611,1,1,0,0,0,75.5,19.6);
	this.fa_init_continue_btn._off = true;
	new cjs.ButtonHelper(this.fa_init_continue_btn, 0, 1, 1);

	this.faquestion_mc = new lib.finalassessmentquestion();
	this.faquestion_mc.setTransform(504.7,203.6,1,1,0,0,0,458.1,81.7);
	this.faquestion_mc.alpha = 0;
	this.faquestion_mc._off = true;

	this.finish_btn = new lib.btn_finish();
	this.finish_btn.setTransform(1104.9,611,1,1,0,0,0,75.5,19.6);
	this.finish_btn._off = true;
	new cjs.ButtonHelper(this.finish_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.start_btn}]},15).to({state:[{t:this.start_btn}]},6).to({state:[{t:this.start_btn}]},2).to({state:[{t:this.start_btn}]},2).to({state:[{t:this.start_btn}]},6).to({state:[]},1).to({state:[{t:this.alison_mc,p:{x:509.1}}]},48).to({state:[]},29).to({state:[{t:this.alison_mc,p:{x:386.7}}]},9).to({state:[]},30).to({state:[{t:this.desc_mc}]},5).to({state:[{t:this.desc_mc}]},8).to({state:[{t:this.desc_mc}]},12).to({state:[{t:this.desc_mc}]},7).to({state:[]},1).to({state:[{t:this.choice2_mc}]},37).to({state:[{t:this.coaching_mc}]},24).to({state:[{t:this.coaching_mc}]},9).to({state:[{t:this.coaching_mc}]},6).to({state:[{t:this.coaching_mc}]},8).to({state:[]},7).to({state:[{t:this.kcquestion_mc}]},16).to({state:[{t:this.kcquestion_mc}]},8).to({state:[{t:this.kcquestion_mc}]},49).to({state:[{t:this.kcquestion_mc}]},7).to({state:[]},1).to({state:[{t:this.fa_init_continue_btn}]},8).to({state:[{t:this.fa_init_continue_btn}]},8).to({state:[{t:this.fa_init_continue_btn}]},1).to({state:[{t:this.fa_init_continue_btn}]},5).to({state:[{t:this.faquestion_mc}]},1).to({state:[{t:this.faquestion_mc}]},13).to({state:[{t:this.faquestion_mc}]},5).to({state:[]},26).to({state:[{t:this.finish_btn}]},14).to({state:[{t:this.finish_btn}]},8).to({state:[]},5).to({state:[]},32).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.start_btn).wait(15).to({_off:false},0).to({x:905.6},6).to({x:916.1},2).wait(2).to({y:922.4},6).to({_off:true},1).wait(448));
	this.timeline.addTween(cjs.Tween.get(this.desc_mc).wait(153).to({_off:false},0).to({x:758.1},8).wait(12).to({x:1262.1},7).to({_off:true},1).wait(299));
	this.timeline.addTween(cjs.Tween.get(this.coaching_mc).wait(242).to({_off:false},0).to({y:270},9).wait(6).to({y:-167},8).to({_off:true},7).wait(208));
	this.timeline.addTween(cjs.Tween.get(this.kcquestion_mc).wait(288).to({_off:false},0).to({alpha:1},8).wait(49).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.fa_init_continue_btn).wait(361).to({_off:false},0).to({x:924.9},8).wait(1).to({x:1104.9},5).to({_off:true},1).wait(104));
	this.timeline.addTween(cjs.Tween.get(this.faquestion_mc).wait(376).to({_off:false},0).wait(13).to({alpha:1},5).to({_off:true},26).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.finish_btn).wait(434).to({_off:false},0).to({x:944.9},8).to({_off:true},5).wait(33));

	// Layer 5
	this.instance_3 = new lib.opening_screen_bottom("synched",0);
	this.instance_3.setTransform(512,806.9,1,1,0,0,0,512,152.5);
	this.instance_3._off = true;

	this.start_btn_1 = new lib.btn_start();
	this.start_btn_1.setTransform(916.1,614.4,1,1,0,0,0,88.2,19.6);
	this.start_btn_1._off = true;
	new cjs.ButtonHelper(this.start_btn_1, 0, 1, 1);

	this.tammy_mc = new lib.characters();
	this.tammy_mc.setTransform(632.8,428.2,1,1,0,0,0,116,223.8);
	this.tammy_mc.alpha = 0;

	this.title_mc = new lib.conversation_title();
	this.title_mc.setTransform(-223.1,131.5,1,1,0,0,0,220.1,28.7);
	this.title_mc._off = true;

	this.choice3_mc = new lib.choice_panel();
	this.choice3_mc.setTransform(838.3,752.2,1,1,0,0,0,153.9,130.1);
	this.choice3_mc.visible = false;

	this.coaching_continue_btn = new lib.btn_continue();
	this.coaching_continue_btn.setTransform(1103.6,593.6,1,1,0,0,0,75.5,19.6);
	this.coaching_continue_btn._off = true;
	new cjs.ButtonHelper(this.coaching_continue_btn, 0, 1, 1);

	this.kcanswer1_mc = new lib.answer();
	this.kcanswer1_mc.setTransform(153.7,740.1,1,1,0,0,0,115.2,121);
	this.kcanswer1_mc._off = true;

	this.faanswer1_mc = new lib.assessment_panel();
	this.faanswer1_mc.setTransform(504.8,692.9,1,1,0,0,0,458.8,35.1);
	this.faanswer1_mc._off = true;

	this.complete_mc = new lib.coursecompleteMC();
	this.complete_mc.setTransform(492,348,1,1,0,0,0,196,23);
	this.complete_mc.alpha = 0;
	this.complete_mc._off = true;

	this.fail_mc = new lib.courseFailMC();
	this.fail_mc.setTransform(492,348,1,1,0,0,0,196,23);
	this.fail_mc.alpha = 0;
	this.fail_mc._off = true;

	this.attempts_txt = new cjs.Text("You have 2 attempts remaining", "30px 'Frutiger Next Pro Medium'", "#00A2DD");
	this.attempts_txt.name = "attempts_txt";
	this.attempts_txt.textAlign = "center";
	this.attempts_txt.lineHeight = 31;
	this.attempts_txt.lineWidth = 586;
	this.attempts_txt.setTransform(487,400.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},5).to({state:[{t:this.instance_3}]},8).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_3}]},10).to({state:[{t:this.instance_3}]},6).to({state:[]},1).to({state:[{t:this.start_btn_1}]},77).to({state:[{t:this.start_btn_1}]},6).to({state:[]},1).to({state:[{t:this.tammy_mc}]},2).to({state:[]},30).to({state:[{t:this.title_mc}]},5).to({state:[{t:this.title_mc}]},8).to({state:[{t:this.title_mc}]},12).to({state:[{t:this.title_mc}]},7).to({state:[]},1).to({state:[{t:this.choice3_mc}]},37).to({state:[{t:this.coaching_continue_btn}]},24).to({state:[{t:this.coaching_continue_btn}]},6).to({state:[{t:this.coaching_continue_btn}]},8).to({state:[{t:this.coaching_continue_btn}]},1).to({state:[{t:this.coaching_continue_btn}]},7).to({state:[]},1).to({state:[{t:this.kcanswer1_mc}]},23).to({state:[{t:this.kcanswer1_mc}]},8).to({state:[{t:this.kcanswer1_mc}]},8).to({state:[{t:this.kcanswer1_mc}]},41).to({state:[{t:this.kcanswer1_mc}]},7).to({state:[]},1).to({state:[{t:this.faanswer1_mc}]},23).to({state:[{t:this.faanswer1_mc}]},18).to({state:[{t:this.faanswer1_mc}]},7).to({state:[{t:this.complete_mc}]},19).to({state:[{t:this.complete_mc}]},11).to({state:[{t:this.fail_mc}]},3).to({state:[{t:this.fail_mc},{t:this.attempts_txt}]},11).to({state:[]},2).wait(33));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off:false},0).to({y:484.9},8).to({y:498.9},2).wait(10).to({startPosition:0},0).to({y:806.9},6).to({_off:true},1).wait(448));
	this.timeline.addTween(cjs.Tween.get(this.start_btn_1).wait(109).to({_off:false},0).to({y:922.4},6).to({_off:true},1).wait(364));
	this.timeline.addTween(cjs.Tween.get(this.title_mc).wait(153).to({_off:false},0).to({x:294.4},8).wait(12).to({mode:"synched",startPosition:0},0).to({x:-221.6},7).to({_off:true},1).wait(299));
	this.timeline.addTween(cjs.Tween.get(this.coaching_continue_btn).wait(242).to({_off:false},0).wait(6).to({x:942.6},8).wait(1).to({x:1103.6},7).to({_off:true},1).wait(215));
	this.timeline.addTween(cjs.Tween.get(this.kcanswer1_mc).wait(288).to({_off:false},0).wait(8).to({y:353.7},8).wait(41).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.faanswer1_mc).wait(376).to({_off:false},0).wait(18).to({y:315.4},7).to({_off:true},19).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.complete_mc).wait(420).to({_off:false},0).to({y:558,alpha:1},11).to({_off:true},3).wait(46));
	this.timeline.addTween(cjs.Tween.get(this.fail_mc).wait(434).to({_off:false},0).to({y:558,alpha:1},11).to({_off:true},2).wait(33));

	// Layer 1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_5 = new cjs.Graphics().p("A8/cgQsBr0AAwsQAAwrMBr0QMBr0Q+AAQQ/AAMAL0QMCL0AAQrQAAQssCL0QsALzw/AAQw+AAsBrzg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(5).to({graphics:mask_graphics_5,x:602.5,y:336}).wait(27).to({graphics:null,x:0,y:0}).wait(448));

	// Layer 9
	this.instance_4 = new lib.neuralMC();
	this.instance_4.setTransform(585,331,0.5,0.5);

	this.kevin_mc = new lib.characters();
	this.kevin_mc.setTransform(695.6,204.4);
	this.kevin_mc.alpha = 0;

	this.instance_5 = new lib.opening_screen_bottom("synched",0);
	this.instance_5.setTransform(512,498.9,1,1,0,0,0,512,152.5);
	this.instance_5._off = true;

	this.titleImg_mc = new lib.conversation_title_image_1();
	this.titleImg_mc.setTransform(-262.5,309.1,1,1,0,0,0,255,143);
	this.titleImg_mc._off = true;

	this.characters_mc = new lib.conversation_character();
	this.characters_mc.setTransform(190.4,420.2,1,1,0,0,0,235.5,319);

	this.kcanswer2_mc = new lib.answer();
	this.kcanswer2_mc.setTransform(396,740.1,1,1,0,0,0,115.2,121);
	this.kcanswer2_mc._off = true;

	this.faanswer2_mc = new lib.assessment_panel();
	this.faanswer2_mc.setTransform(504.8,689,1,1,0,0,0,458.8,35.1);
	this.faanswer2_mc._off = true;

	this.etext_mc = new lib.etextMC();
	this.etext_mc.setTransform(503.4,321.1,0.62,0.62,0,0,0,411.4,81);
	this.etext_mc.alpha = 0;
	this.etext_mc._off = true;

	this.instance_4.mask = this.kevin_mc.mask = this.instance_5.mask = this.titleImg_mc.mask = this.characters_mc.mask = this.kcanswer2_mc.mask = this.faanswer2_mc.mask = this.etext_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},5).to({state:[]},27).to({state:[{t:this.kevin_mc,p:{regX:0,regY:0,x:695.6,y:204.4}}]},48).to({state:[{t:this.instance_5}]},29).to({state:[{t:this.instance_5}]},6).to({state:[{t:this.kevin_mc,p:{regX:116,regY:223.8,x:882.4,y:428.2}}]},3).to({state:[{t:this.titleImg_mc}]},30).to({state:[{t:this.titleImg_mc}]},5).to({state:[{t:this.titleImg_mc}]},8).to({state:[{t:this.titleImg_mc}]},12).to({state:[{t:this.titleImg_mc}]},7).to({state:[{t:this.characters_mc}]},1).to({state:[]},91).to({state:[{t:this.kcanswer2_mc}]},16).to({state:[{t:this.kcanswer2_mc}]},12).to({state:[{t:this.kcanswer2_mc}]},8).to({state:[{t:this.kcanswer2_mc}]},37).to({state:[{t:this.kcanswer2_mc}]},7).to({state:[]},1).to({state:[{t:this.faanswer2_mc}]},23).to({state:[{t:this.faanswer2_mc}]},20).to({state:[{t:this.faanswer2_mc}]},7).to({state:[]},17).to({state:[{t:this.etext_mc}]},33).to({state:[{t:this.etext_mc}]},10).to({state:[{t:this.etext_mc}]},5).to({state:[{t:this.etext_mc}]},5).to({state:[]},1).to({state:[]},5).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(109).to({_off:false},0).to({y:806.9},6).to({_off:true},3).wait(362));
	this.timeline.addTween(cjs.Tween.get(this.titleImg_mc).wait(148).to({_off:false},0).wait(5).to({x:255},8).wait(12).to({x:-261},7).to({_off:true},1).wait(299));
	this.timeline.addTween(cjs.Tween.get(this.kcanswer2_mc).wait(288).to({_off:false},0).wait(12).to({y:353.7},8).wait(37).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.faanswer2_mc).wait(376).to({_off:false},0).wait(20).to({y:389},7).to({_off:true},17).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.etext_mc).wait(453).to({_off:false},0).to({regY:80.9,scaleX:1,scaleY:1,alpha:1},10).wait(5).to({alpha:0},5).to({_off:true},1).wait(6));

	// bg
	this.instance_6 = new lib.opening_bg("synched",0);
	this.instance_6.setTransform(512,363.2,1,1,0,0,0,512,364);
	this.instance_6.alpha = 0;

	this.bgs_mc = new lib.bg1();
	this.bgs_mc.setTransform(512,364.4,1,1,0,0,0,512,364);
	this.bgs_mc.alpha = 0;
	this.bgs_mc._off = true;

	this.kcanswer3_mc = new lib.answer();
	this.kcanswer3_mc.setTransform(638.2,740.1,1,1,0,0,0,115.2,121);
	this.kcanswer3_mc._off = true;

	this.faanswer3_mc = new lib.assessment_panel();
	this.faanswer3_mc.setTransform(504.8,685.2,1,1,0,0,0,458.8,35.1);
	this.faanswer3_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({alpha:1},11).wait(14).to({startPosition:0},0).to({alpha:0},6).to({_off:true},1).wait(77).to({_off:false,alpha:1},0).to({alpha:0},6).wait(7).to({startPosition:0},0).to({_off:true},59).wait(299));
	this.timeline.addTween(cjs.Tween.get(this.bgs_mc).wait(181).to({_off:false},0).wait(3).to({alpha:1},9).to({_off:true},79).wait(208));
	this.timeline.addTween(cjs.Tween.get(this.kcanswer3_mc).wait(288).to({_off:false},0).wait(16).to({y:353.7},8).wait(33).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.faanswer3_mc).wait(376).to({_off:false},0).wait(22).to({y:462.7},7).to({_off:true},15).wait(60));

	// bg
	this.kcanswer4_mc = new lib.answer();
	this.kcanswer4_mc.setTransform(880.5,740.1,1,1,0,0,0,115.2,121);
	this.kcanswer4_mc._off = true;

	this.faanswer4_mc = new lib.assessment_panel();
	this.faanswer4_mc.setTransform(504.8,685.9,1,1,0,0,0,458.8,35.1);
	this.faanswer4_mc._off = true;

	this.instance_7 = new lib.epilogueBG();
	this.instance_7.setTransform(511.5,358.4,1,1,0,0,0,512.5,310.4);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.kcanswer4_mc).wait(276).to({_off:false},0).wait(20).to({y:353.7},8).wait(41).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.faanswer4_mc).wait(376).to({_off:false},0).wait(23).to({y:536.3},7).to({_off:true},14).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(447).to({_off:false},0).to({alpha:1},5).wait(22).to({alpha:0},4).to({_off:true},1).wait(1));

	// bg
	this.instance_8 = new lib.Tween1("synched",0);
	this.instance_8.setTransform(512,326.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween2("synched",0);
	this.instance_9.setTransform(512,326.5);
	this.instance_9._off = true;

	this.faanswer5_mc = new lib.assessment_panel();
	this.faanswer5_mc.setTransform(504.8,685.9,1,1,0,0,0,458.8,35.1);
	this.faanswer5_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(260).to({_off:false},0).to({_off:true,alpha:1},6).wait(214));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(260).to({_off:false},6).wait(79).to({startPosition:0},0).to({alpha:0},7).to({_off:true},1).wait(127));
	this.timeline.addTween(cjs.Tween.get(this.faanswer5_mc).wait(376).to({_off:false},0).wait(25).to({y:610.3},7).to({_off:true},12).wait(60));

	// bg
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhP/AyxMAAAhliMCf/AAAMAAABlig");
	this.shape.setTransform(512,325);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},479).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(322.1,-274.8,1409.5,1805.4);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;